//
//  AppDelegate.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import FirebaseMessaging
import FirebaseInstanceID
import FirebaseCore
import CoreLocation
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()
    var drawerController = KYDrawerController.init(drawerDirection: .left, drawerWidth: 300)
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusBar.backgroundColor = UIColor(red: 0.094, green: 0.094, blue: 0.149, alpha: 1)
//        }
        
        GMSServices.provideAPIKey(Constant().google_map_key)
        GMSPlacesClient.provideAPIKey(Constant().google_map_key)
                
            if #available(iOS 13.0, *) {
                self.window?.overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
        
        let login_d = UserDefaults.standard.object(forKey: "user_info") as? NSDictionary ?? [:]
        
        print(login_d)
        
        if login_d.count != 0
        {
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigation = UINavigationController.init(rootViewController: drawerC)
            navigation.setNavigationBarHidden(true, animated: true)
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
        else
        {
            
        }
        
        
        LocationSingleton.sharedInstance.startUpdatingLocation()
        
    
        FirebaseApp.configure()
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            
            //  Messaging.messaging().delegate = self
            
        }
        
        application.registerForRemoteNotifications()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(appDelegate.tokenRefereshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
       // UIApplication.shared.statusBarStyle = .lightContent
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print(fcmToken)
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    @objc func tokenRefereshNotification()
    {
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                UserDefaults.standard.set(result.token, forKey: "DeviceToken")
            }
        })
        
    }

    
    func checklocationAuthorization()
    {
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Location Services Turned Off", message: "Turn on Location services to allow Clique MJ to determine your location", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) in
                    print("")
                    let ur = URL.init(string: UIApplication.openSettingsURLString)
                    UIApplication.shared.open(ur!, options: [:], completionHandler: nil)
                }))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        //let googleDidHandle = GIDSignIn.sharedInstance()?.handle(url as URL)
        
        //        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL,
        //                                                                sourceApplication: sourceApplication,
        //                                                                annotation: annotation)
        
        let facebookDidHandle = ApplicationDelegate.shared.application(
            application,
            open: url as URL,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        return  facebookDidHandle
    }
    
    
    
}

