//
//  AgeView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 28/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class AgeView: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var date_btn: UIButton!
    @IBOutlet weak var date_picker: UIDatePicker!
    @IBOutlet weak var date_view: UIView!
    @IBOutlet weak var age_img: UIImageView!
    @IBOutlet weak var custom_view: ANCustomView!
    @IBOutlet weak var confirm_btn: KButton!
    @IBOutlet weak var birth_date_txt: CustomUITextField!
    
    var user_id = String()
    var error_message = String()
    var come_from = String()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.custom_view.layer.cornerRadius = 5.0
        self.custom_view.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func date_change(sender:UITextField)
    {
        
    }
    
    @IBAction func Done_action(_ sender: Any)
    {
        self.date_view.isHidden = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.birth_date_txt.text = dateFormatter.string(from: date_picker.date)
        
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirm_btn_action(_ sender: Any)
    {
        if Check_validation() == false
        {
            self.alertmessage(error: "", message: self.error_message)
        }
        else
        {
            
            self.Age_API(user_id: self.user_id, dob: self.birth_date_txt.text!)
        }
    }
    
    func Age_API(user_id:String,dob:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().ws_set_date_of_birth)")
            
            let dict = [APIKeys().user_id:user_id,APIKeys().dob:dob]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        if self.come_from == "login"
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            
                            let main = UIStoryboard.init(name: "Main", bundle: nil)
                            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                            let navigation = UINavigationController.init(rootViewController: drawerC)
                            navigation.setNavigationBarHidden(true, animated: true)
                            appDelegate.window?.rootViewController = navigation
                            appDelegate.window?.makeKeyAndVisible()
                        }
                        else
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            let onboarding = self.storyboard?.instantiateViewController(withIdentifier: "IntroView") as! IntroView
                            onboarding.modalPresentationStyle = .fullScreen
                            self.present(onboarding, animated: true, completion: nil)
                        }
                        
                        
                        
                        
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    func Check_validation() -> Bool
    {
        
        if birth_date_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().date_empty
            return false
        }
        
        let age_cal = self.getAgeFromDOF(date: self.birth_date_txt.text!)
        if age_cal.0 < 20
        {
            self.error_message = ResponseMessage().age_valid
            return false
        }
        self.error_message = ""
        return true
    }
    
    @IBAction func Done_btn_Action(_ sender: Any)
    {
        self.date_view.isHidden = false
    }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //# Age Calculate
    
    func getAgeFromDOF(date: String) -> (Int,Int,Int) {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "YYYY-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)
        
        let calender = Calendar.current
        
        let dateComponent = calender.dateComponents([.year, .month, .day], from:
            dateOfBirth!, to: Date())
        
        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }
}
