//
//  BrowseFirstCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class BrowseFirstCell: UITableViewCell {

    @IBOutlet weak var following_btn: UIButton!
    
    @IBOutlet weak var line_view: UIImageView!
    @IBOutlet weak var near_me_btn: UIButton!
    @IBOutlet weak var opticity_img1: UIImageView!
    @IBOutlet weak var opticity_img: UIImageView!
    @IBOutlet weak var main_customView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.opticity_img.layer.cornerRadius = 18.0
        self.opticity_img.layer.masksToBounds = true
        self.opticity_img1.layer.cornerRadius = 18.0
        self.opticity_img1.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
