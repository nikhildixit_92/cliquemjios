//
//  BrowseSecondCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class BrowseSecondCell: UITableViewCell {

    @IBOutlet weak var follow_btn: UIButton!
    @IBOutlet weak var buds_count_lbl: UILabel!
    @IBOutlet weak var CollectBuds_btn: UIButton!
    @IBOutlet weak var Disp_time_lbl: UILabel!
    @IBOutlet weak var disp_status_lbl: UILabel!
    @IBOutlet weak var disp_view: UIView!
    @IBOutlet weak var star_lbl: UILabel!
    @IBOutlet weak var disp_name_lbl: UILabel!
    @IBOutlet weak var dipsdistance_lbl: UILabel!
    @IBOutlet weak var disp_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
