//
//  BrowseView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SVProgressHUD

protocol show_homeAPI
{
    func HomeAPI()
}

class BrowseView: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,GMSMapViewDelegate,DispensaryNameShow,UITextFieldDelegate,Update_location
{
 
    var search_lng = ""
    var search_long = ""
    var homeDelegate:show_homeAPI?
    @IBOutlet weak var address_txt: UILabel!
    @IBOutlet weak var saerch_Txt: CustomUITextField!
    var markers = [GMSMarker]()
    @IBOutlet weak var map_view: GMSMapView!
    
    var come_from = String()
    var select_value = 0
    @IBOutlet weak var table_originx: NSLayoutConstraint!
    @IBOutlet weak var search_table_view: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "BrowseFirstCell", bundle: nil)
        self.search_table_view.register(nib, forCellReuseIdentifier: "BrowseFirstCell")
        
        let nib1 = UINib.init(nibName: "BrowseSecondCell", bundle: nil)
        self.search_table_view.register(nib1, forCellReuseIdentifier: "BrowseSecondCell")
        
        let nib2 = UINib.init(nibName: "NoDisCell", bundle: nil)
        self.search_table_view.register(nib2, forCellReuseIdentifier: "NoDisCell")
        
        
        self.search_table_view.rowHeight = 178
        
        self.map_view.padding = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 10)
        self.map_view.delegate = self
        
        self.Update_location()
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeUp.direction = .up
        swipeUp.delegate = self // set delegate
        self.search_table_view.addGestureRecognizer(swipeUp)
        // Do any additional setup after loading the view.
        
        
        if come_from == "Browse"
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", search_txt: "")
            }
            
            
            //            if CheckLocation().check_location() == false
            //            {
            //
            //            }
            //            else
            //            {
            //                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", search_txt: "")
            //            }
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", search_txt: "")
            }
            
            
            //            if CheckLocation().check_location() == false
            //            {
            //
            //            }
            //            else
            //            {
            //                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", search_txt: "")
            //            }
        }
        
        self.saerch_Txt.delegate = self
        self.saerch_Txt.addTarget(self, action: #selector(search_txt_Action), for: .editingChanged)
        
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.geocode(latitude: LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0, longitude: LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0) { (response, error) in
                
                let state = response?.administrativeArea ?? ""
                let city = response?.country ?? ""
                
                self.address_txt.text = "\(state), \(city)"
                // self.home_table.reloadData()
            }
        }
        
    }
    
    @objc func search_txt_Action(sender:UITextField)
    {
        if self.saerch_Txt.text?.count == 0
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", search_txt: "")
            }
            
            
            //                      if CheckLocation().check_location() == false
            //                      {
            //
            //                      }
            //                      else
            //                      {
            //                          self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", search_txt: "")
            //                      }
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", search_txt: self.saerch_Txt.text!)
            }
            
            
            //                      if CheckLocation().check_location() == false
            //                      {
            //
            //                      }
            //                      else
            //                      {
            //                        self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", search_txt: self.saerch_Txt.text!)
            //                      }
        }
    }
    
    func Update_location()
    {
        cameraMoveToLocation(toLocation: LocationSingleton.sharedInstance.locationManager?.location?.coordinate)
        
        self.map_view.isMyLocationEnabled = true
        self.map_view.settings.myLocationButton = true
        self.map_view.settings.allowScrollGesturesDuringRotateOrZoom = false
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?)
    {
        if toLocation != nil
        {
            self.map_view.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 18.0)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            if come_from == "Browse"
            {
                if Search_Dips_List.count != 0
                {
                    if select_value == 0
                    {
                        if Search_Dips_List[0].followed.count != 0
                        {
                            return Search_Dips_List[0].followed.count
                        }
                        else
                        {
                            return 1
                        }
                        
                        
                    }
                    else if select_value == 1
                    {
                        if Search_Dips_List[0].nearby.count != 0
                        {
                            return Search_Dips_List[0].nearby.count
                        }
                        else
                        {
                            return 1
                        }
                        
                        
                    }
                    else
                    {
                        return 1
                    }
                }
                else
                {
                    return 1
                }
            }
            else
            {
                if Search_Dips_List.count != 0
                {
                    if select_value == 0
                    {
                        if Search_Dips_List[0].nearby.count != 0
                        {
                            return Search_Dips_List[0].nearby.count
                        }
                        else
                        {
                            return 1
                        }
                        
                        
                    }
                    else if select_value == 1
                    {
                        if Search_Dips_List[0].followed.count != 0
                        {
                            return Search_Dips_List[0].followed.count
                        }
                        else
                        {
                            return 1
                        }
                        
                        
                    }
                    else
                    {
                        return 1
                    }
                }
                else
                {
                    return 1
                }
                
                
            }
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseFirstCell", for: indexPath) as! BrowseFirstCell
            
            cell.main_customView.clipsToBounds = true
            cell.main_customView.layer.cornerRadius = 30
            cell.main_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            cell.main_customView.isUserInteractionEnabled = true
            
            let swipedown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            swipedown.direction = .down
            swipedown.delegate = self // set delegate
            cell.main_customView.addGestureRecognizer(swipedown)
            
            if come_from == "Browse"
            {
                cell.following_btn.setTitle("FOLLOWING", for: .normal)
                cell.near_me_btn.setTitle("NEAR ME", for: .normal)
                
                if select_value == 0
                {
                    cell.opticity_img.isHidden = false
                    cell.opticity_img1.isHidden = true
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    
                    
                    
                }
                else
                {
                    cell.opticity_img.isHidden = true
                    cell.opticity_img1.isHidden = false
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                }
                
                cell.following_btn.addTarget(self, action: #selector(following_me_action), for: .touchUpInside)
                cell.near_me_btn.addTarget(self, action: #selector(nearby_me_Action), for: .touchUpInside)
            }
            else
            {
                cell.following_btn.setTitle("DISCOVER", for: .normal)
                cell.near_me_btn.setTitle("FOLLOWING", for: .normal)
                
                if select_value == 0
                {
                    
                    
                    cell.opticity_img.isHidden = false
                    cell.opticity_img1.isHidden = true
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                }
                else
                {
                    cell.opticity_img.isHidden = true
                    cell.opticity_img1.isHidden = false
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                }
                
                cell.following_btn.addTarget(self, action: #selector(Discover_action), for: .touchUpInside)
                cell.near_me_btn.addTarget(self, action: #selector(New_following_me), for: .touchUpInside)
            }
            
            return cell
        }
        else
        {
            if Search_Dips_List.count != 0
            {
                
                if come_from == "Browse"
                {
                    
                    if select_value == 0
                    {
                        if Search_Dips_List[0].followed.count != 0
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseSecondCell", for: indexPath) as! BrowseSecondCell
                            let dispensireis = Search_Dips_List[0].followed[indexPath.row]
                            
                            cell.disp_name_lbl.text = dispensireis.dispensaryName ?? ""
                            cell.dipsdistance_lbl.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                            cell.star_lbl.text = "\(dispensireis.rating ?? 0).0"
                            
                            let dispensries_img_S = dispensireis.profilePicture
                            let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            cell.disp_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                      placeholderImage: UIImage(named: ""),
                                                      options: .refreshCached,
                                                      completed: nil)
                            
                            let is_open = dispensireis.isOpen ?? 0
                            
                            if is_open == 0
                            {
                                cell.disp_status_lbl.text = "Close"
                                cell.Disp_time_lbl.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            }
                            else
                            {
                                cell.disp_status_lbl.text = "Open"
                                cell.Disp_time_lbl.text = "Close \(dispensireis.realClosingTime ?? "")"
                            }
                            
                            cell.buds_count_lbl.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                            cell.CollectBuds_btn.tag  = indexPath.row
                            cell.CollectBuds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                            cell.follow_btn.tag = indexPath.row
                            cell.follow_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                            
                            let follow = dispensireis.follow ?? 0
                            
                            if follow == 0
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                            }
                            else if follow == 1
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            else if follow == 2
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            return cell
                        }
                        else
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDisCell", for: indexPath) as! NoDisCell
                            
                            cell.ui_info.text = "Go to the near me tab to browse dispensaries"
                            
                            return cell
                        }
                    }
                    else
                    {
                        if Search_Dips_List[0].nearby.count != 0
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseSecondCell", for: indexPath) as! BrowseSecondCell
                            
                            let dispensireis = Search_Dips_List[0].nearby[indexPath.row]
                            
                            cell.disp_name_lbl.text = dispensireis.dispensaryName ?? ""
                            cell.dipsdistance_lbl.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                            cell.star_lbl.text = "\(dispensireis.rating ?? 0).0"
                            
                            let dispensries_img_S = dispensireis.profilePicture
                            let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            cell.disp_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                      placeholderImage: UIImage(named: ""),
                                                      options: .refreshCached,
                                                      completed: nil)
                            
                            let is_open = dispensireis.isOpen ?? 0
                            
                            if is_open == 0
                            {
                                cell.disp_status_lbl.text = "Close"
                                cell.Disp_time_lbl.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            }
                            else
                            {
                                cell.disp_status_lbl.text = "Open"
                                cell.Disp_time_lbl.text = "Close \(dispensireis.realClosingTime ?? "")"
                            }
                            
                            cell.buds_count_lbl.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                            cell.CollectBuds_btn.tag  = indexPath.row
                            cell.CollectBuds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                            cell.follow_btn.tag = indexPath.row
                            cell.follow_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                            
                            let follow = dispensireis.follow ?? 0
                            
                            if follow == 0
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                            }
                            else if follow == 1
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            else if follow == 2
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            
                            return cell
                        }
                        else
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDisCell", for: indexPath) as! NoDisCell
                            cell.ui_info.text = "There is no dispensaries near by"
                            return cell
                        }
                    }
                }
                else
                {
                    if select_value == 0
                    {
                        if Search_Dips_List[0].nearby.count != 0
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseSecondCell", for: indexPath) as! BrowseSecondCell
                            let dispensireis = Search_Dips_List[0].nearby[indexPath.row]
                            
                            cell.disp_name_lbl.text = dispensireis.dispensaryName ?? ""
                            cell.dipsdistance_lbl.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                            cell.star_lbl.text = "\(dispensireis.rating ?? 0).0"
                            
                            let dispensries_img_S = dispensireis.profilePicture
                            let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            cell.disp_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                      placeholderImage: UIImage(named: ""),
                                                      options: .refreshCached,
                                                      completed: nil)
                            
                            let is_open = dispensireis.isOpen ?? 0
                            
                            if is_open == 0
                            {
                                cell.disp_status_lbl.text = "Close"
                                cell.Disp_time_lbl.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            }
                            else
                            {
                                cell.disp_status_lbl.text = "Open"
                                cell.Disp_time_lbl.text = "Close \(dispensireis.realClosingTime ?? "")"
                            }
                            
                            cell.buds_count_lbl.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                            cell.CollectBuds_btn.tag  = indexPath.row
                            cell.CollectBuds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                            cell.follow_btn.tag = indexPath.row
                            cell.follow_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                            
                            let follow = dispensireis.follow ?? 0
                            
                            if follow == 0
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                            }
                            else if follow == 1
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            else if follow == 2
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            return cell
                        }
                        else
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDisCell", for: indexPath) as! NoDisCell
                            
                            return cell
                        }
                    }
                    else
                    {
                        if Search_Dips_List[0].followed.count != 0
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseSecondCell", for: indexPath) as! BrowseSecondCell
                            
                            let dispensireis = Search_Dips_List[0].followed[indexPath.row]
                            
                            cell.disp_name_lbl.text = dispensireis.dispensaryName ?? ""
                            cell.dipsdistance_lbl.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                            cell.star_lbl.text = "\(dispensireis.rating ?? 0).0"
                            
                            let dispensries_img_S = dispensireis.profilePicture
                            let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            cell.disp_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                      placeholderImage: UIImage(named: ""),
                                                      options: .refreshCached,
                                                      completed: nil)
                            
                            let is_open = dispensireis.isOpen ?? 0
                            
                            if is_open == 0
                            {
                                cell.disp_status_lbl.text = "Close"
                                cell.Disp_time_lbl.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            }
                            else
                            {
                                cell.disp_status_lbl.text = "Open"
                                cell.Disp_time_lbl.text = "Close \(dispensireis.realClosingTime ?? "")"
                            }
                            
                            cell.buds_count_lbl.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                            cell.CollectBuds_btn.tag  = indexPath.row
                            cell.CollectBuds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                            cell.follow_btn.tag = indexPath.row
                            cell.follow_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                            
                            let follow = dispensireis.follow ?? 0
                            
                            if follow == 0
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                            }
                            else if follow == 1
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            else if follow == 2
                            {
                                cell.follow_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            
                            return cell
                        }
                        else
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDisCell", for: indexPath) as! NoDisCell
                            
                            return cell
                        }
                    }
                    
                    
                }
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoDisCell", for: indexPath) as! NoDisCell
                
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 99
        }
        else
        {
            if come_from == "Browse"
            {
                if Search_Dips_List.count != 0
                {
                    if select_value == 0
                    {
                        if Search_Dips_List[0].followed.count != 0
                        {
                            return UITableView.automaticDimension
                        }
                        else
                        {
                            return 175
                        }
                    }
                    else if select_value == 1
                    {
                        if Search_Dips_List[0].nearby.count != 0
                        {
                            return UITableView.automaticDimension
                        }
                        else
                        {
                            return 175
                        }
                    }
                    else
                    {
                        return 175
                    }
                }
                else
                {
                    return 175
                }
            }
            else
            {
                return 175
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if come_from == "Browse"
        {
            if Search_Dips_List.count != 0
            {
                if select_value == 0
                {
                    if Search_Dips_List[0].followed.count != 0
                    {
                        let dict = Search_Dips_List[0].followed[indexPath.row]
                        
                        let dispensary_profile = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as! DispenseryDetailsView
                        dispensary_profile.modalPresentationStyle = .fullScreen
                        dispensary_profile.dispensaryID = "\(dict.id ?? 0)"
                        self.present(dispensary_profile, animated: true, completion: nil)
                    }
                    else
                    {
                       
                    }
                }
                else if select_value == 1
                {
                    if Search_Dips_List[0].nearby.count != 0
                    {
                        let dict = Search_Dips_List[0].nearby[indexPath.row]
                        
                        let dispensary_profile = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as! DispenseryDetailsView
                        dispensary_profile.modalPresentationStyle = .fullScreen
                        dispensary_profile.dispensaryID = "\(dict.id ?? 0)"
                        self.present(dispensary_profile, animated: true, completion: nil)
                    }
                    else
                    {
                       
                    }
                }
            }
            else
            {
                
            }
        }
        else
        {
            if select_value == 0
            {
                if Search_Dips_List[0].nearby.count != 0
                {
                    let dict = Search_Dips_List[0].nearby[indexPath.row]
                    
                    let dispensary_profile = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as! DispenseryDetailsView
                    dispensary_profile.modalPresentationStyle = .fullScreen
                    dispensary_profile.dispensaryID = "\(dict.id ?? 0)"
                    self.present(dispensary_profile, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
            else
            {
                if Search_Dips_List[0].followed.count != 0
                {
                    let dict = Search_Dips_List[0].followed[indexPath.row]
                    
                    let dispensary_profile = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as! DispenseryDetailsView
                    dispensary_profile.modalPresentationStyle = .fullScreen
                    dispensary_profile.dispensaryID = "\(dict.id ?? 0)"
                    self.present(dispensary_profile, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
        }
        
    }
    
    func Update_location(select_user_lat: String, select_user_long: String, select_Address: String)
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        self.search_lng = select_user_lat
        self.search_long = select_user_long
        self.address_txt.text = select_Address
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: select_user_lat, long: select_user_long, search_txt: "")
    }
    
    
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        homeDelegate?.HomeAPI()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func search_btn_action(_ sender: Any)
    {
        
    }
    
    @objc func show_Like_action(sender:UIButton)
    {
        if come_from == "Browse"
        {
            if select_value == 0
            {
                let dispensireis =  Search_Dips_List[0].followed[sender.tag]
                let dispensaries_id = "\(dispensireis.id ?? 0)"
                let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
                let follow_id = dispensireis.follow ?? 0
                let dispensaries_name = dispensireis.dispensaryName ?? ""
                let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
                follow_dis.user_id = id
                follow_dis.follow = follow_id
                follow_dis.despensaries_id = dispensaries_id
                follow_dis.index_value = sender.tag
                follow_dis.dispensary_name = dispensaries_name
                follow_dis.modalPresentationStyle = .fullScreen
                follow_dis.come_from = "searchList"
                follow_dis.new_come_view = "Browse"
                follow_dis.dis_delgate = self
                follow_dis.search_Disp_value = self.select_value
                self.present(follow_dis, animated: true, completion: nil)
            }
            else
            {
                let dispensireis =  Search_Dips_List[0].nearby[sender.tag]
                let dispensaries_id = "\(dispensireis.id ?? 0)"
                let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
                let follow_id = dispensireis.follow ?? 0
                let dispensaries_name = dispensireis.dispensaryName ?? ""
                let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
                follow_dis.user_id = id
                follow_dis.follow = follow_id
                follow_dis.despensaries_id = dispensaries_id
                follow_dis.index_value = sender.tag
                follow_dis.dispensary_name = dispensaries_name
                follow_dis.modalPresentationStyle = .fullScreen
                follow_dis.come_from = "searchList"
                follow_dis.new_come_view = "Browse"
                follow_dis.search_Disp_value = self.select_value
                follow_dis.dis_delgate = self
                self.present(follow_dis, animated: true, completion: nil)
            }
        }
        else
        {
            if select_value == 0
            {
                let dispensireis =  Search_Dips_List[0].nearby[sender.tag]
                let dispensaries_id = "\(dispensireis.id ?? 0)"
                let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
                let follow_id = dispensireis.follow ?? 0
                let dispensaries_name = dispensireis.dispensaryName ?? ""
                let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
                follow_dis.user_id = id
                follow_dis.follow = follow_id
                follow_dis.despensaries_id = dispensaries_id
                follow_dis.index_value = sender.tag
                follow_dis.dispensary_name = dispensaries_name
                follow_dis.modalPresentationStyle = .fullScreen
                follow_dis.come_from = "searchList"
                follow_dis.new_come_view = ""
                follow_dis.search_Disp_value = self.select_value
                follow_dis.dis_delgate = self
                self.present(follow_dis, animated: true, completion: nil)
            }
            else
            {
                let dispensireis =  Search_Dips_List[0].followed[sender.tag]
                let dispensaries_id = "\(dispensireis.id ?? 0)"
                let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
                let follow_id = dispensireis.follow ?? 0
                let dispensaries_name = dispensireis.dispensaryName ?? ""
                let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
                follow_dis.user_id = id
                follow_dis.follow = follow_id
                follow_dis.despensaries_id = dispensaries_id
                follow_dis.index_value = sender.tag
                follow_dis.dispensary_name = dispensaries_name
                follow_dis.modalPresentationStyle = .fullScreen
                follow_dis.come_from = "searchList"
                follow_dis.new_come_view = ""
                follow_dis.search_Disp_value = self.select_value
                follow_dis.dis_delgate = self
                self.present(follow_dis, animated: true, completion: nil)
            }
        }
        
        
    }
    
    @objc func show_collect_buds(sender:UIButton)
    {
        if come_from == "Browse"
        {
            if select_value == 0
            {
                let pos = Search_Dips_List[0].followed[sender.tag].pos
                
                if pos == 0
                {
                    let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
                    collect.modalPresentationStyle = .fullScreen
                    collect.come_from = "searchlist"
                    collect.new_come = "Browse"
                    collect.select_value = self.select_value
                    collect.followed_search = Search_Dips_List[0].followed[sender.tag]
                    self.present(collect, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
            else
            {
                let pos = Search_Dips_List[0].nearby[sender.tag].pos
                
                if pos == 0
                {
                    let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
                    collect.modalPresentationStyle = .fullScreen
                    collect.come_from = "searchlist"
                    collect.new_come = "Browse"
                    collect.select_value = self.select_value
                    collect.nearby_search = Search_Dips_List[0].nearby[sender.tag]
                    self.present(collect, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
        }
        else
        {
            if select_value == 0
            {
                let pos = Search_Dips_List[0].nearby[sender.tag].pos
                
                if pos == 0
                {
                    let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
                    collect.modalPresentationStyle = .fullScreen
                    collect.come_from = "searchlist"
                    collect.new_come = ""
                    collect.select_value = self.select_value
                    collect.nearby_search = Search_Dips_List[0].nearby[sender.tag]
                    self.present(collect, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
            else
            {
                let pos = Search_Dips_List[0].followed[sender.tag].pos
                
                if pos == 0
                {
                    let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
                    collect.modalPresentationStyle = .fullScreen
                    collect.come_from = "searchlist"
                    collect.new_come = ""
                    collect.select_value = self.select_value
                    collect.followed_search = Search_Dips_List[0].followed[sender.tag]
                    self.present(collect, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
        }
        
        
        
        
    }
    
    func Dipensary_name(ind_val: Int, send_id: Int)
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        if self.search_lng.isEmpty == true && self.search_long.isEmpty == true
        {
            
                  if CheckLocation().check_location() == false
                  {
                      
                  }
                  else
                  {
                      self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", search_txt: self.saerch_Txt.text!)
                  }
        }
        else
        {
            
                  if CheckLocation().check_location() == false
                  {
                      
                  }
                  else
                  {
                    self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: self.search_lng, long: self.search_long, search_txt: self.saerch_Txt.text!)
                  }
        }
        
      
       // self.search_table_view.reloadData()
    }
    
    @IBAction func filter_btn_action(_ sender: Any)
    {
        if come_from == "Browse"
        {
            let Drawer_list = self.storyboard?.instantiateViewController(withIdentifier: "DrawerListView") as! DrawerListView
            Drawer_list.come_from = "Browse"
            Drawer_list.modalPresentationStyle = .fullScreen
            self.present(Drawer_list, animated: true, completion: nil)
        }
        else
        {
            let Drawer_list = self.storyboard?.instantiateViewController(withIdentifier: "DrawerListView") as! DrawerListView
            Drawer_list.come_from = ""
            Drawer_list.modalPresentationStyle = .fullScreen
            self.present(Drawer_list, animated: true, completion: nil)
        }
        
        
    }
    
    @objc func nearby_me_Action()
    {
        self.markers.removeAll()
        self.map_view.clear()
        
        if Search_Dips_List.count != 0
               {
                 
        
        for i in 0..<Search_Dips_List[0].nearby.count
        {
            let dict_loc = Search_Dips_List[0].nearby[i]
            
            let lat = dict_loc.lat ?? 0.0
            let long = dict_loc.lng ?? 0.0
            
            if ((lat == 0.0)||(long == 0.0))
            {
                
            }
            else
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 6)
                {
                    
                    let state_marker = GMSMarker()
                    state_marker.position = CLLocationCoordinate2D(latitude:lat, longitude: long)
                    state_marker.icon = UIImage.init(named:"markericon")
                    state_marker.map = self.map_view
                    self.markers.append(state_marker)
                    
                }
            }
            
        }
        }
        
        self.select_value = 1
        self.search_table_view.reloadData()
    }
    
    @objc func following_me_action()
    {
        self.markers.removeAll()
        self.map_view.clear()
        
        if Search_Dips_List.count != 0
        {
            
    
        for i in 0..<Search_Dips_List[0].followed.count
        {
            let dict_loc = Search_Dips_List[0].followed[i]
            
            let lat = dict_loc.lat ?? 0.0
            let long = dict_loc.lng ?? 0.0
            
            if ((lat == 0.0)||(long == 0.0))
            {
                
            }
            else
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 6)
                {
                    
                    let state_marker = GMSMarker()
                    state_marker.position = CLLocationCoordinate2D(latitude:lat, longitude: long)
                    state_marker.icon = UIImage.init(named:"markericon")
                    state_marker.map = self.map_view
                    self.markers.append(state_marker)
                    
                }
            }
        }
        }
        
        
        self.select_value = 0
        self.search_table_view.reloadData()
    }
    
    @objc func Discover_action()
    {
        self.markers.removeAll()
        self.map_view.clear()
        
        if Search_Dips_List.count != 0
        {
        
        for i in 0..<Search_Dips_List[0].nearby.count
        {
            let dict_loc = Search_Dips_List[0].nearby[i]
            
            let lat = dict_loc.lat ?? 0.0
            let long = dict_loc.lng ?? 0.0
            
            if ((lat == 0.0)||(long == 0.0))
            {
                
            }
            else
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 6)
                {
                    
                    let state_marker = GMSMarker()
                    state_marker.position = CLLocationCoordinate2D(latitude:lat, longitude: long)
                    state_marker.icon = UIImage.init(named:"markericon")
                    state_marker.map = self.map_view
                    self.markers.append(state_marker)
                    
                }
            }
        }
        }
        self.select_value = 0
        self.search_table_view.reloadData()
    }
    
    @objc func New_following_me()
    {
        
        self.markers.removeAll()
        self.map_view.clear()
        
        if Search_Dips_List.count != 0
        {
            for i in 0..<Search_Dips_List[0].followed.count
            {
                let dict_loc = Search_Dips_List[0].followed[i]
                
                let lat = dict_loc.lat ?? 0.0
                let long = dict_loc.lng ?? 0.0
                
                if ((lat == 0.0)||(long == 0.0))
                {
                    
                }
                else
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 6)
                    {
                        
                        let state_marker = GMSMarker()
                        state_marker.position = CLLocationCoordinate2D(latitude:lat, longitude: long)
                        state_marker.icon = UIImage.init(named:"markericon")
                        state_marker.map = self.map_view
                        self.markers.append(state_marker)
                        
                    }
                }
        }
        }
        else
        {
            
        }
        
        self.select_value = 1
        self.search_table_view.reloadData()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    // Debugging - All Swipes Are Detected Now
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                
                if self.table_originx.constant == 0.0
                {
                    self.table_originx.constant = (self.view.frame.size.height - 157.0) - 340.0
                }
                else
                {
                    
                }
                
            case  UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
                
                if self.table_originx.constant > 0.0
                {
                    self.table_originx.constant = 0.0
                }
                else
                {
                    
                }
                
            default:
                break
            }
        }
    }
    
    
    //# Dispensary List
    
    func Dispensary_list(User_id:String,lat:String,long:String,search_txt:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().browse_disp)")
            
            let dict  = [APIKeys().user_id:User_id,APIKeys().search_lat:lat,APIKeys().search_lng:long,APIKeys().search_key:search_txt]
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                Search_Dips_List = []
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "staus") as? Int ?? 0
                    
                    if response_status == 200
                    {
                        
                        let reason = response as? [String:Any] ?? [:]
                        Search_Dips_List.append(SearchDispensary.init(fromDictionary: reason))
                        
                        self.search_table_view.reloadData()
                        
                        if self.come_from == ""
                        {
                            for i in 0..<Search_Dips_List[0].nearby.count
                            {
                                let dict_loc = Search_Dips_List[0].nearby[i]
                                
                                let lat = dict_loc.lat ?? 0.0
                                let long = dict_loc.lng ?? 0.0
                                
                                if ((lat == 0.0)||(long == 0.0))
                                {
                                    
                                }
                                else
                                {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 6)
                                    {
                                        
                                        let state_marker = GMSMarker()
                                        state_marker.position = CLLocationCoordinate2D(latitude:lat, longitude: long)
                                        state_marker.icon = UIImage.init(named:"markericon")
                                        state_marker.map = self.map_view
                                        self.markers.append(state_marker)
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            for i in 0..<Search_Dips_List[0].followed.count
                            {
                                let dict_loc = Search_Dips_List[0].followed[i]
                                
                                let lat = dict_loc.lat ?? 0.0
                                let long = dict_loc.lng ?? 0.0
                                
                                if ((lat == 0.0)||(long == 0.0))
                                {
                                    
                                }
                                else
                                {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 6)
                                    {
                                        
                                        let state_marker = GMSMarker()
                                        state_marker.position = CLLocationCoordinate2D(latitude:lat, longitude: long)
                                        state_marker.icon = UIImage.init(named:"markericon")
                                        state_marker.map = self.map_view
                                        self.markers.append(state_marker)
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        
                    }
                    else
                    {
                        
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    @IBAction func address_btn_action(_ sender: Any)
    {
        let refer = self.storyboard?.instantiateViewController(withIdentifier: "LocationView") as! LocationView
        refer.modalPresentationStyle = .fullScreen
        refer.update_loc = self
        self.present(refer, animated: true, completion: nil)
    }
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    // Below Mehtod will print error if not able to update location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error Location")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Access the last object from locations to get perfect current location
        if let location = locations.last {
            
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            
            geocode(latitude: myLocation.latitude, longitude: myLocation.longitude) { placemark, error in
                guard let placemark = placemark, error == nil else { return }
                // you should always update your UI in the main thread
                DispatchQueue.main.async {
                    //  update UI here
                    print("address1:", placemark.thoroughfare ?? "")
                    print("address2:", placemark.subThoroughfare ?? "")
                    print("city:",     placemark.locality ?? "")
                    print("state:",    placemark.administrativeArea ?? "")
                    print("zip code:", placemark.postalCode ?? "")
                    print("country:",  placemark.country ?? "")
                }
            }
        }
        manager.stopUpdatingLocation()
        
    }
    
    
}
