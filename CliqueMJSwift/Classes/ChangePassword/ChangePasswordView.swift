//
//  ChangePasswordView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class ChangePasswordView: UIViewController {

    @IBOutlet weak var confirm_pass_txt: CustomUITextField!
    @IBOutlet weak var new_pass_Txt: CustomUITextField!
    @IBOutlet weak var current_pass_txt: CustomUITextField!
    @IBOutlet weak var no_btn: KButton!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var border_customView: UIView!
    @IBOutlet weak var user_pick_btn: UIButton!
    @IBOutlet weak var user_img: UIImageView!
    @IBOutlet weak var user_view: UIView!
    var errormessage = ""
    var current_show = 0
    var old_show = 0
    var confirm_show = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        self.border_customView.clipsToBounds = true
        self.border_customView.layer.cornerRadius = 40
        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
    
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        let dispensries_img_S = userInformation.value(forKey: "profile_picture") as? String ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.user_img.sd_setImage(with: URL(string:disp_img ?? ""),
               placeholderImage: UIImage(named: "avatar_icon"),
               options: .refreshCached,
               completed: nil)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true
            , completion: nil)
    }
    
    @objc func Save_btn_Action(sender:UIButton)
    {
       
    }
    @IBAction func save_action(_ sender: Any)
    {
        if self.CheckValidation() == false
        {
            self.alertmessage(error:"", message: self.errormessage)
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                   print(user_data)
                
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            self.ChangePasswordAPI(user_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", old_pass: self.current_pass_txt.text!, new_pass: self.new_pass_Txt.text!)
        }
        
    }
    @IBAction func no_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true
        , completion: nil)
    }
    @IBAction func current_pass_show_action(_ sender: Any)
    {
        if old_show == 0
        {
            old_show = 1
            self.current_pass_txt.isSecureTextEntry = false
        }
        else
        {
            old_show = 0
            self.current_pass_txt.isSecureTextEntry = true
        }
    }
    
    @IBAction func new_pass_show_action(_ sender: Any)
    {
        if current_show == 0
        {
            current_show = 1
            self.new_pass_Txt.isSecureTextEntry = false
        }
        else
        {
            current_show = 0
            self.new_pass_Txt.isSecureTextEntry = true
        }
    }
    
    @IBAction func confirm_pass_show_action(_ sender: Any)
    {
        if confirm_show == 0
        {
            confirm_show = 1
            self.confirm_pass_txt.isSecureTextEntry = false
        }
        else
        {
            confirm_show = 0
            self.confirm_pass_txt.isSecureTextEntry = true
        }
    }
    
    func CheckValidation() -> Bool
    {
        if self.current_pass_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().current_error
            return false
        }
        if passwordSpace().validate(string: self.current_pass_txt.text!) ==  false
        {
            self.errormessage = ResponseMessage().currrent_sace
            return false
        }
        if self.new_pass_Txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().new_error
            return false
        }
        if passwordSpace().validate(string: self.new_pass_Txt.text!) ==  false
        {
            self.errormessage = ResponseMessage().new_sace
            return false
        }
        if self.new_pass_Txt.text!.count < 6 || self.new_pass_Txt.text!.count > 20
        {
            self.errormessage = ResponseMessage().password_length
            return false
        }
        if self.confirm_pass_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().confirm_error
            
            return false
        }
        if self.new_pass_Txt.text! != self.confirm_pass_txt.text!
        {
            self.errormessage = ResponseMessage().password_m
            return false
        }
        
        self.errormessage = ""
        return true
    }
    
    func ChangePasswordAPI(user_id:String,old_pass:String,new_pass:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().change_password)")
            
            let dict  = [APIKeys().user_id:user_id,APIKeys().old_pass:old_pass,APIKeys().new_pass:new_pass]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "status") as? Int ?? 0
                    
                    if response_status == 200
                    {
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.Successalertmessage(error: "", message: msg)
                    }
                    else
                    {
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //# Success Alert Messages
       
       func Successalertmessage(error:String,message:String)
       {
           let alertController = UIAlertController(title: error,
                                                   message: message,
                                                   preferredStyle: .alert)
           
           let defaultAction = UIAlertAction(title: "OK", style: .default, handler: successAction)
           alertController.addAction(defaultAction)
           
           self.present(alertController, animated: true, completion: nil)
       }
    
    func successAction(sender:UIAlertAction)
    {
        UserDefaults.standard.removeObject(forKey: "user_info")
        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LoginView
        login.modalPresentationStyle = .fullScreen
        self.present(login, animated: true, completion: nil)
    }
    
}
