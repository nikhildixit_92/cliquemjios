//
//  CollectBudsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 07/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class CollectBudsView: UIViewController {

    @IBOutlet weak var check_icon_img: UIImageView!
    @IBOutlet weak var code_txt: CustomUITextField!
    @IBOutlet weak var code_view: UIView!
    @IBOutlet weak var amount_txt: CustomUITextField!
    @IBOutlet weak var amount_view: UIView!
    @IBOutlet weak var no_btn: UIButton!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var custom_view: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        // Do any additional setup after loading the view.
    }

    @IBAction func save_btn_Action(_ sender: Any)
    {
        let dis = self.storyboard?.instantiateViewController(withIdentifier: "successLoyality") as! successLoyality
        self.present(dis, animated: true, completion: nil)
    }
    
    @IBAction func no_btn_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func check_dis_action(_ sender: Any)
    {
        
    }
}
