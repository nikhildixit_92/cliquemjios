//
//  DealBannerNewCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

protocol cliqDeal
{
    func Clique_Deal_action(data:CliqueDeal?)
}

class DealBannerNewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var more_Deal_btn: UIButton!
    @IBOutlet weak var food_table_height: NSLayoutConstraint!
    var cliqueDeDelagate:cliqDeal?
    @IBOutlet weak var food_table: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nib = UINib.init(nibName: "DealBannerNewCollCell", bundle: nil)
        self.food_table.register(nib, forCellWithReuseIdentifier: "DealBannerNewCollCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if  DispProfile_List.count != 0
        {
             return DispProfile_List[0].cliqueDeals.count
        }
        else
        {
            return 0
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealBannerNewCollCell", for: indexPath) as! DealBannerNewCollCell
        
        if  DispProfile_List.count != 0
        {
            if DispProfile_List[0].cliqueDeals.count != 0
            {
                let dict = DispProfile_List[0].cliqueDeals[indexPath.row]
                
                let dispensries_img_S = dict.logo ?? ""
                let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                cell.Deal_icon.sd_setImage(with: URL(string:disp_img ?? ""),
                                          placeholderImage: UIImage(named: ""),
                                          options: .refreshCached,
                                          completed: nil)
                
                cell.about_Txt.text = dict.about ?? ""
                cell.title_txt.text = dict.title ?? ""
            }
            else
            {
                
            }
            
        }
        else
        {
            
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.food_table.frame.size.width, height: 130.0)
        
       // return CGSize.init(width: self.food_table.frame.size.width, height: self.food_table.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
         let dict = DispProfile_List[0].cliqueDeals[indexPath.row]
        
        cliqueDeDelagate?.Clique_Deal_action(data: dict)
        
    }
    
}
