//
//  DealCollectionView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DealCollectionView: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    

    @IBOutlet weak var page_index: UIPageControl!
    @IBOutlet weak var deal_collection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nib = UINib.init(nibName: "DealCollCell", bundle: nil)
        self.deal_collection.register(nib, forCellWithReuseIdentifier: "DealCollCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealCollCell", for: indexPath) as! DealCollCell
        
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.deal_collection.frame.size.width, height: self.deal_collection.frame.size.height)
    }
    
}
