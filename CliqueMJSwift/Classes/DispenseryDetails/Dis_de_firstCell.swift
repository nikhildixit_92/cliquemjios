//
//  Dis_de_firstCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import Cosmos
class Dis_de_firstCell: UITableViewCell {

    @IBOutlet var background_img: UIImageView!
    @IBOutlet weak var followtxt: UILabel!
    @IBOutlet weak var unlike_icon: UIImageView!
    @IBOutlet weak var time_lbl: UILabel!
    @IBOutlet weak var loyality_buds_lbl: UILabel!
    @IBOutlet weak var we_deliver_txt: UILabel!
    @IBOutlet weak var open_time: UILabel!
    @IBOutlet weak var address_view: UILabel!
    @IBOutlet weak var star_view: CosmosView!
    @IBOutlet weak var rating_lbl: UILabel!
    @IBOutlet weak var disp_name_lbl: UILabel!
    @IBOutlet weak var follow_btn: UIButton!
    @IBOutlet weak var Dispensaries_img: UIImageView!
    @IBOutlet weak var share_btn: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var collect_buds_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
