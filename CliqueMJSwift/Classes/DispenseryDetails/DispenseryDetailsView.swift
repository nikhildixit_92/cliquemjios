//
//  DispenseryDetailsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class DispenseryDetailsView: UIViewController,UITableViewDelegate,UITableViewDataSource,DispensaryNameShow,homeUpdate_API,cliqDeal,CallHomeAPI_Follow
{
    
   
    
    var rating_id = String()
    var show_more = false
    @IBOutlet weak var not_view_height: NSLayoutConstraint!
    var come_from = ""
    @IBOutlet weak var not_dis_btn: UIButton!
    @IBOutlet weak var not_view: UIView!
    var dispensaryID = String()
    @IBOutlet weak var dispensery_detaills_table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "Dis_de_firstCell", bundle: nil)
        self.dispensery_detaills_table.register(nib, forCellReuseIdentifier: "Dis_de_firstCell")

        let nib1 = UINib.init(nibName: "dis_de_secondCell", bundle: nil)
        self.dispensery_detaills_table.register(nib1, forCellReuseIdentifier: "dis_de_secondCell")
    
        let nib2 = UINib.init(nibName: "dis_de_thirdCell", bundle: nil)
        self.dispensery_detaills_table.register(nib2, forCellReuseIdentifier: "dis_de_thirdCell")
        
        let nib3 = UINib.init(nibName: "dis_de_forthCell", bundle: nil)
        self.dispensery_detaills_table.register(nib3, forCellReuseIdentifier: "dis_de_forthCell")
        
        let nib4 = UINib.init(nibName: "DealsCell", bundle: nil)
        self.dispensery_detaills_table.register(nib4, forCellReuseIdentifier: "DealsCell")
        
        let nib5 = UINib.init(nibName: "DealCollectionView", bundle: nil)
        self.dispensery_detaills_table.register(nib5, forCellReuseIdentifier: "DealCollectionView")
        
        let nib6 = UINib.init(nibName: "DealBannerCell", bundle: nil)
        self.dispensery_detaills_table.register(nib6, forCellReuseIdentifier: "DealBannerCell")
        
        let nib7 = UINib.init(nibName: "DealBannerNewCell", bundle: nil)
        self.dispensery_detaills_table.register(nib7, forCellReuseIdentifier: "DealBannerNewCell")
    
        let nib8 = UINib.init(nibName: "DealProductCell", bundle: nil)
        self.dispensery_detaills_table.register(nib8, forCellReuseIdentifier: "DealProductCell")
        
        let nib9 = UINib.init(nibName: "RatingCell", bundle: nil)
        self.dispensery_detaills_table.register(nib9, forCellReuseIdentifier: "RatingCell")
        
        let nib10 = UINib.init(nibName: "MJDealCell", bundle: nil)
        self.dispensery_detaills_table.register(nib10, forCellReuseIdentifier: "MJDealCell")
        
        
        self.dispensery_detaills_table.rowHeight = 474
        
        if come_from == "already"
        {
            self.not_view.isHidden = false
            self.not_view.clipsToBounds = true
            self.not_view_height.constant = 72.0
            self.not_view.layer.cornerRadius = 30
            self.not_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        else
        {
            self.not_view.isHidden = true
            self.not_view_height.constant = 0
           // self.dispensery_detaills_table.frame.size.height = self.view.frame.size.height
        }
        
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", dispensary_id:self.dispensaryID, User_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", User_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
        }
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 14
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 4
        {
            if DispProfile_List.count != 0
            {
                return DispProfile_List[0].loyaltyDeals.count
            }
            else
            {
                return 0
            }
        }
        else if section == 6
        {
            if DispProfile_List.count != 0
            {
                return  DispProfile_List[0].mjDeals.count
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Dis_de_firstCell", for: indexPath) as! Dis_de_firstCell
            
            cell.back_btn.addTarget(self, action: #selector(dismiss_action), for: .touchUpInside)
            cell.follow_btn.addTarget(self, action: #selector(Follow_btn_Action), for: .touchUpInside)
            cell.collect_buds_btn.addTarget(self, action: #selector(collect_buds_action), for: .touchUpInside)
            cell.share_btn.addTarget(self, action: #selector(share_dispensary), for: .touchUpInside)
            
            if DispProfile_List.count != 0
            {
                let dict = DispProfile_List[0].dispensary
                
                cell.disp_name_lbl.text = dict?.dispensaryName ?? ""
                let dispensries_img_S = dict?.profilePicture
                let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                cell.Dispensaries_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                placeholderImage: UIImage(named: ""),
                                                options: .refreshCached,
                                                completed: nil)
                
                
                let dispensries_img_S1 = dict?.backgroundImage
                let disp_img1 = dispensries_img_S1?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                cell.background_img.sd_setImage(with: URL(string:disp_img1 ?? ""),
                                                placeholderImage: UIImage(named: "back_img"),
                                                options: .refreshCached,
                                                completed: nil)
                
                
                let is_open = dict?.isOpen ?? 0
                
                if is_open == 0
                {
                    cell.open_time.text = "Close Now"
                    cell.time_lbl.text = " •\(dict?.realOpeningTime ?? "")"
                }
                else
                {
                    cell.open_time.text = "Open Now"
                    cell.time_lbl.text = " •\(dict?.realClosingTime ?? "")"
                }
                
                let follow = dict?.follow ?? 0

                if follow == 0
                {
                    cell.followtxt.text = "Follow"
                    cell.unlike_icon.image = UIImage.init(named:"New_unlike")
                }
                else if follow == 1
                {
                    cell.followtxt.text = "Following"
                    cell.unlike_icon.image = UIImage.init(named:"like_icon")
                   
                }
                else if follow == 2
                {
                    cell.followtxt.text = "Following"
                    cell.unlike_icon.image = UIImage.init(named:"like_icon")
                }
                
                cell.loyality_buds_lbl.text = "\(dict?.bud ?? 0)"
                cell.address_view.text = "\(dict?.distance ?? "") • \(dict?.address ?? "")"
                
                cell.rating_lbl.text = "\(dict?.rating ?? 0)"
                
                cell.star_view.rating = Double(dict?.rating ?? 0)
                
            }
            else
            {
                
            }
            
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_secondCell", for: indexPath) as! dis_de_secondCell
            
            cell.website_view.layer.cornerRadius = cell.website_view.frame.size.width/2
             cell.Call_View.layer.cornerRadius = cell.Call_View.frame.size.width/2
             cell.orderView.layer.cornerRadius = cell.orderView.frame.size.width/2
             cell.exchangeView.layer.cornerRadius = cell.exchangeView.frame.size.width/2
            
            
            cell.website_btn.addTarget(self, action: #selector(Website_open), for: .touchUpInside)
                       
            cell.exchange_btn.addTarget(self, action: #selector(TransferBudsView), for: .touchUpInside)

            cell.call_view.addTarget(self, action: #selector(order_view), for: .touchUpInside)
            
          
            
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_thirdCell", for: indexPath) as! dis_de_thirdCell
            
            return cell
            
        }
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
            
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].loyaltyDeals.count != 0
                {
                    cell.isHidden = false
                    cell.info_txt.text = "Loyalty Deals"
                }
                else
                {
                    cell.isHidden = true
                }
                
                
            }
            else
            {
                cell.info_txt.text = ""
                cell.isHidden = true
            }
            
            
          
            return cell
        }
        else if indexPath.section == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealsCell", for: indexPath) as! DealsCell
            
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].loyaltyDeals.count != 0
                {
                    cell.isHidden = false
                    let dict = DispProfile_List[0].loyaltyDeals[indexPath.row]
                    
                    let dispensries_img_S = dict.logo ?? ""
                    let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    cell.Deal_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                    placeholderImage: UIImage(named: ""),
                                                    options: .refreshCached,
                                                    completed: nil)
                    
                    cell.title.text = dict.title ?? ""
                    cell.buds_timer_lbl.text = "\(dict.bud ?? 0)"
                }
                else
                {
                    cell.isHidden = true
                }
          
                
            }
            else
            {
                cell.isHidden = true
            }
            
            
            
            return cell
        }
        else if indexPath.section == 5
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
            
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].mjDeals.count != 0
                {
                    cell.isHidden = false
                    cell.info_txt.text = "MJ Deals"
                }
                else
                {
                    cell.isHidden = true
                }
            }
            else
            {
                cell.isHidden = true
            }
            
            
            
            return cell
        }
        else if indexPath.section == 6
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MJDealCell", for: indexPath) as! MJDealCell
            
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].mjDeals.count != 0
                {
                    cell.isHidden = false
                    let dict = DispProfile_List[0].mjDeals[indexPath.row]
                    
                    let dispensries_img_S = dict.logo ?? ""
                    let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    cell.Deal_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                    placeholderImage: UIImage(named: ""),
                                                    options: .refreshCached,
                                                    completed: nil)
                    
                    cell.title.text = dict.title ?? ""
                    cell.buds_timer_lbl.text = "\(dict.bud ?? 0)"
                }
                else
                {
                    cell.isHidden = true
                
                }
                
                
                
            }
            else
            {
                cell.isHidden = true
            }
            
            return cell
        }
        else if indexPath.section == 7
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealCollectionView", for: indexPath) as! DealCollectionView
          
            cell.isHidden = true
            return cell
            
        }
        else if indexPath.section == 8
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
            
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].cliqueDeals.count != 0
                {
                    cell.isHidden = false
                    cell.info_txt.text = "Nearby Munchies"
                }
                else
                {
                    cell.isHidden = true
                   
                }
            }
            else
            {
                cell.isHidden = true
                cell.info_txt.text = ""
            }
            
            
            
            return cell
            
        }
        else if indexPath.section == 9
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealBannerNewCell", for: indexPath) as! DealBannerNewCell
            
            if show_more == false
            {
                if DispProfile_List.count != 0
                {
                    if DispProfile_List[0].cliqueDeals.count != 0
                    {
                        cell.isHidden = false
                        if let layout = cell.food_table.collectionViewLayout as? UICollectionViewFlowLayout {
                            layout.scrollDirection = .horizontal  // .horizontal
                        }
                        cell.cliqueDeDelagate = self
                        cell.food_table.reloadData()
                    }
                    else
                    {
                        
                        cell.isHidden = true
                        
                    }
                }
                else
                {
                    cell.isHidden = true
                    
                }
            }
            else
            {
                if DispProfile_List.count != 0
                {
                    if DispProfile_List[0].cliqueDeals.count != 0
                    {
                        cell.isHidden = false
                        if let layout = cell.food_table.collectionViewLayout as? UICollectionViewFlowLayout {
                            layout.scrollDirection = .vertical  // .horizontal
                        }
                        
                        cell.cliqueDeDelagate = self
                        cell.food_table.reloadData()
                        
                        cell.food_table_height.constant = CGFloat(DispProfile_List[0].cliqueDeals.count*130)
                    }
                    else
                    {
                        cell.isHidden = true
                        
                    }
                }
                else
                {
                    cell.isHidden = true
                    
                }
            }
            
            
            cell.more_Deal_btn.addTarget(self, action: #selector(show_more_deal), for: .touchUpInside)
            
            
            return cell
            
                
        }
        else if indexPath.section == 10
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealBannerCell", for: indexPath) as! DealBannerCell
            
             return cell
        
        }
        else if indexPath.section == 11
        {
              let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
                
                cell.info_txt.text = "Popular Products"
                
                return cell
        }
        else if indexPath.section == 12
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealProductCell", for: indexPath) as! DealProductCell
            
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingCell", for: indexPath) as! RatingCell
            
            
            if DispProfile_List.count != 0
            {
                cell.star1btn.addTarget(self, action: #selector(star_1_action), for: .touchUpInside)
                             cell.star2btn.addTarget(self, action: #selector(Star_2_action), for: .touchUpInside)
                             cell.star3btn.addTarget(self, action: #selector(Star_3_action), for: .touchUpInside)
                             cell.star4btn.addTarget(self, action: #selector(Star_4_action), for: .touchUpInside)
                             cell.star5Btn.addTarget(self, action: #selector(Star_5_action), for: .touchUpInside)
                             
                              let dict = DispProfile_List[0].dispensary
                 
                             cell.total_rating.text = "\(dict?.rating ?? 0).0"
                 
                             if rating_id == "1"
                             {
                                 //star
                                 cell.star_1Img.image = UIImage.init(named:"yellow_star")
                                 cell.star_2Img.image = UIImage.init(named:"star")
                                 cell.star3_img.image = UIImage.init(named:"star")
                                 cell.star4Img.image = UIImage.init(named:"star")
                                 cell.star_5Img.image = UIImage.init(named:"star")
                 
                             }
                             else if rating_id == "2"
                             {
                                 cell.star_1Img.image = UIImage.init(named:"yellow_star")
                                 cell.star_2Img.image = UIImage.init(named:"star")
                                 cell.star3_img.image = UIImage.init(named:"star")
                                 cell.star4Img.image = UIImage.init(named:"star")
                                 cell.star_5Img.image = UIImage.init(named:"star")
                             }
                             else if rating_id == "3"
                             {
                                 cell.star_1Img.image = UIImage.init(named:"yellow_star")
                                 cell.star_2Img.image = UIImage.init(named:"star")
                                 cell.star3_img.image = UIImage.init(named:"star")
                                 cell.star4Img.image = UIImage.init(named:"star")
                                 cell.star_5Img.image = UIImage.init(named:"star")
                             }
                             else if rating_id == "4"
                             {
                                 cell.star_1Img.image = UIImage.init(named:"yellow_star")
                                 cell.star_2Img.image = UIImage.init(named:"star")
                                 cell.star3_img.image = UIImage.init(named:"star")
                                 cell.star4Img.image = UIImage.init(named:"star")
                                 cell.star_5Img.image = UIImage.init(named:"star")
                             }
                             else if rating_id == "5"
                             {
                                 cell.star_1Img.image = UIImage.init(named:"yellow_star")
                                 cell.star_2Img.image = UIImage.init(named:"star")
                                 cell.star3_img.image = UIImage.init(named:"star")
                                 cell.star4Img.image = UIImage.init(named:"star")
                                 cell.star_5Img.image = UIImage.init(named:"star")
                             }
                
            }
            else
            {
                
            }
            
           
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
             return 449
        }
        else if indexPath.section == 1
        {
             return 137
        }
        else if indexPath.section == 2
        {
            return 263
        }
        else if indexPath.section == 3
        {
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].loyaltyDeals.count != 0
                {
                    return 82
                }
                else
                {
                    return 0
            
                }
            }
            else
            {
                 return 0
            }
        }
        else if indexPath.section == 4
        {
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].loyaltyDeals.count != 0
                {
                    return 141
                }
                else
                {
                    return 0
            
                }
            }
            else
            {
                 return 0
            }
        }
        else if indexPath.section == 5
        {
            if DispProfile_List.count != 0
            {
                if DispProfile_List[0].mjDeals.count != 0
                {
                    return 82
                }
                else
                {
                    return 0
            
                }
            }
            else
            {
                 return 0
            }
            
        }
        else if indexPath.section == 6
        {
           if DispProfile_List.count != 0
            {
                if DispProfile_List[0].mjDeals.count != 0
                {
                    return 141
                }
                else
                {
                    return 0
            
                }
            }
            else
            {
                 return 0
            }
        }
       else if indexPath.section == 7
        {
            return 0
        }
        else if indexPath.section == 8
        {
             if DispProfile_List.count != 0
             {
                if DispProfile_List[0].cliqueDeals.count != 0
                {
                    return 82
                }
                else
                {
                    return 0
                    
                }
             }
             else
             {
                return 0
            }
        }
        else if indexPath.section == 9
        {
              if DispProfile_List.count != 0
              {
                if DispProfile_List[0].cliqueDeals.count != 0
                {
                    if show_more == false
                    {
                        return 214
                    }
                    else
                    {
                        return UITableView.automaticDimension
                    }
                    
                    
                }
                else
                {
                    return 0
                    
                }
              }
              else
              {
                return 0
            }
        }
        else if indexPath.section == 10
        {
            return  135
        }
        else if indexPath.section == 11
        {
            return 0
            //82
        }
        else if indexPath.section == 12
        {
            return 0
            //279
        }
        else
        {
            return 95
            //95
        }
    }
    
    @objc func show_more_deal()
    {
        if show_more == false
        {
            self.show_more = true
            self.dispensery_detaills_table.reloadData()
        }
        else
        {
            self.show_more = false
            self.dispensery_detaills_table.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 4
        {
             let dispensireis = DispProfile_List[0].dispensary
            let dict = DispProfile_List[0].loyaltyDeals[indexPath.row]
            
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityDealView") as! LoyalityDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensay_id  = "\(dict.dispensaryId ?? 0)"
            loyality.inde_val = indexPath.row
            loyality.come_from = "homeprofiledata"
            loyality.redeem_profile = dict
            loyality.new_buds = dispensireis?.bud ?? 0
            self.present(loyality, animated: true, completion: nil)
        }
        else if indexPath.section == 6
        {
              let dispensireis = DispProfile_List[0].dispensary
            
            let dict = DispProfile_List[0].mjDeals[indexPath.row]
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "MJDealView") as! MJDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensay_id = "\(dict.dispensaryId ?? 0)"
            loyality.inde_val = indexPath.row
            loyality.Mj_deal_lsit = dict
            loyality.come_from = "homeprofilelist"
            loyality.new_buds = dispensireis?.bud ?? 0
            self.present(loyality, animated: true, completion: nil)
        }
    }
    
    func Clique_Deal_action(data: CliqueDeal?)
    {
        
         let dispensireis = DispProfile_List[0].dispensary
        
        if dispensireis?.follow == 0
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "FollowRedeemView") as! FollowRedeemView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensary_id = "\(dispensireis?.id ?? 0)"
            loyality.ind_value = 0
            loyality.come_from = "homeprofiledeal"
            loyality.profile_cliqueDeal = data
            loyality.Home_APIDelegate = self
            self.present(loyality, animated: true, completion: nil)
        }
        else
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "CliqueDealView") as! CliqueDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensary_id = "\(dispensireis?.id ?? 0)"
            loyality.ind_value = 0
            loyality.come_from = "newhomeprofiledeal"
            loyality.profile_cliqueDeal = data
            self.present(loyality, animated: true, completion: nil)
        }
        
          
    }
       
     func Hme_API()
     {
         let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
         print(user_data)
         let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
         
         if CheckLocation().check_location() == false
         {
             
         }
         else
         {
             self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", dispensary_id:self.dispensaryID, User_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", User_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
         }
     }
     
    
    
    @IBAction func not_dis_action(_ sender: Any)
    {
        let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "FollowDispeneryLocationEnableView") as! FollowDispeneryLocationEnableView
        self.present(follow_dis, animated: true, completion: nil)
    }
    
    @objc func dismiss_action(sender:UIButton)
    {
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        let navigation = UINavigationController.init(rootViewController: drawerC)
        navigation.setNavigationBarHidden(true, animated: true)
        appDelegate.window?.rootViewController = navigation
        appDelegate.window?.makeKeyAndVisible()
        
        //self.dismiss(animated: true, completion: nil)
    }
    
    @objc func collect_buds_action(sender:UIButton)
    {
        let dispensireis = DispProfile_List[0].dispensary
        
        let pos = DispProfile_List[0].dispensary.pos
        
        if pos == 0
        {
            let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
            collect.modalPresentationStyle = .overFullScreen
            collect.come_from = "profilecollectbuds"
            collect.update_API = self
            collect.profileSelectBuds = dispensireis
            self.present(collect, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    func update_HomeAPI() {
          
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", dispensary_id:self.dispensaryID, User_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", User_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
        }
      }
    
    
    @objc func Follow_btn_Action()
    {
       let dispensireis = DispProfile_List[0].dispensary
        let dispensaries_id = "\(dispensireis?.id ?? 0)"
        let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
        let follow_id = dispensireis?.follow ?? 0
        let dispensaries_name = dispensireis?.dispensaryName ?? ""
        let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
        follow_dis.user_id = id
        follow_dis.follow = follow_id
        follow_dis.despensaries_id = dispensaries_id
        follow_dis.index_value = 0
        follow_dis.dispensary_name = dispensaries_name
        follow_dis.modalPresentationStyle = .overFullScreen
        follow_dis.come_from = "homedetails"
        follow_dis.dis_delgate = self
        self.present(follow_dis, animated: true, completion: nil)
    }
    
    func Dipensary_name(ind_val: Int, send_id: Int)
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", dispensary_id:self.dispensaryID, User_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", User_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
        }
    }
    
    @objc func TransferBudsView()
    {
        if DispProfile_List.count != 0
        {
            let dispensireis = DispProfile_List[0].dispensary
                   
            let tranfer_view = self.storyboard?.instantiateViewController(withIdentifier: "TransferBudsView") as! TransferBudsView
            tranfer_view.modalPresentationStyle = .overFullScreen
            tranfer_view.transfer_budsDisp = dispensireis
            tranfer_view.come_from = "homeprofiledata"
            self.present(tranfer_view, animated: true, completion: nil)
        }
        else
        {
            
        }
       
    }
    
    @objc func star_1_action()
    {
        self.rating_id = "1"
        
        let dispensireis = DispProfile_List[0].dispensary
        let dispensaries_id = "\(dispensireis?.id ?? 0)"
        self.Dispensary_Rating(Rate_Id:self.rating_id,dispensary_Rating:dispensaries_id)
        
        self.dispensery_detaills_table.reloadData()
    }

    @objc func Star_2_action()
    {
        self.rating_id = "2"
        
        let dispensireis = DispProfile_List[0].dispensary
        let dispensaries_id = "\(dispensireis?.id ?? 0)"
        self.Dispensary_Rating(Rate_Id:self.rating_id,dispensary_Rating:dispensaries_id)
        self.dispensery_detaills_table.reloadData()
    }
    
    @objc func Star_3_action()
    {
       self.rating_id = "3"
       
       let dispensireis = DispProfile_List[0].dispensary
       let dispensaries_id = "\(dispensireis?.id ?? 0)"
       self.Dispensary_Rating(Rate_Id:self.rating_id,dispensary_Rating:dispensaries_id)
        self.dispensery_detaills_table.reloadData()
    }
    
    @objc func Star_4_action()
    {
       self.rating_id = "4"
        
        let dispensireis = DispProfile_List[0].dispensary
        let dispensaries_id = "\(dispensireis?.id ?? 0)"
        self.Dispensary_Rating(Rate_Id:self.rating_id,dispensary_Rating:dispensaries_id)
        self.dispensery_detaills_table.reloadData()
    }
    
    @objc func Star_5_action()
    {
       self.rating_id = "5"
        
        let dispensireis = DispProfile_List[0].dispensary
        let dispensaries_id = "\(dispensireis?.id ?? 0)"
        self.Dispensary_Rating(Rate_Id:self.rating_id,dispensary_Rating:dispensaries_id)
        self.dispensery_detaills_table.reloadData()
    }
    
    @objc func share_dispensary()
    {
        let share = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
        self.present(share, animated: true, completion: nil)
    }
    
    @objc func order_view(sender:UIButton)
    {
        
        let dispensireis = DispProfile_List[0].dispensary.userPhone ?? ""
        
        if dispensireis.isEmpty == true
        {
           self.alertmessage(error: "", message: "no nuber is available")
        }
        else
        {
           if let url = URL(string: "tel://\(dispensireis)"),
               UIApplication.shared.canOpenURL(url) {
                  if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               } else {
                       
                self.alertmessage(error: "", message: "Calling error")
            }
        }
       
    }
    
    @objc func Website_open()
    {
        
        let dispensireis = DispProfile_List[0].dispensary.website_url ?? ""
        
        if dispensireis.isEmpty == true
        {
           
        }
        else
        {
            if let url = URL(string: dispensireis) {
                UIApplication.shared.open(url)
                       
            }
        }
      
    }
    
    //# Dispensary List
       
    func Dispensary_list(User_id:String,dispensary_id:String,User_lat:String,User_long:String)
       {
           if Connectivity.isConnectedToInternet()
           {
               SVProgressHUD.show(withStatus: "Please wait")
               SVProgressHUD.setDefaultMaskType(.black)
               
               let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().new_ws_get_dispensary_detail)")
               
            let dict  = [APIKeys().user_id:User_id,APIKeys().dispensary_id:dispensary_id,APIKeys().user_lat:User_lat,APIKeys().user_lng:User_long]
               
               
               APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                   SVProgressHUD.dismiss()
                   DispProfile_List = []
                   if status == true
                   {
                       let response_Data = response as? NSDictionary ?? [:]
                       print(response_Data)
                       
                       let response_status = response_Data.value(forKey: "status") as? Int ?? 0
                       if response_status == 200
                       {
                           let reason = response as? [String:Any] ?? [:]
                           
                           DispProfile_List.append(DisProfileClass.init(fromDictionary: reason))
                         self.dispensery_detaills_table.reloadData()
                       }
                       else
                       {
                           let msg = response_Data.value(forKey: "msg") as? String ?? ""
                           self.alertmessage(error: "", message: msg)
                       }
                   }
                   else
                   {
                       self.alertmessage(error: "", message: ResponseMessage().something_error)
                   }
               }
           }
           else
           {
               self.alertmessage(error: "", message: ResponseMessage().no_internet)
           }
       }
    
    func Dispensary_Rating(Rate_Id:String,dispensary_Rating:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().new_ws_rate_dispensary)")
            
         let dict  = [APIKeys().rate:Rate_Id,APIKeys().dispensary_id:dispensary_Rating]
            
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                DispProfile_List = []
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "status") as? Int ?? 0
                    
                    if response_status == 200
                    {
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                    else
                    {
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
