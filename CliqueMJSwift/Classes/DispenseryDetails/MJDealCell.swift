//
//  MJDealCell.swift
//  CliqueMJSwift
//
//  Created by Nikhil Dixit on 16/07/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class MJDealCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
       @IBOutlet weak var buds_timer_lbl: UILabel!
       @IBOutlet weak var loyal_green_img: UIImageView!
       @IBOutlet weak var deal_icon: UIImageView!
       @IBOutlet weak var deal_name: UILabel!
       @IBOutlet weak var Deal_img: UIImageView!
       @IBOutlet weak var custom_view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.custom_view.layer.cornerRadius = 5.0
               self.custom_view.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
