//
//  RatingCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class RatingCell: UITableViewCell {

    @IBOutlet var star5Btn: UIButton!
    @IBOutlet var star4btn: UIButton!
    @IBOutlet var star3btn: UIButton!
    @IBOutlet var star2btn: UIButton!
    @IBOutlet var star1btn: UIButton!
    @IBOutlet var star_5Img: UIImageView!
    @IBOutlet var star4Img: UIImageView!
    @IBOutlet var star3_img: UIImageView!
    @IBOutlet var star_2Img: UIImageView!
    @IBOutlet var star_1Img: UIImageView!
    @IBOutlet var total_rating: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
