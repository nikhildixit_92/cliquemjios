//
//  dis_de_secondCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class dis_de_secondCell: UITableViewCell {

    @IBOutlet weak var call_view: UIButton!
    @IBOutlet weak var order_btn: UIButton!
    @IBOutlet weak var exchange_btn: UIButton!
    @IBOutlet weak var website_btn: UIButton!
    
    @IBOutlet weak var Call_View: ANCustomView!
    @IBOutlet weak var orderView: ANCustomView!
    @IBOutlet weak var exchangeView: ANCustomView!
    @IBOutlet weak var website_view: ANCustomView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
