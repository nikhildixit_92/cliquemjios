//
//  dis_de_thirdCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import GoogleMaps

class dis_de_thirdCell: UITableViewCell,GMSMapViewDelegate {

    @IBOutlet weak var get_direction_btn: KButton!
    @IBOutlet weak var address_txt: UILabel!
    @IBOutlet weak var map_view: GMSMapView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.map_view.delegate = self
        
        self.map_view.padding = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 10)
        self.map_view.delegate = self
        
        self.Update_location()
        
        self.get_direction_btn.addTarget(self, action: #selector(openGoogleMap), for: .touchUpInside)
        
        if DispProfile_List.count != 0
        {
            let dict = DispProfile_List[0].dispensary
            self.address_txt.text = dict?.address ?? ""
        }
        else
        {
            self.address_txt.text = ""
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func Update_location()
    {
        cameraMoveToLocation(toLocation: LocationSingleton.sharedInstance.locationManager?.location?.coordinate)
        
        self.map_view.isMyLocationEnabled = true
        self.map_view.settings.myLocationButton = true
        self.map_view.settings.allowScrollGesturesDuringRotateOrZoom = false
        
        let marker: GMSMarker = GMSMarker() // Allocating Marker

         marker.title = "" // Setting title
         marker.snippet = "" // Setting sub title
         marker.icon = UIImage(named: "markericon") // Marker icon
         marker.appearAnimation = .pop // Appearing animation. default
        marker.position = (LocationSingleton.sharedInstance.locationManager?.location!.coordinate)! // CLLocationCoordinate2D

        DispatchQueue.main.async { // Setting marker on mapview in main thread.
            marker.map = self.map_view // Setting marker on Mapview
        }
        
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?)
    {
        if toLocation != nil
        {
            self.map_view.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 14.0)
        }
    }
    
    @objc func openGoogleMap() {
        
        if DispProfile_List.count != 0
        {
            let dict = DispProfile_List[0].dispensary
            
            let double = dict?.lat
            let latDouble = Double(double ?? 0.0)
            let double_long = dict?.lng
            let longDouble = Double(double_long ?? 0.0)
               
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app

                if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(latDouble),\(longDouble)&directionsmode=driving") {
                          UIApplication.shared.open(url, options: [:])
                 }}
            else {
                   //Open in browser
                if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(latDouble ),\(longDouble)&directionsmode=driving") {
                       UIApplication.shared.open(urlDestination)
                                 
               }
                      }

            
        }
        else
        {
            
        }
        
        
       }
    
}
