//
//  DispenseryFollowView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol DispensaryNameShow
{
    func Dipensary_name(ind_val:Int,send_id:Int)
}

class DispenseryFollowView: UIViewController
{
    var new_redeem_ID = Int()
    @IBOutlet weak var unfollow_btn: UIButton!
    var dis_delgate:DispensaryNameShow?
    @IBOutlet weak var follow_btn: UIButton!
    @IBOutlet weak var switch_btn: UISwitch!
    var come_from = String()
    @IBOutlet weak var dispensery_txt: UILabel!
    @IBOutlet weak var custom_vieew: UIView!
    var despensaries_id = String()
    var user_id = String()
    var follow = Int()
    var dispensary_name = String()
    //var home_follow:HomeDispensary?
    var index_value = Int()
    var search_Disp_value = Int()
    var new_come_view = String()
    var screen_type = String()
    var deal_id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.custom_vieew.clipsToBounds = true
        self.custom_vieew.layer.cornerRadius = 40
        self.custom_vieew.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_txt.text = self.dispensary_name
        
        if follow == 2
        {
            self.switch_btn.isOn = false
        }
        else if follow == 1
        {
            self.switch_btn.isOn = true
        }
        else
        {
            self.switch_btn.isOn = false
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func following_btn(_ sender: Any)
    {
        if follow == 2
        {
            self.alertmessage(error: "", message: ResponseMessage().followed)
        }
        else
        {
            self.follow_dispensary_API(user_id: self.user_id, dispensary_id: self.despensaries_id, type: "2")
        }
    }
    
    @IBAction func switch_btn_action(_ sender: Any)
    {
        if follow == 1
        {
            self.switch_btn.isOn = true
            self.alertmessage(error: "", message: ResponseMessage().subscribe)
        }
        else
        {
            self.switch_btn.isOn = true
            self.follow_dispensary_API(user_id: self.user_id, dispensary_id: self.despensaries_id, type: "1")
        }
    }
    @IBAction func follow_btn_Action(_ sender: Any)
    {
        if follow == 0
        {
            self.alertmessage(error: "", message: ResponseMessage().unfollowed)
        }
        else
        {
            self.follow_dispensary_API(user_id: self.user_id, dispensary_id: self.despensaries_id, type: "0")
        }
        
        
    }
    
    func follow_dispensary_API(user_id:String,dispensary_id:String,type:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().follow_dispensary)")
            
            let dict  = [APIKeys().user_id:user_id,APIKeys().dispensary_id:dispensary_id,APIKeys().type:type,APIKeys().screen_type:self.screen_type,APIKeys().deal_id:self.deal_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "status") as? Int ?? 0
                    
                    if response_status == 200
                    {
                        
                        if self.come_from == "searchList"
                        {
                            if self.new_come_view == "Browse"
                            {
                                if type == "0"
                                {
                                    if self.search_Disp_value == 0
                                    {
                                        Search_Dips_List[0].followed.remove(at:self.index_value)
                                    }
                                    else
                                    {
                                        Search_Dips_List[0].nearby.remove(at:self.index_value)
                                    }
                                }
                                else
                                {
                                    if self.search_Disp_value == 0
                                    {
                                        Search_Dips_List[0].followed[self.index_value].follow = Int(type)
                                    }
                                    else
                                    {
                                        Search_Dips_List[0].nearby[self.index_value].follow = Int(type)
                                    }
                                }
                            }
                            else
                            {
                                if type == "0"
                                {
                                    if self.search_Disp_value == 0
                                    {
                                        Search_Dips_List[0].nearby.remove(at:self.index_value)
                                    }
                                    else
                                    {
                                        Search_Dips_List[0].followed.remove(at:self.index_value)
                                    }
                                    
                                }
                                else
                                {
                                    if self.search_Disp_value == 0
                                    {
                                        Search_Dips_List[0].nearby[self.index_value].follow = Int(type)
                                    }
                                    else
                                    {
                                        Search_Dips_List[0].followed[self.index_value].follow = Int(type)
                                    }
                                }
                            }
                            
                            
                        }
                        else if self.come_from == "collectbudslist"
                        {
                            if type == "0"
                            {
                                if self.search_Disp_value == 0
                                {
                                    FollowSearchModel[0].followed.remove(at:self.index_value)
                                }
                                else
                                {
                                    FollowSearchModel[0].nearby.remove(at:self.index_value)
                                }
                            }
                            else
                            {
                                if self.search_Disp_value == 0
                                {
                                    FollowSearchModel[0].followed[self.index_value].follow = Int(type)
                                }
                                else
                                {
                                    FollowSearchModel[0].nearby[self.index_value].follow = Int(type)
                                }
                                
                            }
                        }
                        else if self.come_from == "cliquefollow"
                        {
                            if type == "0"
                            {
                                
                            }
                            else
                            {
                                self.index_value  = 1000
                                self.new_redeem_ID = response_Data.value(forKey: "deal_id") as? Int ?? 0
                                print(self.new_redeem_ID)
                            }
                        }
                        else if self.come_from == "homeprofiledeal"
                        {
                            self.index_value  = 1000
                            self.new_redeem_ID = response_Data.value(forKey: "deal_id") as? Int ?? 0
                                                           print(self.new_redeem_ID)
                        }
                        else if self.come_from == "homedetails"
                        {
                            
                        }
                        else
                        {
                            if type == "0"
                            {
                                CompleteRoot_data[0].dispensaries.remove(at:self.index_value)
                            }
                            else
                            {
                                CompleteRoot_data[0].dispensaries[self.index_value].follow = Int(type)
                            }
                        }
                        
            
                        
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.sucessmessage(error: "", message: msg)
                    }
                    else
                    {
                        
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sucessmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: sucess_dimiss)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sucess_dimiss(sender:UIAlertAction)
    {
        if come_from == "cliquefollow"
        {
            dis_delgate?.Dipensary_name(ind_val:0, send_id:self.new_redeem_ID)
            self.dismiss(animated: true, completion: nil)
        }
        else if come_from == "homeprofiledeal"
        {
            dis_delgate?.Dipensary_name(ind_val:1, send_id:self.new_redeem_ID)
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            dis_delgate?.Dipensary_name(ind_val:self.index_value, send_id:0)
            self.dismiss(animated: true, completion: nil)
        }
                
        // }
        
    }
}
