//
//  EditProfileView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditProfileView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var verify_mobile_btn: UIButton!
    @IBOutlet weak var verify_email_btn: UIButton!
    @IBOutlet weak var Phone_number_txt: CustomUITextField!
    @IBOutlet weak var save_btn: KButton!
    @IBOutlet weak var change_password_btn: UIButton!
    @IBOutlet weak var email_txt: CustomUITextField!
    @IBOutlet weak var user_name_txt: CustomUITextField!
    @IBOutlet weak var name_txt: CustomUITextField!
    @IBOutlet weak var border_customView: UIView!
    @IBOutlet weak var user_pick_btn: UIButton!
    @IBOutlet weak var user_img: UIImageView!
    @IBOutlet weak var user_view: UIView!
    var errormessage = ""
    let imagePicker = UIImagePickerController()
    
    var Profile_image = Data()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.border_customView.clipsToBounds = true
        self.border_customView.layer.cornerRadius = 40
        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        //        self.user_view.layer.cornerRadius = self.user_view.frame.size.width/2
        //        self.user_view.layer.masksToBounds = true
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
        
        
        self.user_pick_btn.addTarget(self, action: #selector(UserPick_btn_action), for: .touchUpInside)
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        self.user_name_txt.placeholder = "Enter nick name"
        self.email_txt.placeholder = "Enter email"
        self.Phone_number_txt.placeholder = "Enter phone number"
        self.name_txt.placeholder = "Enter name"
        self.user_name_txt.text  = user_data.value(forKey: "nickname") as? String ?? ""
        self.name_txt.text = "\(userInformation.value(forKey: "first_name") as? String ?? "") \(userInformation.value(forKey:"last_name") as? String ?? "")"
        self.email_txt.text = user_data.value(forKey: "email") as? String ?? ""
        self.Phone_number_txt.text = user_data.value(forKey:"mobile_number") as? String ?? ""
        print(self.name_txt.text!)
        
        let dispensries_img_S = userInformation.value(forKey: "profile_picture") as? String ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.user_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                  placeholderImage: UIImage(named: "avatar_icon"),
                                  options: .refreshCached,
                                  completed: nil)
        
        if dispensries_img_S.isEmpty == true
        {
            
        }
        else
        {
            do
            {
                DispatchQueue.global(qos: .background).async
                    {
                    self.Profile_image = try! Data.init(contentsOf: URL.init(string: disp_img!)!, options: .alwaysMapped)
                }
                
            }
            catch
            {
                print("Could not convert image into data")
            }
            
        }
        
        
        let email_verified = user_data.value(forKey:"email_verified") as? Int ?? 0
        
        if email_verified == 1
        {
            self.verify_email_btn.isHidden = false
             self.verify_email_btn.setTitle("Verified", for: .normal)
        }
        else
        {
            self.verify_email_btn.isHidden = false
            self.verify_email_btn.setTitle("Verify", for: .normal)
        }
        
        let  mobile_verified  = user_data.value(forKey:"mobile_verified") as? Int ?? 0
        
        if mobile_verified == 1
        {
            self.verify_mobile_btn.isHidden = false
            self.verify_mobile_btn.setTitle("Verified", for: .normal)
        }
        else
        {
            self.verify_mobile_btn.isHidden = false
            self.verify_mobile_btn.setTitle("Verify", for: .normal)
        }
        
        self.verify_email_btn.addTarget(self, action: #selector(verify_email_btn_action), for: .touchUpInside)
        
        self.verify_mobile_btn.addTarget(self, action: #selector(verify_mobile_btn_action), for: .touchUpInside)
        
        
        self.ProfileGetAPI(user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)")
        
        // Do any additional setup after loading the view.
    }
    
    @objc func verify_email_btn_action(sender:UIButton)
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        self.VerifyEmailAPI(email: user_data.value(forKey: "email") as? String ?? "", user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)")
    }
    
    @objc func verify_mobile_btn_action(sender:UIButton)
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        let otp = self.storyboard?.instantiateViewController(withIdentifier: "OtpView") as! OtpView
        otp.new_user_id  = "\(userInformation.value(forKey:"user_id") as? Int ?? 0)"
        otp.user_id = user_data.value(forKey:"mobile_number") as? String ?? ""
        otp.modalPresentationStyle = .fullScreen
        otp.come_from = "profile"
        self.present(otp, animated: true, completion: nil)
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Change_password_action(_ sender: Any)
    {
        let change_pass  = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordView") as! ChangePasswordView
        change_pass.modalPresentationStyle = .overFullScreen
        self.present(change_pass, animated: true, completion: nil)
    }
    @IBAction func edit_intrest_btn(_ sender: Any)
    {
        let change_pass  = self.storyboard?.instantiateViewController(withIdentifier: "profileEditIntrestView") as! profileEditIntrestView
        change_pass.modalPresentationStyle = .overFullScreen
        self.present(change_pass, animated: true, completion: nil)
        
    }
    
    @IBAction func save_btn_action(_ sender: Any)
    {
        if checkValidation() == false
        {
            self.alertmessage(error: "", message: self.errormessage)
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
    //print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            let Display_name = self.name_txt.text!
            print(Display_name)
            let complete_name = Display_name.components(separatedBy: " ")
            
            var first_name = String()
            var last_name = String()
            
            if complete_name.count > 1
            {
                first_name = complete_name[0]
                
                let new_string = Display_name.replacingOccurrences(of: first_name, with: "")
                
                last_name = new_string
                
                print(first_name)
                print(last_name)
            }
            else
            {
                first_name = complete_name[0]
            }
            
            self.ProfileSubmitAPI(user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)", nick_name: self.user_name_txt.text!, first_name: first_name, last_name: last_name, email_id: self.email_txt.text!, phone_number:self.Phone_number_txt.text!)
        }
    }
    
    @IBAction func cancel_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Qrcode_scanner_action(_ sender: Any)
    {
        let qr_codeView = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeProfileView") as! QRCodeProfileView
        self.present(qr_codeView, animated: true, completion: nil)
    }
    
    func checkValidation() -> Bool
    {
        if self.Profile_image.count == 0
        {
            self.errormessage = ResponseMessage().profile_error
            return false
        }
        if self.name_txt.text?.trimmingCharacters(in: .whitespaces).isEmpty == true
        {
            self.errormessage = ResponseMessage().display_error
            return false
        }
        if NameValid().validName(value: self.name_txt.text!) == false
        {
            self.errormessage = ResponseMessage().display_error_valid
            return false
        }
        if self.user_name_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().user_name_empty
            return false
        }
        if passwordSpace().validate(string: self.user_name_txt.text!) == false
        {
            self.errormessage = ResponseMessage().empty_username_space
            return false
        }
        
        if email_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().email_empty
            return false
        }
        if EmailValidation().isValidEmail(testStr: self.email_txt.text!) == false
        {
            self.errormessage = ResponseMessage().email_valid
            return false
        }
        if self.Phone_number_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().phone_number_empty
            return false
        }
        if self.Phone_number_txt.text!.count < 6 || self.Phone_number_txt!.text!.count > 15
        {
            self.errormessage = ResponseMessage().phone_number_limit
            return false
        }
        
        self.errormessage = ""
        return true
    }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func UserPick_btn_action(sender:UIButton)
    {
        let alert = UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom
        {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Open Camera & Check Camera is Available or not
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: Open gallery
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: Image Picker View Delegate & update profile Image API
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil
        {
            let pickernew = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)
            
            
            // self.profile_img.image = pickernew!
            let new_image = self.imageOrientation(pickernew!)
            
            let image = new_image.jpegData(compressionQuality: 0.8)
            
            
            self.Profile_image = image!
            
            self.user_img.image = new_image
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imageOrientation(_ srcImage: UIImage)->UIImage {
        if srcImage.imageOrientation == UIImage.Orientation.up {
            return srcImage
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch srcImage.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: srcImage.size.width, y: srcImage.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))// replace M_PI by Double.pi when using swift 4.0
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: srcImage.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2))// replace M_PI_2 by Double.pi/2 when using swift 4.0
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: srcImage.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi/2))// replace M_PI_2 by Double.pi/2 when using swift 4.0
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        switch srcImage.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: srcImage.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: srcImage.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        let ctx:CGContext = CGContext(data: nil, width: Int(srcImage.size.width), height: Int(srcImage.size.height), bitsPerComponent: (srcImage.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (srcImage.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        ctx.concatenate(transform)
        switch srcImage.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(srcImage.cgImage!, in: CGRect(x: 0, y: 0, width: srcImage.size.height, height: srcImage.size.width))
            break
        default:
            ctx.draw(srcImage.cgImage!, in: CGRect(x: 0, y: 0, width: srcImage.size.width, height: srcImage.size.height))
            break
        }
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        return img
    }
    
    func VerifyEmailAPI(email:String,user_id:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().ws_verify_email)")
            
            let dict = [APIKeys().email_id:email,APIKeys().user_id:user_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        let message = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func ProfileGetAPI(user_id:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().ws_get_user_profile)")
            
            let dict = [APIKeys().user_id:user_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                       UserDefaults.standard.set(resonse_status, forKey: "user_info")
                        
                        let user_data = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        print(user_data)
                        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
                        
                        self.user_name_txt.placeholder = "Enter nick name"
                        self.email_txt.placeholder = "Enter email"
                        self.Phone_number_txt.placeholder = "Enter phone number"
                        self.name_txt.placeholder = "Enter name"
                        self.user_name_txt.text  = user_data.value(forKey: "nickname") as? String ?? ""
                        self.name_txt.text = "\(userInformation.value(forKey: "first_name") as? String ?? "") \(userInformation.value(forKey:"last_name") as? String ?? "")"
                        self.email_txt.text = user_data.value(forKey: "email") as? String ?? ""
                        self.Phone_number_txt.text = user_data.value(forKey:"mobile_number") as? String ?? ""
                        print(self.name_txt.text!)
                        
                        let dispensries_img_S = userInformation.value(forKey: "profile_picture") as? String ?? ""
                        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        self.user_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                  placeholderImage: UIImage(named: "avatar_icon"),
                                                  options: .refreshCached,
                                                  completed: nil)
                        
                        let email_verified = user_data.value(forKey:"email_verified") as? Int ?? 0
                        
                       if email_verified == 1
                        {
                            self.verify_email_btn.isHidden = false
                             self.verify_email_btn.setTitle("Verified", for: .normal)
                        }
                        else
                        {
                            self.verify_email_btn.isHidden = false
                            self.verify_email_btn.setTitle("Verify", for: .normal)
                        }
                        
                        let  mobile_verified  = user_data.value(forKey:"mobile_verified") as? Int ?? 0
                        
                        if mobile_verified == 1
                        {
                            self.verify_mobile_btn.isHidden = false
                            self.verify_mobile_btn.setTitle("Verified", for: .normal)
                        }
                        else
                        {
                            self.verify_mobile_btn.isHidden = false
                            self.verify_mobile_btn.setTitle("Verify", for: .normal)
                        }
                        
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    
    func ProfileSubmitAPI(user_id:String,nick_name:String,first_name:String,last_name:String,email_id:String,phone_number:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().ws_change_profile)")
            
            let dict = [APIKeys().user_id:user_id,APIKeys().nickname:nick_name,APIKeys().first_name:first_name,APIKeys().last_name:last_name,APIKeys().email:email_id,APIKeys().mobile_number:phone_number]
            
            print(dict)
            
            APIMethods().MultiRequest(url: base_url!, imageData: self.Profile_image, parameters: dict, headers: [:]) { (response, status) in
         
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                       UserDefaults.standard.set(resonse_status, forKey: "user_info")
                        
                        let user_data = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        print(user_data)
                        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
                        
                        self.user_name_txt.placeholder = "Enter nick name"
                        self.email_txt.placeholder = "Enter email"
                        self.Phone_number_txt.placeholder = "Enter phone number"
                        self.name_txt.placeholder = "Enter name"
                        self.user_name_txt.text  = user_data.value(forKey: "nickname") as? String ?? ""
                        self.name_txt.text = "\(userInformation.value(forKey: "first_name") as? String ?? "") \(userInformation.value(forKey:"last_name") as? String ?? "")"
                        self.email_txt.text = user_data.value(forKey: "email") as? String ?? ""
                        self.Phone_number_txt.text = user_data.value(forKey:"mobile_number") as? String ?? ""
                        print(self.name_txt.text!)
                        
                        let dispensries_img_S = userInformation.value(forKey: "profile_picture") as? String ?? ""
                        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        self.user_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                  placeholderImage: UIImage(named: "avatar_icon"),
                                                  options: .refreshCached,
                                                  completed: nil)
                         let email_verified = user_data.value(forKey:"email_verified") as? Int ?? 0
                        if email_verified == 1
                        {
                            self.verify_email_btn.isHidden = false
                             self.verify_email_btn.setTitle("Verified", for: .normal)
                        }
                        else
                        {
                            self.verify_email_btn.isHidden = false
                            self.verify_email_btn.setTitle("Verify", for: .normal)
                        }
                        
                        let  mobile_verified  = user_data.value(forKey:"mobile_verified") as? Int ?? 0
                        
                        if mobile_verified == 1
                        {
                            self.verify_mobile_btn.isHidden = false
                            self.verify_mobile_btn.setTitle("Verified", for: .normal)
                        }
                        else
                        {
                            self.verify_mobile_btn.isHidden = false
                            self.verify_mobile_btn.setTitle("Verify", for: .normal)
                        }
                        
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                        
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
}
