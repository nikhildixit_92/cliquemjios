////
////  profileEditIntrestView.swift
////  CliqueMJSwift
////
////  Created by nikhil on 30/04/20.
////  Copyright © 2020 nikhil. All rights reserved.
////


//
//  profileEditIntrestView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class profileEditIntrestView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var intrest_table_heignt: NSLayoutConstraint!
    @IBOutlet weak var intrest_table: UITableView!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var border_customView: UIView!
    @IBOutlet weak var user_pick_btn: UIButton!
    @IBOutlet weak var user_img: UIImageView!
    @IBOutlet weak var user_view: UIView!
    var selected_value = NSMutableArray()
    var new_all_select = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.border_customView.clipsToBounds = true
        self.border_customView.layer.cornerRadius = 40
        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
        
        let nib = UINib.init(nibName: "productIntCell", bundle: nil)
        self.intrest_table.register(nib, forCellReuseIdentifier: "productIntCell")
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        let dispensries_img_S = userInformation.value(forKey: "profile_picture") as? String ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.user_img.sd_setImage(with: URL(string:disp_img ?? ""),
               placeholderImage: UIImage(named: "avatar_icon"),
               options: .refreshCached,
               completed: nil)
        
    
        self.GetInterestAPI(user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)")
        
        self.save_btn.addTarget(self, action: #selector(save_action), for: .touchUpInside)
        
      //  let email_verified =
        
        // Do any additional setup after loading the view.
    }
    
    @objc func save_action()
    {
        if checckValidation() == false
        {
            self.alertmessage(error: "", message: "Please select atleast one interest")
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            
            let data = try! JSONSerialization.data(withJSONObject: self.selected_value, options: .prettyPrinted)
            let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        

            self.SetIntresetAPI(user_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", intreset: string as! String)
        }
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true
            , completion: nil)
    }
    
    @IBAction func all_select_action(_ sender: Any)
    {
        
        
        if selected_value.count == InterestList[0].interests.count
        {
            
        }
        else
        {
            for i in 0..<InterestList[0].interests.count
            {
                let select_id = InterestList[0].interests[i].selected ?? 0
                
                if self.selected_value.contains(select_id)
                {
                    
                }
                else
                {
                    InterestList[0].interests[i].selected = 1
                    
                    self.selected_value.add(InterestList[0].interests[i].id)
                }
                
            }
        }
        
        
         print(self.selected_value)
        self.intrest_table.reloadData()
    }
    @IBAction func cancel_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if InterestList.count != 0
        {
            return InterestList[0].interests.count
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productIntCell", for: indexPath) as! productIntCell
         
        let dict = InterestList[0].interests[indexPath.row]
        
        cell.intrest_name.text = dict.name ?? ""
        
        let selected_value = dict.selected ?? 0
        
        if selected_value == 0
        {
            cell.c.image = UIImage.init(named:"check_box")
            cell.backgroud_img.backgroundColor = UIColor.black
            cell.backgroud_img.alpha = 0.2
            cell.custom_view.layer.cornerRadius = 5.0
            cell.custom_view.layer.masksToBounds = true
        }
        else
        {
            cell.c.image = UIImage.init(named:"uncheck_box")
            cell.backgroud_img.backgroundColor = UIColor.fromHexaString(hex:"262B50")
            cell.backgroud_img.alpha = 1.0
            cell.custom_view.layer.cornerRadius = 5.0
            cell.custom_view.layer.masksToBounds = true
            
            //self.selected_value.add(dict.id ?? 0)
            
            //38 43 80

        }
        
        let dispensries_img_S = dict.image ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.intrest_img.sd_setImage(with: URL(string:disp_img ?? ""),
                      placeholderImage: UIImage(named: ""),
                      options: .refreshCached,
                      completed: nil)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = InterestList[0].interests[indexPath.row]
        
        let selected_row = dict.selected ?? 0
        
        if selected_row == 0
        {
            self.selected_value.add(InterestList[0].interests[indexPath.row].id)
            InterestList[0].interests[indexPath.row].selected = 1
        }
        else
        {
            self.selected_value.remove(InterestList[0].interests[indexPath.row].id)
            InterestList[0].interests[indexPath.row].selected = 0
        }
        
        print(self.selected_value)
        self.intrest_table.reloadData()
    }
    
    
    
    func GetInterestAPI(user_id:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_get_interest)")
            
            let dict = [APIKeys().user_id:user_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        let data = response as? [String:Any] ?? [:]
                    
                         InterestList.append(InterestClass.init(fromDictionary: data))
                        self.intrest_table.reloadData()
                        
                        for i in 0..<InterestList[0].interests.count
                        {
                            let select_id = InterestList[0].interests[i].selected ?? 0
                            
                            if select_id == 1
                            {
                                 self.selected_value.add(InterestList[0].interests[i].id)
                            }
                            else
                            {
                            
                            }
                        }
                        
                        self.intrest_table_heignt.constant = CGFloat(InterestList[0].interests.count*65)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func SetIntresetAPI(user_id:String,intreset:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_set_interest)")
            
            let dict = [APIKeys().user_id:user_id,APIKeys().interests:self.selected_value] as [String : Any]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func checckValidation() -> Bool
    {
        if self.selected_value.count == 0
        {
            return false
        }
        
        return true
        
    }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}



//
//import UIKit
//import SVProgressHUD
//
//class profileEditIntrestView: UIViewController,UITableViewDelegate,UITableViewDataSource {
//
//    @IBOutlet weak var intrest_table_heignt: NSLayoutConstraint!
//    @IBOutlet weak var intrest_table: UITableView!
//    @IBOutlet weak var save_btn: UIButton!
//    @IBOutlet weak var border_customView: UIView!
//    @IBOutlet weak var user_pick_btn: UIButton!
//    @IBOutlet weak var user_img: UIImageView!
//    @IBOutlet weak var user_view: UIView!
//    var selected_value = NSMutableArray()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.border_customView.clipsToBounds = true
//        self.border_customView.layer.cornerRadius = 40
//        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
//
//        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
//        self.user_img.layer.masksToBounds = true
//
//        let nib = UINib.init(nibName: "productIntCell", bundle: nil)
//        self.intrest_table.register(nib, forCellReuseIdentifier: "productIntCell")
//
//        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
//        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
//
//        let dispensries_img_S = userInformation.value(forKey: "profile_picture") as? String ?? ""
//        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        self.user_img.sd_setImage(with: URL(string:disp_img ?? ""),
//               placeholderImage: UIImage(named: "avatar_icon"),
//               options: .refreshCached,
//               completed: nil)
//
//
//        self.GetInterestAPI(user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)")
//
//        self.save_btn.addTarget(self, action: #selector(save_action), for: .touchUpInside)
//
//      //  let email_verified =
//
//        // Do any additional setup after loading the view.
//    }
//
//    @objc func save_action()
//    {
//        if checckValidation() == false
//        {
//            self.alertmessage(error: "", message: "Please select atleast one interest")
//        }
//        else
//        {
//            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
//            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
//
//
//            let data = try! JSONSerialization.data(withJSONObject: self.selected_value, options: .prettyPrinted)
//            let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//
//
//            self.SetIntresetAPI(user_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", intreset: string as! String)
//        }
//    }
//
//    @IBAction func dismiss_action(_ sender: Any)
//    {
//        self.dismiss(animated: true
//            , completion: nil)
//    }
//
//    @IBAction func all_select_action(_ sender: Any)
//    {
//        for i in 0..<InterestList[0].interests.count
//        {
//            let select_id = InterestList[0].interests[i].selected ?? 0
//
//            if select_id == 0
//            {
//                InterestList[0].interests[i].selected = 1
//
//                self.selected_value.add(InterestList[0].interests[i].id)
//
//            }
//            else
//            {
//                self.selected_value.remove(InterestList[0].interests[i].id)
//                InterestList[0].interests[i].selected = 0
//            }
//        }
//         print(self.selected_value)
//        self.intrest_table.reloadData()
//    }
//    @IBAction func cancel_btn_action(_ sender: Any)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        if InterestList.count != 0
//        {
//            return InterestList[0].interests.count
//        }
//        else
//        {
//            return 0
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "productIntCell", for: indexPath) as! productIntCell
//
//        let dict = InterestList[0].interests[indexPath.row]
//
//        cell.intrest_name.text = dict.name ?? ""
//
//        let selected_value = dict.selected ?? 0
//
//        if selected_value == 0
//        {
//            cell.c.image = UIImage.init(named:"check_box")
//            cell.backgroud_img.backgroundColor = UIColor.black
//            cell.backgroud_img.alpha = 0.2
//            cell.custom_view.layer.cornerRadius = 5.0
//            cell.custom_view.layer.masksToBounds = true
//        }
//        else
//        {
//            cell.c.image = UIImage.init(named:"uncheck_box")
//            cell.backgroud_img.backgroundColor = UIColor.fromHexaString(hex:"262B50")
//            cell.backgroud_img.alpha = 1.0
//            cell.custom_view.layer.cornerRadius = 5.0
//            cell.custom_view.layer.masksToBounds = true
//            //38 43 80
//
//        }
//
//        let dispensries_img_S = dict.image ?? ""
//        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        cell.intrest_img.sd_setImage(with: URL(string:disp_img ?? ""),
//                      placeholderImage: UIImage(named: ""),
//                      options: .refreshCached,
//                      completed: nil)
//
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 65
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        let dict = InterestList[0].interests[indexPath.row]
//
//        let selected_row = dict.selected ?? 0
//
//        if selected_row == 0
//        {
//            self.selected_value.add(InterestList[0].interests[indexPath.row].id)
//            InterestList[0].interests[indexPath.row].selected = 1
//        }
//        else
//        {
//            self.selected_value.remove(InterestList[0].interests[indexPath.row].id)
//            InterestList[0].interests[indexPath.row].selected = 0
//        }
//
//        print(self.selected_value)
//        self.intrest_table.reloadData()
//    }
//
//
//
//    func GetInterestAPI(user_id:String)
//    {
//        if Connectivity.isConnectedToInternet()
//        {
//            SVProgressHUD.show(withStatus: "Please wait")
//            SVProgressHUD.setDefaultMaskType(.black)
//
//            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_get_interest)")
//
//            let dict = [APIKeys().user_id:user_id]
//
//            print(dict)
//
//            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
//
//                SVProgressHUD.dismiss()
//
//                if status == true
//                {
//                    let resonse_status = response as? NSDictionary ?? [:]
//                    print(resonse_status)
//
//                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
//
//                    if status_login == 200
//                    {
//                        let data = response as? [String:Any] ?? [:]
//
//                         InterestList.append(InterestClass.init(fromDictionary: data))
//                        self.intrest_table.reloadData()
//
//                        self.intrest_table_heignt.constant = CGFloat(InterestList[0].interests.count*65)
//                    }
//                    else
//                    {
//                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
//                        self.alertmessage(error: "", message: message)
//                    }
//                }
//                else
//                {
//                    self.alertmessage(error: "", message: ResponseMessage().something_error)
//                }
//
//            }
//        }
//        else
//        {
//            self.alertmessage(error: "", message: ResponseMessage().no_internet)
//        }
//    }
//
//    func SetIntresetAPI(user_id:String,intreset:String)
//    {
//        if Connectivity.isConnectedToInternet()
//        {
//            SVProgressHUD.show(withStatus: "Please wait")
//            SVProgressHUD.setDefaultMaskType(.black)
//
//            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_set_interest)")
//
//            let dict = [APIKeys().user_id:user_id,APIKeys().interests:self.selected_value] as [String : Any]
//
//            print(dict)
//
//            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
//
//                SVProgressHUD.dismiss()
//
//                if status == true
//                {
//                    let resonse_status = response as? NSDictionary ?? [:]
//                    print(resonse_status)
//
//                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
//
//                    if status_login == 200
//                    {
//                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
//                        self.alertmessage(error: "", message: message)
//                    }
//                    else
//                    {
//                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
//                        self.alertmessage(error: "", message: message)
//                    }
//                }
//                else
//                {
//                    self.alertmessage(error: "", message: ResponseMessage().something_error)
//                }
//
//            }
//        }
//        else
//        {
//            self.alertmessage(error: "", message: ResponseMessage().no_internet)
//        }
//    }
//
//    func checckValidation() -> Bool
//    {
//        if self.selected_value.count == 0
//        {
//            return false
//        }
//
//        return true
//
//    }
//
//    //# Alert Messages
//
//    func alertmessage(error:String,message:String)
//    {
//        let alertController = UIAlertController(title: error,
//                                                message: message,
//                                                preferredStyle: .alert)
//
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(defaultAction)
//
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func json(from object:Any) -> String? {
//        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
//            return nil
//        }
//        return String(data: data, encoding: String.Encoding.utf8)
//    }
//}
