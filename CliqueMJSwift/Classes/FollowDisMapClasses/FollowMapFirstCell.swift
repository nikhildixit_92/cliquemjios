//
//  FollowMapFirstCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 11/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class FollowMapFirstCell: UITableViewCell {

    @IBOutlet weak var opticity_img: UIImageView!
    @IBOutlet weak var custom_view: ANCustomView!
    @IBOutlet weak var main_customView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
