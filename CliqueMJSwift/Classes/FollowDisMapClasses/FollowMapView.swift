//
//  FollowMapView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 07/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import MapKit

class FollowMapView: UIViewController,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
  
    

    @IBOutlet weak var map_table_x: NSLayoutConstraint!
    @IBOutlet weak var mapSearch_table: UITableView!
    @IBOutlet weak var map_view: MKMapView!
    @IBOutlet weak var address_txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.map_view.delegate = self
        
        let nib = UINib.init(nibName: "FollowMapFirstCell", bundle: nil)
        self.mapSearch_table.register(nib, forCellReuseIdentifier: "FollowMapFirstCell")

        let nib1 = UINib.init(nibName: "FollowMapSecondCell", bundle: nil)
        self.mapSearch_table.register(nib1, forCellReuseIdentifier: "FollowMapSecondCell")
        
         self.mapSearch_table.rowHeight = 178
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeUp.direction = .up
        swipeUp.delegate = self // set delegate
        self.mapSearch_table.addGestureRecognizer(swipeUp)
        
//        let swipedown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
//        swipedown.direction = .down
//        swipedown.delegate = self // set delegate
//        self.mapSearch_table.addGestureRecognizer(swipedown)
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            return 5
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FollowMapFirstCell", for: indexPath) as! FollowMapFirstCell
            
            cell.main_customView.clipsToBounds = true
            cell.main_customView.layer.cornerRadius = 30
            cell.main_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            cell.main_customView.isUserInteractionEnabled = true
            cell.opticity_img.layer.cornerRadius = 31
            cell.opticity_img.layer.masksToBounds = true
            
            let swipedown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            swipedown.direction = .down
            swipedown.delegate = self // set delegate
            cell.main_customView.addGestureRecognizer(swipedown)
            
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FollowMapSecondCell", for: indexPath) as! FollowMapSecondCell
            
            cell.backgroundColor = UIColor.fromHexaString(hex: colorcodes().Port_Gore)

            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 123
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    // Debugging - All Swipes Are Detected Now
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                
                if self.map_table_x.constant == 0.0
                {
                    self.map_table_x.constant = self.view.frame.size.height - 233.0
                }
                else
                {
                    
                }
                
            case  UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
                
                if self.map_table_x.constant > 0.0
                {
                    self.map_table_x.constant = 0.0
                }
                else
                {
                    
                }
                
            default:
                break
            }
        }
    }
    
    @IBAction func skip(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func address_btn_action(_ sender: Any)
    {
        
    }
}
