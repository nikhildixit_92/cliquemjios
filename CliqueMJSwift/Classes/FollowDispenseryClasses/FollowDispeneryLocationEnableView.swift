//
//  FollowDispeneryLocationEnableView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 07/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class FollowDispeneryLocationEnableView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var search_txt: CustomUITextField!
    @IBOutlet weak var address_txt: UILabel!
    @IBOutlet weak var dispensaries_list: UITableView!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

          let nib = UINib.init(nibName: "Follow_dis_Cell", bundle: nil)
          self.dispensaries_list.register(nib, forCellReuseIdentifier: "Follow_dis_Cell")
        
        self.dispensaries_list.rowHeight = 121
        
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Follow_dis_Cell", for: indexPath) as! Follow_dis_Cell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dispensary_btn_action(_ sender: Any)
    {
        
    }
    @IBAction func address_btn_Action(_ sender: Any)
    {
        
    }
    
}
