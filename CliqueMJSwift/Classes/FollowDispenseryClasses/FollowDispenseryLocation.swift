//
//  FollowDispenseryLocation.swift
//  CliqueMJSwift
//
//  Created by nikhil on 07/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class FollowDispenseryLocation: UIViewController {

    
    @IBOutlet weak var location_img: UIImageView!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var location_txt: CustomUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.location_img.layer.cornerRadius = 5.0
        self.location_img.layer.masksToBounds = true
        
      
    

        // Do any additional setup after loading the view.
    }
    
    @IBAction func submit_btn_action(_ sender: Any)
    {
        let map_viewFollow = self.storyboard?.instantiateViewController(withIdentifier: "FollowMapView") as! FollowMapView
        self.present(map_viewFollow, animated: true, completion: nil)
    }
    @IBAction func skip_btn_action(_ sender: Any)
    {
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        let navigation = UINavigationController.init(rootViewController: drawerC)
        navigation.setNavigationBarHidden(true, animated: true)
        appDelegate.window?.rootViewController = navigation
        appDelegate.window?.makeKeyAndVisible()
    }
}
