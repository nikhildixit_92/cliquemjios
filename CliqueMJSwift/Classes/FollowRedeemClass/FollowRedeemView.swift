//
//  FollowRedeemView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

protocol CallHomeAPI_Follow
{
    func Hme_API()
}

class FollowRedeemView: UIViewController,DispensaryNameShow {
        
    var Home_APIDelegate:CallHomeAPI_Follow?
    var come_from = String()
    @IBOutlet weak var ok_btn: UIButton!
    @IBOutlet weak var dispensery_address: UILabel!
    @IBOutlet weak var dispensery_name_lbl: UILabel!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var qr_img: UIImageView!
    @IBOutlet weak var user_name_txt: UILabel!
    var clique_deal:MoreDeal?
    var dispensary_id = String()
    var ind_value = Int()
    var profile_cliqueDeal:CliqueDeal?
    
   // var come_from = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.qr_img.image = #imageLiteral(resourceName: "Sample_voc")
        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        if come_from == "moredeal"
        {
            let dispensries_img_S = clique_deal?.dispenaryLogo ?? ""
            let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            self.qr_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                       placeholderImage: UIImage(named: ""),
                                                       options: .refreshCached,
                                                       completed: nil)
                       
                    
            self.user_name_txt.text = "Follow \(clique_deal?.dispensaryName ?? "") to claim deal"
            
            self.dispensery_name_lbl.text = clique_deal?.dispensaryName ?? ""
            self.dispensery_address.text = clique_deal?.address ?? ""
            
            self.ok_btn.setTitle("Follow", for: .normal)
            
        }
        else if come_from == "homeprofiledeal"
        {
            let dispensries_img_S = profile_cliqueDeal?.dispenaryLogo ?? ""
            let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            self.qr_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                       placeholderImage: UIImage(named: ""),
                                                       options: .refreshCached,
                                                       completed: nil)
                       
                    
            self.user_name_txt.text = "Follow \(profile_cliqueDeal?.dispensaryName ?? "") to claim deal"
            
            self.dispensery_name_lbl.text = profile_cliqueDeal?.dispensaryName ?? ""
            self.dispensery_address.text = ""
            
            self.ok_btn.setTitle("Follow", for: .normal)
        }
        else
        {
            
        }
        
        // Do any additional setup after loading the view.
    }

    @IBAction func ok_btn_Action(_ sender: Any)
    {
        if come_from == "moredeal"
        {
            let dispensireis = clique_deal
            let dispensaries_id = "\(dispensireis?.dispensaryId ?? 0)"
            let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
            let follow_id = 0
            let dispensaries_name = dispensireis?.dispensaryName ?? ""
            let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
            follow_dis.user_id = id
            follow_dis.follow = follow_id
            follow_dis.despensaries_id = dispensaries_id
            follow_dis.index_value = 1000
            follow_dis.dispensary_name = dispensaries_name
            follow_dis.modalPresentationStyle = .overFullScreen
            follow_dis.come_from = "cliquefollow"
            follow_dis.dis_delgate = self
            follow_dis.deal_id = "\(dispensireis?.id ?? 0)"
            follow_dis.screen_type = "deal_redeem"
            self.present(follow_dis, animated: true, completion: nil)

        }
        else if come_from == "homeprofiledeal"
        {
            let dispensireis = profile_cliqueDeal
            let dispensaries_id = "\(dispensireis?.dispensaryId ?? 0)"
            let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
            let follow_id = 0
            let dispensaries_name = dispensireis?.dispensaryName ?? ""
            let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
            follow_dis.user_id = id
            follow_dis.follow = follow_id
            follow_dis.despensaries_id = dispensaries_id
            follow_dis.index_value = 1000
            follow_dis.dispensary_name = dispensaries_name
            follow_dis.modalPresentationStyle = .overFullScreen
            follow_dis.come_from = "homeprofiledeal"
            follow_dis.dis_delgate = self
            follow_dis.deal_id = "\(dispensireis?.id ?? 0)"
            follow_dis.screen_type = "deal_redeem"
            self.present(follow_dis, animated: true, completion: nil)
        }
    }
    @IBAction func not_now_action(_ sender: Any)
    {
        Home_APIDelegate?.Hme_API()
        self.dismiss(animated: true, completion: nil)
    }
    
    func Dipensary_name(ind_val: Int, send_id: Int)
    {
        if ind_val == 0
        {
            self.dismiss(animated: true, completion: nil)
            
            print(send_id)
            //self.dispensary_id = "\(send_id)"
            
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "CliqueDealView") as! CliqueDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensary_id = self.dispensary_id
            loyality.ind_value = 0
            loyality.come_from = "moredeal"
            loyality.clique_deal = clique_deal
            loyality.new_offer_id = send_id
            self.present(loyality, animated: true, completion: nil)
        }
        else if ind_val == 1
        {
            self.dismiss(animated: true, completion: nil)
            
            print(send_id)
            //self.dispensary_id = "\(send_id)"
            
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "CliqueDealView") as! CliqueDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensary_id = self.dispensary_id
            loyality.ind_value = 0
            loyality.come_from = "homeprofiledeal"
            loyality.profile_cliqueDeal = profile_cliqueDeal
            loyality.new_offer_id = send_id
            self.present(loyality, animated: true, completion: nil)
        }
    }
    
    
}
