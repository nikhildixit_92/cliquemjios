//
//  ForgotView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 02/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD


class ForgotView: UIViewController {

    @IBOutlet weak var submit_btn: KButton!
    @IBOutlet weak var forgot_img: UIImageView!
    @IBOutlet weak var Email_address_txt: CustomUITextField!
    var error_message = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.forgot_img.layer.cornerRadius = 5.0
        self.forgot_img.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func submit_btn_Action(_ sender: Any)
    {
        if Check_validation() == false
        {
            self.alertmessage(error: "", message: self.error_message)
        }
        else
        {
            self.forgot_Pass_API(email: self.Email_address_txt.text!)
        }
        
        
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    //# Check Validation
    
    func Check_validation() -> Bool
    {
        if self.Email_address_txt.text?.isNumeric == true
        {
            if self.Email_address_txt.text?.isEmpty == true
            {
                self.error_message = ResponseMessage().forgot_email_phone
                return false
            }
        }
        else
        {
            if self.Email_address_txt.text?.isEmpty == true
            {
                self.error_message = ResponseMessage().forgot_email_phone
                return false
            }
            if EmailValidation().isValidEmail(testStr: self.Email_address_txt.text ?? "") == false
            {
                self.error_message = ResponseMessage().email_valid
                return false
            }
        }
        
        self.error_message = ""
        return true
        
    }
    
    
    func forgot_Pass_API(email:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().forgot_pass)")
            
            let dict = [APIKeys().email_id:email]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        let message = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.Successmessage(error: "", message: message)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func Successmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: sucess_action)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sucess_action(sender:UIAlertAction)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
