//
//  CliqueDealsCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 12/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit


protocol Deal_click_check
{
    func DealClicked(deal_id:String,dispensary_id:String,deal_data:MoreDeal);
}


class CliqueDealsCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var Deal_clickedDelagate:Deal_click_check?
    @IBOutlet weak var clique_deals_view: UICollectionView!
    var Deals_m:MoreDeal?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nib = UINib.init(nibName: "cliqueCollectionCell", bundle: nil)
        self.clique_deals_view.register(nib, forCellWithReuseIdentifier: "cliqueCollectionCell")
        
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return CompleteRoot_data[0].moreDeals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cliqueCollectionCell", for: indexPath) as! cliqueCollectionCell
        
        let dict = CompleteRoot_data[0].moreDeals[indexPath.row]
        
        let dispensries_img_S = dict.logo ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.clique_img.sd_setImage(with: URL(string:disp_img ?? ""),
        placeholderImage: UIImage(named: ""),
        options: .refreshCached,
        completed: nil)
        
        cell.off_txt.text = "%\(dict.percent ?? 0) Off"
        
        cell.clique_view.layer.cornerRadius = 5.0
        cell.clique_view.layer.masksToBounds = true
        
        cell.Deal_btn.tag = indexPath.row
        cell.Deal_btn.addTarget(self, action: #selector(Clique_Deal_button), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: 155, height: 213)
    }
    
    @objc func Clique_Deal_button(sender:UIButton)
    {
        let dict = CompleteRoot_data[0].moreDeals[sender.tag]
        
        Deal_clickedDelagate?.DealClicked(deal_id: "\(dict.id ?? 0)",dispensary_id:"\(dict.dispensaryId ?? 0)", deal_data: dict)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
