//
//  HomeDispensaryCell.swift
//  CliqueMJSwift
//
//  Created by Nikhil Dixit on 23/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class HomeDispensaryCell: UITableViewCell {

    @IBOutlet weak var fav_icon_btn: UIButton!
    @IBOutlet weak var dispansery_img: UIImageView!
       @IBOutlet weak var dispensary_name_txt: UILabel!
       @IBOutlet weak var adress_txt: UILabel!
       @IBOutlet weak var star_txt: UILabel!
       @IBOutlet weak var star_icon: UIImageView!
       @IBOutlet weak var opclose_txt: UILabel!
       @IBOutlet weak var time_Txt: UILabel!
       @IBOutlet weak var loyality_txt: UILabel!
       @IBOutlet weak var collect_buds_btn: KButton!
       
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
