//
//  HomeDispensaryDisCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 11/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

protocol showpopup
{
    func showPop_up(dic:AnyObject,dis_id:String,index_value:Int);
}

class HomeDispensaryDisCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var fav_icon_btn: UIButton!
    var show_delegate:showpopup?
    @IBOutlet weak var dispansery_img: UIImageView!
    @IBOutlet weak var dispensary_name_txt: UILabel!
    @IBOutlet weak var adress_txt: UILabel!
    @IBOutlet weak var star_txt: UILabel!
    @IBOutlet weak var star_icon: UIImageView!
    @IBOutlet weak var opclose_txt: UILabel!
    @IBOutlet weak var time_Txt: UILabel!
    @IBOutlet weak var loyality_txt: UILabel!
    @IBOutlet weak var collect_buds_btn: KButton!
    @IBOutlet weak var page_control_h: UIPageControl!
    @IBOutlet weak var hor_collectionView: UICollectionView!
    var slide = Int()
    var Dis_deals:Dispensary?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let nib = UINib.init(nibName: "HomeDisCollectionCell", bundle: nil)
        self.hor_collectionView.register(nib, forCellWithReuseIdentifier: "HomeDisCollectionCell")
        
       // self.page_control_h.numberOfPages = self.Dis_deals!.deals.count
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       // print(Dis_deals?.deals.count)
        return (Dis_deals?.deals.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeDisCollectionCell", for: indexPath) as! HomeDisCollectionCell

        
        let dict = Dis_deals?.deals[indexPath.row]
        
        let compaign_type = dict?["compaign_type"] as? Int ?? 0
        
        if compaign_type == 0
        {
            cell.Deal_icon.image  = #imageLiteral(resourceName: "MjDealIcon")
            cell.Loyality_deal_lbl.text = "MJ Deal"
        }
        else if compaign_type == 1
        {
            cell.Deal_icon.image  = #imageLiteral(resourceName: "redeemsearchicon")
            cell.Loyality_deal_lbl.text = "Loyalty Deal™"
        }
        else if compaign_type == 2
        {
            cell.Deal_icon.image  = #imageLiteral(resourceName: "clique_icon")
            cell.Loyality_deal_lbl.text = "Clique Deal"
        }
        
        cell.deal_txt.text = "\(dict?["bud"] as? Int ?? 0)"
        cell.precent_txt.text = "%\(dict?["percent"] as? Int ?? 0) Off"
        let dispensries_img_S = dict?["logo"] as? String ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.img_img.sd_setImage(with: URL(string:disp_img ?? ""),
        placeholderImage: UIImage(named: ""),
        options: .refreshCached,
        completed: nil)
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.hor_collectionView.frame.size.width, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = Dis_deals?.deals[indexPath.row]
        show_delegate?.showPop_up(dic: dict!, dis_id: "\(Dis_deals?.id ?? 0)",index_value:indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.page_control_h.currentPage = (Int)(scrollView.contentOffset.x/self.hor_collectionView.frame.size.width)
      //  slide = (Int)(scrollView.contentOffset.x/self.hor_collectionView.frame.size.width)
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
