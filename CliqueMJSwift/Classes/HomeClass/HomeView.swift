//
//  HomeView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import CoreLocation

class HomeView: UIViewController,UITableViewDataSource,UITableViewDelegate,showpopup,Deal_click_check,Update_location,DispensaryNameShow,show_homeAPI,homeUpdate_API,CallHomeAPI_Follow
{
    
    
    
    
    var name_Area = String()
    var show_more = false
    var i = 1
    @IBOutlet weak var collect_buds_btn: UIButton!
    var dispensary_arr = [String]()
    @IBOutlet weak var home_table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib.init(nibName: "HamburgerCell", bundle: nil)
        self.home_table.register(nib, forCellReuseIdentifier: "HamburgerCell")
        
        let nib1 = UINib.init(nibName: "HomeFirstCell", bundle: nil)
        self.home_table.register(nib1, forCellReuseIdentifier: "HomeFirstCell")
        
        let nib2 = UINib.init(nibName: "HomeSecondCell", bundle: nil)
        self.home_table.register(nib2, forCellReuseIdentifier: "HomeSecondCell")
        
        let nib3 = UINib.init(nibName: "HomeDispensaryTitleCell", bundle: nil)
        self.home_table.register(nib3, forCellReuseIdentifier: "HomeDispensaryTitleCell")
        
        let nib4 = UINib.init(nibName: "HomeDispensaryDisCell", bundle: nil)
        self.home_table.register(nib4, forCellReuseIdentifier: "HomeDispensaryDisCell")
        
        let nib5 = UINib.init(nibName: "HomeDispmoreCell", bundle: nil)
        self.home_table.register(nib5, forCellReuseIdentifier: "HomeDispmoreCell")
        
        let nib6 = UINib.init(nibName: "HomeBannerCell", bundle: nil)
        self.home_table.register(nib6, forCellReuseIdentifier: "HomeBannerCell")
        
        let nib7 = UINib.init(nibName: "CliqueDealsCell", bundle: nil)
        self.home_table.register(nib7, forCellReuseIdentifier: "CliqueDealsCell")
        
        
        let nib8 = UINib.init(nibName: "nearby_textCell", bundle: nil)
        self.home_table.register(nib8, forCellReuseIdentifier: "nearby_textCell")
        
        let nib9 = UINib.init(nibName: "HomeDispensaryCell", bundle: nil)
        self.home_table.register(nib9, forCellReuseIdentifier: "HomeDispensaryCell")
        
        self.home_table.rowHeight = 122
        
        
        let Checking_open = UserDefaults.standard.value(forKey: "Open_app") as? Int ?? 0
        
        if Checking_open == 0
        {
            UserDefaults.standard.set(i, forKey: "Open_app")
            
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
            }
            
            //            if CheckLocation().check_location() == false
            //            {
            //                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", select_lat: "26.94092001302814", select_long: "75.80355090096202")
            //            }
            //            else
            //            {
            //                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", select_lat: "26.94092001302814", select_long: "75.80355090096202")
            //            }
        }
        else
        {
            UserDefaults.standard.set(Checking_open + 1, forKey: "Open_app")
            
            let new_checkingOpen = UserDefaults.standard.value(forKey: "Open_app") as? Int ?? 0
            
            if new_checkingOpen % 2 == 0
            {
                let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                print(user_data)
                let email_verified = user_data.value(forKey: "email_verified") as? Int ?? 0
                let mobile_verified = user_data.value(forKey: "mobile_verified") as? Int ?? 0
                
                if email_verified == 0 && mobile_verified == 0
                {
                    let show_verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyPopUpView") as! VerifyPopUpView
                    show_verify.modalPresentationStyle = .fullScreen
                    show_verify.come_from = 2
                    self.present(show_verify, animated: true, completion: nil)
                }
                else if email_verified == 0
                {
                    let show_verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyPopUpView") as! VerifyPopUpView
                    show_verify.modalPresentationStyle = .fullScreen
                    show_verify.come_from = 0
                    self.present(show_verify, animated: true, completion: nil)
                }
                else if mobile_verified == 0
                {
                    let show_verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyPopUpView") as! VerifyPopUpView
                    show_verify.modalPresentationStyle = .fullScreen
                    show_verify.come_from = 1
                    self.present(show_verify, animated: true, completion: nil)
                }
                else
                {
                    let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                    print(user_data)
                    let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
                    
                    if CheckLocation().check_location() == false
                    {
                        
                    }
                    else
                    {
                        self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
                    }
                    
                    //                    if CheckLocation().check_location() == false
                    //                    {
                    //                        self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", select_lat: "26.94092001302814", select_long: "75.80355090096202")
                    //                    }
                    //                    else
                    //                    {
                    //                        self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", select_lat: "26.94092001302814", select_long: "75.80355090096202")
                    //                    }
                }
            }
            else
            {
                let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                print(user_data)
                let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
                
                if CheckLocation().check_location() == false
                {
                    
                }
                else
                {
                    self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
                }
                
                //                if CheckLocation().check_location() == false
                //                {
                //                    self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", select_lat: "26.94092001302814", select_long: "75.80355090096202")
                //                }
                //                else
                //                {
                //                    self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "26.94092001302814", long: "75.80355090096202", select_lat: "26.94092001302814", select_long: "75.80355090096202")
                //                }
            }
        }
        
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.geocode(latitude: LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0, longitude: LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0) { (response, error) in
                
                let state = response?.administrativeArea ?? ""
                let city = response?.country ?? ""
                
                self.name_Area = "\(state), \(city)"
                // self.home_table.reloadData()
            }
        }
        
        
        
        //self.dispensary_arr = ["",""]
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        let drawer_id = UserDefaults.standard.value(forKey: "drawer_id") as? String ?? ""
        
        if drawer_id == "home"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
            }
            
            if CheckLocation().check_location() == false
            {
                
            }
            else
            {
                self.geocode(latitude: LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0, longitude: LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0) { (response, error) in
                    
                    let state = response?.administrativeArea ?? ""
                    let city = response?.country ?? ""
                    
                    self.name_Area = "\(state), \(city)"
                    // self.home_table.reloadData()
                }
            }
            
        }
        else if drawer_id == "editprofile"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
            dispensery.modalPresentationStyle = .overFullScreen
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "Rewards"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
            dispensery.modalPresentationStyle = .fullScreen
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "referfriend"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "ReferAFriendView") as! ReferAFriendView
            dispensery.modalPresentationStyle = .fullScreen
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "myqrcode"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeProfileView") as! QRCodeProfileView
            dispensery.modalPresentationStyle = .overFullScreen
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "Changelocation"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "LocationView") as! LocationView
            dispensery.modalPresentationStyle = .fullScreen
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "collectbuds"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
            collect.come_from = ""
            collect.update_API = self
            collect.modalPresentationStyle = .overFullScreen
            self.present(collect, animated: true, completion: nil)
        }
        else if drawer_id == "logout"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            UserDefaults.standard.removeObject(forKey: "user_info")
            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            login.modalPresentationStyle = .fullScreen
            self.present(login, animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if CompleteRoot_data.count != 0
        {
            return 8
        }
        else
        {
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 3
        {
            if self.show_more == false
            {
                return 1
            }
            else
            {
                return CompleteRoot_data[0].dispensaries.count
            }
            
        }
        else if section == 6
        {
            return 1
        }
        else
        {
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if CompleteRoot_data.count == 0
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HamburgerCell", for: indexPath) as! HamburgerCell
                cell.hamburger_btn.addTarget(self, action: #selector(hamburger_Action), for: .touchUpInside)
                cell.Bell_btn.addTarget(self, action: #selector(Bell_action), for: .touchUpInside)
                cell.search_btn.addTarget(self, action: #selector(Search_action), for: .touchUpInside)
                return cell
            }
            else if indexPath.section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFirstCell", for: indexPath) as! HomeFirstCell
                
                let user_info = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                
                cell.namr_lbl.text = "Hi \(user_info.value(forKey: "nickname") as? String ?? "")"
                
                cell.address_lbl.text = self.name_Area
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSecondCell", for: indexPath) as! HomeSecondCell
                
                cell.Browse_btn.addTarget(self, action: #selector(show_browse_cat), for: .touchUpInside)
                
                return cell
                
            }
        }
        else
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HamburgerCell", for: indexPath) as! HamburgerCell
                cell.hamburger_btn.addTarget(self, action: #selector(hamburger_Action), for: .touchUpInside)
                cell.Bell_btn.addTarget(self, action: #selector(Bell_action), for: .touchUpInside)
                cell.search_btn.addTarget(self, action: #selector(Search_action), for: .touchUpInside)
                return cell
            }
            else if indexPath.section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFirstCell", for: indexPath) as! HomeFirstCell
                
                let user_info = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                
                cell.namr_lbl.text = "Hi \(user_info.value(forKey: "nickname") as? String ?? "")"
                cell.address_lbl.text = self.name_Area
                return cell
            }
            else if indexPath.section == 2
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryTitleCell", for: indexPath) as! HomeDispensaryTitleCell
                
                
                return cell
            }
            else if indexPath.section == 3
            {
                if self.show_more == false
                {
                    if CompleteRoot_data[0].dispensaries.count != 0
                    {
                        
                        let dict = CompleteRoot_data[0].dispensaries[0].deals
                        let dispensireis = CompleteRoot_data[0].dispensaries[0]
                        if dict?.count != 0
                        {
                            
                            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryDisCell", for: indexPath) as! HomeDispensaryDisCell
                            cell.dispensary_name_txt.text = dispensireis.dispensaryName
                            cell.adress_txt.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                            cell.star_txt.text = "\(dispensireis.rating ?? 0).0"
                            
                            let dispensries_img_S = dispensireis.profilePicture
                            let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            cell.dispansery_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                            placeholderImage: UIImage(named: ""),
                                                            options: .refreshCached,
                                                            completed: nil)
                            
                            let is_open = dispensireis.is_open ?? 0
                            
                            if is_open == 0
                            {
                                cell.opclose_txt.text = "Close"
                                cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            }
                            else
                            {
                                cell.opclose_txt.text = "Open"
                                cell.time_Txt.text = "Close \(dispensireis.realClosingTime ?? "")"
                            }
                            
                            // cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            cell.Dis_deals = dispensireis
                            //  print(dispensireis.deals.count)
                            cell.page_control_h.numberOfPages = dispensireis.deals.count
                            cell.hor_collectionView.reloadData()
                            cell.show_delegate = self
                            cell.loyality_txt.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                            cell.collect_buds_btn.tag  = indexPath.row
                            cell.collect_buds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                            cell.fav_icon_btn.tag = 0
                            cell.fav_icon_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                            
                            let follow = dispensireis.follow ?? 0
                            
                            if follow == 0
                            {
                                cell.fav_icon_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                            }
                            else if follow == 1
                            {
                                cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            else if follow == 2
                            {
                                cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            return cell
                        }
                        else
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryCell", for: indexPath) as! HomeDispensaryCell
                            
                            cell.dispensary_name_txt.text = dispensireis.dispensaryName
                            cell.adress_txt.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                            cell.star_txt.text = "\(dispensireis.rating ?? 0).0"
                            
                            let dispensries_img_S = dispensireis.profilePicture
                            let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            cell.dispansery_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                            placeholderImage: UIImage(named: ""),
                                                            options: .refreshCached,
                                                            completed: nil)
                            
                            let is_open = dispensireis.is_open ?? 0
                            
                            if is_open == 0
                            {
                                cell.opclose_txt.text = "Close"
                                cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            }
                            else
                            {
                                cell.opclose_txt.text = "Open"
                                cell.time_Txt.text = "Close \(dispensireis.realClosingTime ?? "")"
                            }
                            
                            // cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                            cell.loyality_txt.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                            cell.fav_icon_btn.tag = indexPath.row
                            cell.fav_icon_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                            cell.collect_buds_btn.tag  = indexPath.row
                            cell.collect_buds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                            
                            let follow = dispensireis.follow ?? 0
                            
                            if follow == 0
                            {
                                cell.fav_icon_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                            }
                            else if follow == 1
                            {
                                cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            else if follow == 2
                            {
                                cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                            }
                            
                            return cell
                        }
                    }
                    else
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSecondCell", for: indexPath) as! HomeSecondCell
                        
                        cell.Browse_btn.addTarget(self, action: #selector(show_browse_cat), for: .touchUpInside)
                        return cell
                    }
                }
                else
                {
                    let dict = CompleteRoot_data[0].dispensaries[indexPath.row].deals
                    let dispensireis = CompleteRoot_data[0].dispensaries[indexPath.row]
                    
                    if dict?.count != 0
                    {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryDisCell", for: indexPath) as! HomeDispensaryDisCell
                        cell.dispensary_name_txt.text = dispensireis.dispensaryName
                        cell.adress_txt.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                        cell.star_txt.text = "\(dispensireis.rating ?? 0).0"
                        
                        let dispensries_img_S = dispensireis.profilePicture
                        let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        cell.dispansery_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                        placeholderImage: UIImage(named: ""),
                                                        options: .refreshCached,
                                                        completed: nil)
                        
                        let is_open = dispensireis.is_open ?? 0
                        
                        if is_open == 0
                        {
                            cell.opclose_txt.text = "Close"
                            cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                        }
                        else
                        {
                            cell.opclose_txt.text = "Open"
                            cell.time_Txt.text = "Close \(dispensireis.realClosingTime ?? "")"
                        }
                        
                        //   cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                        cell.Dis_deals = dispensireis
                        //  print(dispensireis.deals.count)
                        cell.page_control_h.numberOfPages = dispensireis.deals.count
                        cell.hor_collectionView.reloadData()
                        cell.show_delegate = self
                        cell.fav_icon_btn.tag = indexPath.row
                        cell.fav_icon_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                        cell.loyality_txt.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                        cell.collect_buds_btn.tag  = indexPath.row
                        cell.collect_buds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                        
                        let follow = dispensireis.follow ?? 0
                        
                        if follow == 0
                        {
                            cell.fav_icon_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                        }
                        else if follow == 1
                        {
                            cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                        }
                        else if follow == 2
                        {
                            cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                        }
                        
                        
                        return cell
                    }
                    else
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryCell", for: indexPath) as! HomeDispensaryCell
                        
                        cell.dispensary_name_txt.text = dispensireis.dispensaryName
                        cell.adress_txt.text = "\(dispensireis.distance ?? "") •\(dispensireis.address ?? "")"
                        cell.star_txt.text = "\(dispensireis.rating ?? 0).0"
                        
                        let dispensries_img_S = dispensireis.profilePicture
                        let disp_img = dispensries_img_S?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        cell.dispansery_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                        placeholderImage: UIImage(named: ""),
                                                        options: .refreshCached,
                                                        completed: nil)
                        
                        let is_open = dispensireis.is_open ?? 0
                        
                        if is_open == 0
                        {
                            cell.opclose_txt.text = "Close"
                            cell.time_Txt.text = "Opens \(dispensireis.realOpeningTime ?? "")"
                        }
                        else
                        {
                            cell.opclose_txt.text = "Open"
                            cell.time_Txt.text = "Close \(dispensireis.realClosingTime ?? "")"
                        }
                        
                        cell.fav_icon_btn.tag = indexPath.row
                        cell.fav_icon_btn.addTarget(self, action: #selector(show_Like_action), for: .touchUpInside)
                        
                        cell.loyality_txt.text = "\(dispensireis.bud ?? 0) Loyality Buds™"
                        cell.collect_buds_btn.tag  = indexPath.row
                        cell.collect_buds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                        
                        let follow = dispensireis.follow ?? 0
                        
                        if follow == 0
                        {
                            cell.fav_icon_btn.setImage(UIImage.init(named: "New_unlike"), for: .normal)
                        }
                        else if follow == 1
                        {
                            cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                        }
                        else if follow == 2
                        {
                            cell.fav_icon_btn.setImage(UIImage.init(named: "like_icon"), for: .normal)
                        }
                        
                        return cell
                    }
                }
                
            }
            else if indexPath.section == 4
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispmoreCell", for: indexPath) as! HomeDispmoreCell
                
                cell.opticty_img.layer.cornerRadius = 19
                cell.opticty_img.layer.masksToBounds = true
                
                cell.see_more_btn.addTarget(self, action: #selector(show_more_action), for: .touchUpInside)
                return cell
                
            }
            else if indexPath.section == 5
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBannerCell", for: indexPath) as! HomeBannerCell
                
                return cell
                
            }
            else if indexPath.section == 6
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CliqueDealsCell", for: indexPath) as! CliqueDealsCell
                
                if CompleteRoot_data[0].moreDeals.count != 0
                {
                    cell.isHidden = false
                    cell.clique_deals_view.reloadData()
                }
                else
                {
                    cell.isHidden = true
                }
                
                
                cell.Deal_clickedDelagate = self
                return cell
                
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "nearby_textCell", for: indexPath) as! nearby_textCell
                
                return cell
                
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if CompleteRoot_data.count == 0
        {
            if indexPath.section == 0
            {
                return 44
            }
            else if indexPath.section == 1
            {
                return 123
            }
            else
            {
                return 338
            }
        }
        else
        {
            if indexPath.section == 0
            {
                return 44
            }
            else if indexPath.section == 1
            {
                return 123
            }
            else if indexPath.section  == 2
            {
                return 58
            }
            else if indexPath.section == 3
            {
                return UITableView.automaticDimension
            }
            else if indexPath.section == 4
            {
                return 122
            }
            else if indexPath.section == 7
            {
                return 44
            }
            else if indexPath.section == 6
            {
                if CompleteRoot_data[0].moreDeals.count != 0
                {
                    return 367
                }
                else
                {
                    return 0
                }
                
            }
            else
            {
                return UITableView.automaticDimension
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 5
        {
            let refer = self.storyboard?.instantiateViewController(withIdentifier: "ReferAFriendView") as! ReferAFriendView
            refer.modalPresentationStyle = .fullScreen
            self.present(refer, animated: true, completion: nil)
        }
        else if indexPath.section == 3
        {
            let dict = CompleteRoot_data[0].dispensaries[indexPath.row]
            
            let dispensary_profile = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as! DispenseryDetailsView
            dispensary_profile.modalPresentationStyle = .fullScreen
            dispensary_profile.dispensaryID = "\(dict.id ?? 0)"
            self.present(dispensary_profile, animated: true, completion: nil)
        }
        else if indexPath.section == 1
        {
            let refer = self.storyboard?.instantiateViewController(withIdentifier: "LocationView") as! LocationView
            refer.modalPresentationStyle = .fullScreen
            refer.update_loc = self
            self.present(refer, animated: true, completion: nil)
        }
    }
    
    
    @objc func hamburger_Action()
    {
        if let drawerController = self.navigationController?.parent as? KYDrawerController
        {
            
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    @objc func show_Like_action(sender:UIButton)
    {
        let dispensireis = CompleteRoot_data[0].dispensaries[sender.tag]
        let dispensaries_id = "\(dispensireis.id ?? 0)"
        let user_information = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        let id = "\(user_information.value(forKey: "id") as? Int ?? 0)"
        let follow_id = dispensireis.follow ?? 0
        let dispensaries_name = dispensireis.dispensaryName ?? ""
        let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
        follow_dis.user_id = id
        follow_dis.follow = follow_id
        follow_dis.despensaries_id = dispensaries_id
        follow_dis.index_value = sender.tag
        follow_dis.dispensary_name = dispensaries_name
        follow_dis.modalPresentationStyle = .overFullScreen
        follow_dis.come_from = "home"
        follow_dis.dis_delgate = self
        self.present(follow_dis, animated: true, completion: nil)
        
        // let type = dispensireis.type
    }
    
    @objc func show_more_action(sender:UIButton)
    {
        self.show_more = true
        self.home_table.reloadData()
    }
    
    @objc func collect_buds_action()
    {
        let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectBudsView") as! CollectBudsView
        collect.modalPresentationStyle = .overFullScreen
        self.present(collect, animated: true, completion: nil)
    }
    
    @objc func Bell_action()
    {
        let bell = self.storyboard?.instantiateViewController(withIdentifier: "NotificationMainView") as! NotificationMainView
        bell.modalPresentationStyle = .fullScreen
        self.present(bell, animated: true, completion: nil)
    }
    
    @objc func Search_action()
    {
        let browse = self.storyboard?.instantiateViewController(withIdentifier: "BrowseView") as! BrowseView
        browse.come_from = "Browse"
        browse.homeDelegate = self
        browse.modalPresentationStyle = .fullScreen
        self.present(browse, animated: true, completion: nil)
    }
    
    @IBAction func collect_btn_action(_ sender: Any)
    {
        let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
        collect.come_from = ""
        collect.update_API = self
        collect.modalPresentationStyle = .overFullScreen
        self.present(collect, animated: true, completion: nil)
    }
    
    @objc func show_browse_cat()
    {
        let browse = self.storyboard?.instantiateViewController(withIdentifier: "BrowseView") as! BrowseView
        browse.come_from = ""
        browse.homeDelegate = self
        browse.modalPresentationStyle = .fullScreen
        self.present(browse, animated: true, completion: nil)
    }
    
    @objc func show_collect_buds(sender:UIButton)
    {
        let pos = CompleteRoot_data[0].dispensaries[sender.tag].pos
        
        if pos == 0
        {
            let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
            collect.modalPresentationStyle = .overFullScreen
            collect.come_from = "list_collectbuds"
            collect.update_API = self
            collect.list_select_dis = CompleteRoot_data[0].dispensaries[sender.tag]
            self.present(collect, animated: true, completion: nil)
        }
        else
        {
            
        }
        
    }
    
    func showPop_up(dic:AnyObject,dis_id:String,index_value:Int)
    {
        let compaign_type = dic["compaign_type"] as? Int ?? 0
        
        if compaign_type == 0
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "MJDealView") as! MJDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensay_id = dis_id
            loyality.inde_val = index_value
            self.present(loyality, animated: true, completion: nil)
            
            //MJDeal
        }
        else if compaign_type == 1
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityDealView") as! LoyalityDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensay_id  = dis_id
            loyality.inde_val = index_value
            loyality.come_from = "home"
            self.present(loyality, animated: true, completion: nil)
            
            // Loyalty Deal
        }
        else if compaign_type == 2
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "CliqueDealView") as! CliqueDealView
            loyality.modalPresentationStyle = .overFullScreen
            loyality.dispensary_id = dis_id
            loyality.ind_value = 0
            loyality.come_from = ""
            loyality.clique_deal_dis = dic
            self.present(loyality, animated: true, completion: nil)
        }
    }
    
    //# MARK: Deal Clicked Delagate
    
    func DealClicked(deal_id: String,dispensary_id:String,deal_data:MoreDeal)
    {
        let loyality = self.storyboard?.instantiateViewController(withIdentifier: "FollowRedeemView") as! FollowRedeemView
        loyality.modalPresentationStyle = .overFullScreen
        loyality.dispensary_id = dispensary_id
        loyality.ind_value = 0
        loyality.come_from = "moredeal"
        loyality.clique_deal = deal_data
        loyality.Home_APIDelegate = self
        self.present(loyality, animated: true, completion: nil)
    }
    
    func Update_location(select_user_lat: String, select_user_long: String,select_Address:String)
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        self.name_Area = select_Address
        self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: select_user_lat, select_long: select_user_long)
    }
    
    //# Dispensary List
    
    func Dispensary_list(User_id:String,lat:String,long:String,select_lat:String,select_long:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().new_ws_get_followed_dispensaries)")
            
            let dict  = [APIKeys().user_id:User_id,APIKeys().user_lat:lat,APIKeys().user_lng:long,APIKeys().search_lat:select_lat,APIKeys().search_lng:select_long]
            
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "staus") as? Int ?? 0
                    CompleteRoot_data = []
                    if response_status == 200
                    {
                        let reason = response as? [String:Any] ?? [:]
                        
                        CompleteRoot_data.append(RootClass.init(fromDictionary: reason))
                        self.home_table.reloadData()
                        // print(HomeDisList[0].dispensary_name)
                    }
                    else
                    {
                        self.home_table.reloadData()
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func HomeAPI()
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
        }
    }
    
    func Hme_API()
    {
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
        }
    }
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func Dipensary_name(ind_val:Int,send_id:Int)
    {
        if CompleteRoot_data.count != 0
        {
            if CompleteRoot_data[0].dispensaries.count
                == 0
            {
                let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                print(user_data)
                let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
                
                
                if CheckLocation().check_location() == false
                {
                    
                }
                else
                {
                    self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
                }
            }
            else
            {
                self.home_table.reloadData()
            }
        }
        //
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    // Below Mehtod will print error if not able to update location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error Location")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Access the last object from locations to get perfect current location
        if let location = locations.last {
            
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            
            geocode(latitude: myLocation.latitude, longitude: myLocation.longitude) { placemark, error in
                guard let placemark = placemark, error == nil else { return }
                // you should always update your UI in the main thread
                DispatchQueue.main.async {
                    //  update UI here
                    print("address1:", placemark.thoroughfare ?? "")
                    print("address2:", placemark.subThoroughfare ?? "")
                    print("city:",     placemark.locality ?? "")
                    print("state:",    placemark.administrativeArea ?? "")
                    print("zip code:", placemark.postalCode ?? "")
                    print("country:",  placemark.country ?? "")
                }
            }
        }
        manager.stopUpdatingLocation()
        
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        return String("\(minutes):\(seconds)")
        // return String(format:”%02i:%02i:%02i”, hours, minutes, seconds)
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        //let new_r = Int(arr_v) ?? 0 - 1000
        
        let ti = NSInteger(interval)
        
        let ms = Int((interval.truncatingRemainder(dividingBy: 1)) * 1000)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        print("hours:\(hours),minutes:\(minutes),seconds:\(seconds),ms:\(ms)")
        return NSString(format: "\(minutes):\(seconds)" as NSString)
    }
    
    func update_HomeAPI()
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        
        if CheckLocation().check_location() == false
        {
            
        }
        else
        {
            self.Dispensary_list(User_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", select_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", select_long: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)")
        }
    }
    
    
}



//func newTimer_index()
//{
//    self.new_index1_timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,   selector: (#selector(update_newtag_timer)), userInfo: nil, repeats: true)
//
//    // self.new_index1_timer.fire()
//
//    RunLoop.current.add(self.new_index1_timer, forMode: RunLoop.Mode.common)
//}

extension TimeInterval {
    var minuteSecondMS: String {
        return String(format:"%d:%02d.%03d", minute, second, millisecond)
    }
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
    
    
}
