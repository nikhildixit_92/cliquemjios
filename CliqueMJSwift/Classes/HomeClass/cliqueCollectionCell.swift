//
//  cliqueCollectionCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class cliqueCollectionCell: UICollectionViewCell {

    @IBOutlet weak var Deal_btn: KButton!
    @IBOutlet weak var clique_img: UIImageView!
    @IBOutlet weak var off_txt: UILabel!
    @IBOutlet weak var clique_mj_deals_txt: UILabel!
    @IBOutlet weak var clique_icon: UIImageView!
    @IBOutlet weak var clique_view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
