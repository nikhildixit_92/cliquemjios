//
//  IntroCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {

    @IBOutlet weak var sub_heading_text: UILabel!
    @IBOutlet weak var heading_text: UILabel!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
