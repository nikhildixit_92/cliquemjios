//
//  IntroView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class IntroView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var page_control: UIPageControl!
    var slide = 0
    @IBOutlet weak var next_btn: UIButton!
    var main_header_arr = ["Collect Loyalty Points","Receive Daily & Weekly Deals","Unlock Exclusive Promos"]
    
    var Sub_header_arr = ["Start collecting loyalty points at the dispensary of your choice now and unlock exclusive deals.","Locate the nearest dispensary, follow, and start receiving targeted dispensary deals.","Get to enjoy local food & entertainment coupons around that dispensary."]
    
    var image_arr = [#imageLiteral(resourceName: "benifits_screen"),#imageLiteral(resourceName: "benfitsscreen1"),#imageLiteral(resourceName: "benifits2")]
    
    @IBOutlet weak var collection_view: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "IntroCell", bundle: nil)
        self.collection_view.register(nib, forCellWithReuseIdentifier: "IntroCell")
        
        
        self.page_control.numberOfPages = self.main_header_arr.count
        
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.main_header_arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        
        cell.heading_text.text = self.main_header_arr[indexPath.row]
        cell.sub_heading_text.text = self.Sub_header_arr[indexPath.row]
        cell.img.image = self.image_arr[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: self.collection_view.frame.size.width, height: self.collection_view.frame.size.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.page_control.currentPage = (Int)(scrollView.contentOffset.x/self.collection_view.frame.size.width)
        slide = (Int)(scrollView.contentOffset.x/self.collection_view.frame.size.width)
        
        if slide == 0
        {
            self.next_btn.setTitle("Next", for: .normal)
            
        }
        else if slide == 1
        {
            self.next_btn.setTitle("Next", for: .normal)
        }
        else
        {
            self.next_btn.setTitle("Get Started", for: .normal)
        }
    }

    @IBAction func Next_btn_action(_ sender: Any)
    {
        if slide == self.image_arr.count-1
        {
           // self.collection_view.isScrollEnabled = false
            
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigation = UINavigationController.init(rootViewController: drawerC)
            navigation.setNavigationBarHidden(true, animated: true)
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
            
        }
        else
        {
            slide += 1
        }
        
        
        let indexPath = IndexPath(item: slide, section: 0)
        
        print("slide value is \(slide)")
        print("indexPath value is \(indexPath)")
        
        self.collection_view.scrollToItem(at: indexPath as IndexPath, at: .right, animated: true)
        
        if slide == 0
        {
            self.next_btn.setTitle("Next", for: .normal)
            
        }
        else if slide == 1
        {
            self.next_btn.setTitle("Next", for: .normal)
        }
        else
        {
            self.next_btn.setTitle("Get Started", for: .normal)
        }
    }
}
