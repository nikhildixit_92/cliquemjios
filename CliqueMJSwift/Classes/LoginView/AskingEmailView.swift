//
//  AskingEmailView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 16/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class AskingEmailView: UIViewController {

    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var Phone_number_txt: CustomUITextField!
    @IBOutlet weak var Phone_number_img: UIImageView!
    @IBOutlet weak var email_txt: CustomUITextField!
    @IBOutlet weak var email_img: UIImageView!
    
    var user_id = String()
    var first_name = String()
    var last_name = String()
    var email = String()
    var pic_url = String()
    var birthday = String()
    var gender = String()
    var Login_type = String()
    var come_social = String()
    var mobile_n = String()
    var error_message = String()
    var new_email = "0"
    var new_mobile = "0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.Phone_number_img.layer.cornerRadius = 5.0
        self.Phone_number_img.layer.masksToBounds = true
        
        self.email_img.layer.cornerRadius = 5.0
        self.email_img.layer.masksToBounds = true
        
        self.email_txt.keyboardType = .emailAddress
        self.Phone_number_txt.keyboardType = .phonePad
        
        
        if email.isEmpty == false
        {
            if come_social == "apple"
            {
                if EmailValidation().isValidEmail(testStr: self.email) == false
                {
                    self.email_txt.text = ""
                    self.Phone_number_txt.text = self.mobile_n
                    self.email_txt.isUserInteractionEnabled = true
                    self.new_mobile = "1"
                    self.new_email = "1"
                }
                else
                {
                    self.email_txt.text = self.email
                    self.Phone_number_txt.text = self.mobile_n
                    self.email_txt.isUserInteractionEnabled = false
                    self.new_mobile = "1"
                    self.new_email = "0"
                }
            }
            else
            {
                self.email_txt.text = self.email
                self.Phone_number_txt.text = self.mobile_n
                
                self.email_txt.isUserInteractionEnabled = false
                self.new_mobile = "1"
                self.new_email = "0"
            }
            
            
        }
        else if mobile_n.isEmpty == false
        {
           
            self.Phone_number_txt.text = self.mobile_n
            self.Phone_number_txt.isUserInteractionEnabled = false
            
            self.new_mobile = "0"
            self.new_email = "1"
        }
        else
        {
            self.new_mobile = "1"
            self.new_email = "1"
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submit_btn_action(_ sender: Any)
    {
        if self.Check_validation() == false
        {
            self.alertmessage(error: "", message: self.error_message)
        }
        else
        {
            if come_social == "fb"
            {
                self.ws_social_LoginAPI(login_type: Login_type, fb_id: user_id, google_id: "", apple_id: "", email_id: self.email_txt.text!, user_type: "", user_status: Constant().user_status, gender: gender, date_of_birth: birthday, first_name: first_name, last_name: last_name, profile_picture: pic_url, mobile_number:self.Phone_number_txt.text!, new_email: self.new_email,new_mobile:self.new_mobile)
            }
            else
            {
                self.ws_social_LoginAPI(login_type: Login_type, fb_id:"" , google_id: "", apple_id: user_id, email_id: self.email_txt.text!, user_type: "", user_status: Constant().user_status, gender: gender, date_of_birth: birthday, first_name: first_name, last_name: last_name, profile_picture: pic_url, mobile_number:self.Phone_number_txt.text!, new_email: self.new_email, new_mobile: self.new_mobile)
            }
        }
    }
    
    //# Social_login API
    
    func ws_social_LoginAPI(login_type:String,fb_id:String,google_id:String,apple_id:String,email_id:String,user_type:String,user_status:String,gender:String,date_of_birth:String,first_name:String,last_name:String,profile_picture:String,mobile_number:String,new_email:String,new_mobile:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().social_login)")
            
            let dict = [APIKeys().login_type:login_type,APIKeys().fb_id:fb_id,APIKeys().google_id:google_id,APIKeys().apple_id:apple_id,APIKeys().user_type:user_type,APIKeys().user_status:user_status,APIKeys().gender:gender,APIKeys().date_of_birth:date_of_birth,APIKeys().first_name:first_name,APIKeys().last_name:last_name,APIKeys().profile_picture:profile_picture,APIKeys().mobile_number:mobile_number,APIKeys().new_mobile:new_mobile,APIKeys().new_email:new_email,APIKeys().email_id:email_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
            
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        if  self.new_mobile  == "1"
                        {
                            let OTP = resonse_status.value(forKey: "otp") as? Int ?? 0
                            let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                            let user_info = user_information.value(forKey: "userInformation") as? NSDictionary ?? [:]
                            print(user_info)
                            let user_id = user_information.value(forKey: "mobile_number") as? String ?? ""
                            print(user_id)
                            let otp_view = self.storyboard?.instantiateViewController(withIdentifier: "OtpView") as! OtpView
                            otp_view.otp_value = OTP
                            otp_view.user_id = user_id
                            otp_view.new_user_id = "\(user_info.value(forKey: "user_id") as? Int ?? 0)"
                            otp_view.modalPresentationStyle = .fullScreen
                            self.present(otp_view, animated: true, completion: nil)
                        }
                        else
                        {
                            let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                            let user_info = user_information.value(forKey: "userInformation") as? NSDictionary ?? [:]
                            print(user_info)
                            let otp_view = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
                            otp_view.user_id = "\(user_info.value(forKey: "user_id") as? Int ?? 0)"
                            otp_view.modalPresentationStyle = .fullScreen
                            self.present(otp_view, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //# Check Validation
    
    func Check_validation() -> Bool
    {
        if self.email_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().email_empty
            return false
        }
        if EmailValidation().isValidEmail(testStr: self.email_txt.text!) == false
        {
            self.error_message = ResponseMessage().email_valid
            return false
        }
        if self.Phone_number_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().phone_number_empty
            return false
        }
        if self.Phone_number_txt.text!.count < 6 || self.Phone_number_txt.text!.count > 15
        {
            self.error_message = ResponseMessage().phone_number_limit
            return false
        }
        self.error_message = ""
        return true
    }
}

