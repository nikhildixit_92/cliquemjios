//
//  LoginView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import FBSDKShareKit
import AuthenticationServices

class LoginView: UIViewController,ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding
{
    
    
    
    @IBOutlet weak var password_img: UIImageView!
    @IBOutlet weak var user_phone_img: UIImageView!
    @IBOutlet weak var Apple_ID_btn: UIButton!
    @IBOutlet weak var email_txt: CustomUITextField!
    @IBOutlet weak var donthave_lbl: UILabel!
    @IBOutlet weak var password_txt: CustomUITextField!
    @IBOutlet weak var show_password_btn: UIButton!
    @IBOutlet weak var forgot_password_btn: UIButton!
    @IBOutlet weak var sign_in_btn: KButton!
    @IBOutlet weak var fb_btn: UIButton!
    
    var Fb_logindata = NSDictionary()
    let loginManager = LoginManager()
    
    var show_password = false
    var error_message = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let complete_s = "Don’t have an account? Sign Up"
        let change_font = "Sign Up"
        
        let range = (complete_s as NSString).range(of: change_font)
        
        let attribute = NSMutableAttributedString.init(string: complete_s)
        attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Inter-Bold", size: 14)! , range: range)
        attribute.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
        self.donthave_lbl.attributedText = attribute
        
        self.user_phone_img.layer.cornerRadius = 5.0
        self.user_phone_img.layer.masksToBounds = true
        
        self.password_img.layer.cornerRadius = 5.0
        self.password_img.layer.masksToBounds = true
        
        if #available(iOS 13.0, *)
        {
            self.Apple_ID_btn.isHidden = false
        }
        else
        {
            self.Apple_ID_btn.isHidden = true
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func show_password_btn_Action(_ sender: Any)
    {
        if self.show_password == false
        {
            self.password_txt.isSecureTextEntry = true
            self.show_password = true
        }
        else
        {
            self.show_password = false
            self.password_txt.isSecureTextEntry = false
        }
    }
    @IBAction func forgot_password_btn_Action(_ sender: Any)
    {
        let forgot = self.storyboard?.instantiateViewController(withIdentifier: "ForgotView") as! ForgotView
        forgot.modalPresentationStyle = .fullScreen
        self.present(forgot, animated: true, completion: nil)
    }
    
    @IBAction func sign_in_btn_action(_ sender: Any)
    {
        if self.check_validation() == false
        {
            self.alertmessage(error: "", message: self.error_message)
        }
        else
        {
            
            self.ws_Login_API(email_id: self.email_txt.text!, Device_token: Constant().Device_Token, password: self.password_txt.text!, flag: Constant().flag)
        }
        
    }
    
    @IBAction func fb_btn_Action(_ sender: Any)
    {
        self.login_fb()
    }
    
    @IBAction func dont_have_btn_Action(_ sender: Any)
    {
        let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
        signup.modalPresentationStyle = .fullScreen
        self.present(signup, animated: true, completion: nil)
    }
    
    
    @IBAction func Apple_ID_action(_ sender: Any)
    {
        self.signInButtonTapped()
    }
    
    @objc private func signInButtonTapped()
    {
        if #available(iOS 13.0, *) {
            let authorizationProvider = ASAuthorizationAppleIDProvider()
            let request = authorizationProvider.createRequest()
            if #available(iOS 13.0, *) {
                request.requestedScopes = [.email,.fullName]
            } else {
                // Fallback on earlier versions
            }
            
            if #available(iOS 13.0, *) {
                let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                authorizationController.delegate = self
                authorizationController.presentationContextProvider = self
                authorizationController.performRequests()
            } else {
                // Fallback on earlier versions
            }
            
            
            
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    //# Check Validation
    
    func check_validation() -> Bool
    {
        if self.email_txt.text?.isNumeric == true
        {
            if self.email_txt.text?.isEmpty == true
            {
                self.error_message = ResponseMessage().email_phone_empty
                return false
            }
        }
        else
        {
            if self.email_txt.text?.isEmpty == true
            {
                self.error_message = ResponseMessage().email_phone_empty
                return false
            }
            if EmailValidation().isValidEmail(testStr: self.email_txt.text ?? "") == false
            {
                self.error_message = ResponseMessage().email_valid
                return false
            }
        }
        if self.password_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().password_empty
            return false
        }
        
        self.error_message = ""
        return true
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if #available(iOS 13.0, *) {
            guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential else {
                return
            }
            
            self.ws_check_social_userAPI(user_id: appleIDCredential.user, login_type:Constant().login_type_apple , first_name: appleIDCredential.fullName?.givenName ?? "", last_name: appleIDCredential.fullName?.familyName ?? "", pic_url: "", gender: "3", birthday: "", email: appleIDCredential.email ?? "")
            
        } else {
            // Fallback on earlier versions
        }
        
        
        
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
        print("AppleID Credential failed with error: \(error.localizedDescription)")
    }
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    
    //# Social_login API
    
    func ws_social_LoginAPI(login_type:String,fb_id:String,google_id:String,apple_id:String,email_id:String,user_type:String,user_status:String,gender:String,date_of_birth:String,first_name:String,last_name:String,profile_picture:String,mobile_number:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().social_login)")
            
            let dict = [APIKeys().login_type:login_type,APIKeys().fb_id:fb_id,APIKeys().google_id:google_id,APIKeys().apple_id:apple_id,APIKeys().user_type:user_type,APIKeys().user_status:user_status,APIKeys().gender:gender,APIKeys().date_of_birth:date_of_birth,APIKeys().first_name:first_name,APIKeys().last_name:last_name,APIKeys().profile_picture:profile_picture,APIKeys().mobile_number:mobile_number]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                self.loginManager.logOut()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        UserDefaults.standard.set(resonse_status, forKey: "user_info")
                        
                        let main = UIStoryboard.init(name: "Main", bundle: nil)
                        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                        let navigation = UINavigationController.init(rootViewController: drawerC)
                        navigation.setNavigationBarHidden(true, animated: true)
                        appDelegate.window?.rootViewController = navigation
                        appDelegate.window?.makeKeyAndVisible()
                        self.present(navigation, animated: true, completion: nil)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# Check Social Login
    
    
    func ws_check_social_userAPI(user_id:String,login_type:String,first_name:String,last_name:String,pic_url:String,gender:String,birthday:String,email:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().ws_check_social_user)")
            
            let dict = [APIKeys().login_type:login_type,APIKeys().social_id:user_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                

                self.loginManager.logOut()
            
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        let user_info = user_information.value(forKey: "userInformation") as? NSDictionary ?? [:]
                        
                        let dob = user_info.value(forKey: "user_birth_date") as? String ?? ""
                        
                        if dob.isEmpty == true
                        {
                            let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
                            age.user_id = "\(user_info.value(forKey: "user_id") as? Int ?? 0)"
                            age.come_from = "login"
                            age.modalPresentationStyle = .fullScreen
                            self.present(age, animated: true, completion: nil)
                        }
                        else
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            
                            let main = UIStoryboard.init(name: "Main", bundle: nil)
                            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                            let navigation = UINavigationController.init(rootViewController: drawerC)
                            navigation.setNavigationBarHidden(true, animated: true)
                            appDelegate.window?.rootViewController = navigation
                            appDelegate.window?.makeKeyAndVisible()
                        }
                    }
                    else if status_login == 400
                    {
                        let ask = self.storyboard?.instantiateViewController(withIdentifier: "AskingEmailView") as! AskingEmailView
                        ask.modalPresentationStyle = .fullScreen
                        ask.user_id = user_id
                        ask.first_name = first_name
                        ask.last_name = last_name
                        ask.Login_type = login_type
                        ask.email = email
                        ask.gender = gender
                        ask.birthday = birthday
                        ask.pic_url = pic_url
                        ask.come_social = login_type
                        ask.mobile_n = ""
                        self.present(ask, animated: true, completion: nil)
                    }
                    else if status_login == 401
                    {
                        let ask = self.storyboard?.instantiateViewController(withIdentifier: "AskingEmailView") as! AskingEmailView
                        ask.modalPresentationStyle = .fullScreen
                        ask.user_id = user_id
                        ask.first_name = first_name
                        ask.last_name = last_name
                        ask.Login_type = login_type
                        
                        if login_type == "fb"
                        {
                            ask.email = email
                        }
                        else
                        {
                            ask.email = ( Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:] ).value(forKey: "email") as? String ?? ""
                        }
                        ask.gender = gender
                        ask.birthday = birthday
                        ask.pic_url = pic_url
                        ask.come_social = login_type
                        ask.mobile_n = ""
                        
                        self.present(ask, animated: true, completion: nil)
                    }
                    else if status_login == 402
                    {
                        let ask = self.storyboard?.instantiateViewController(withIdentifier: "AskingEmailView") as! AskingEmailView
                        ask.modalPresentationStyle = .fullScreen
                        ask.user_id = user_id
                        ask.first_name = first_name
                        ask.last_name = last_name
                        ask.Login_type = login_type
                        
                        if login_type == "fb"
                        {
                            ask.email = email
                        }
                        else
                        {
                            ask.email = ( Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:] ).value(forKey: "email") as? String ?? ""
                        }
                        
                        let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        let mobile = user_information.value(forKey: "mobile_number") as? String ?? ""
                        print(user_id)
                        
                        // ask.email = email
                        ask.gender = gender
                        ask.birthday = birthday
                        ask.pic_url = pic_url
                        ask.come_social = login_type
                        ask.mobile_n = mobile
                        self.present(ask, animated: true, completion: nil)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# ws-Login API
    
    
    func ws_Login_API(email_id:String,Device_token:String,password:String,flag:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().login)")
            
            let dict = [APIKeys().email_id:email_id,APIKeys().token:Device_token,APIKeys().password:password,APIKeys().flag:flag]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        
                        let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        let user_info = user_information.value(forKey: "userInformation") as? NSDictionary ?? [:]
                        
                        let dob = user_info.value(forKey: "user_birth_date") as? String ?? ""
                        
                        if dob.isEmpty == true
                        {
                            let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
                            age.user_id = "\(user_info.value(forKey: "user_id") as? Int ?? 0)"
                            age.come_from = "login"
                            age.modalPresentationStyle = .fullScreen
                            self.present(age, animated: true, completion: nil)
                        }
                        else
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            
                            let main = UIStoryboard.init(name: "Main", bundle: nil)
                            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                            let navigation = UINavigationController.init(rootViewController: drawerC)
                            navigation.setNavigationBarHidden(true, animated: true)
                            appDelegate.window?.rootViewController = navigation
                            appDelegate.window?.makeKeyAndVisible()
                        }
                        
                        
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    //# Fb_login Delegate
    
    
    func login_fb()
    {
        if AccessToken.isCurrentAccessTokenActive == true
        {
            //self.fbgooglelogout(message: custom_message().alreadysocial_login)
        }
        else
        {
            // let loginManager = LoginManager()
            loginManager.logIn(permissions: [.email,.publicProfile], viewController: self) { (loginresult) in
                
                switch loginresult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                //  loginManager.logOut()
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        let request = GraphRequest.init(graphPath: "me", parameters: ["fields":"email,name,id,picture.type(large),first_name,last_name,gender,birthday"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: .get)
        //GraphRequest(graphPath: "me", parameters: ["fields":"email,name,id,picture.type(large),first_name,last_name"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: nil)
        
        request.start { (response, result, error) in
            
            if(error == nil)
            {
                let fb_info = result as? NSDictionary ?? [:]
                print(fb_info)
                
                let user_id = fb_info.value(forKey: "id") as? String ?? ""
                let first_name = fb_info.value(forKey: "first_name") as? String ?? ""
                let last_name = fb_info.value(forKey: "last_name") as? String ?? ""
                let email = fb_info.value(forKey: "email") as? String ?? ""
                let picture  = fb_info.value(forKey: "picture") as? NSDictionary ?? [:]
                print(picture)
                let phone = fb_info.value(forKey: "phone") as? String ?? ""
                print(phone)
                
                let pic_data = picture.value(forKey: "data") as? NSDictionary ?? [:]
                print(pic_data)
                let pic_url = pic_data.value(forKey: "url") as? String ?? ""
                
                print(pic_url)
                
                print(user_id,first_name,last_name,email)
                
                
                let birthday = fb_info.value(forKey: "birthday") as? String ?? ""
                let gender = fb_info.value(forKey: "gender") as? String ?? ""
                
                var gender_status = String()
                
                if gender.lowercased() == "male"
                {
                    gender_status = "1"
                }
                else if gender.lowercased() == "female"
                {
                    gender_status = "2"
                }
                else if gender.lowercased() == "other"
                {
                    gender_status = "3"
                }
                
                print(user_id,first_name,last_name,birthday,gender)
                
                
                self.ws_check_social_userAPI(user_id: user_id, login_type: Constant().login_type_fb, first_name: first_name, last_name: last_name, pic_url: pic_url, gender: gender_status, birthday: birthday, email: email)
                
                
                //self.ws_social_LoginAPI(login_type: Constant().login_type_fb, fb_id: user_id, google_id: "", apple_id: "", email_id: email, user_type: "", user_status: Constant().user_status, gender: gender_status, date_of_birth: birthday, first_name: first_name, last_name: last_name, profile_picture: pic_url, mobile_number:"")
                
                
            }
            else
            {
                self.loginManager.logOut()
                print(error)
            }
        }
    }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}










////
////  LoginView.swift
////  CliqueMJSwift
////
////  Created by nikhil on 01/04/20.
////  Copyright © 2020 nikhil. All rights reserved.
////
//
//import UIKit
//import SVProgressHUD
//import FacebookCore
//import FacebookLogin
//import FBSDKLoginKit
//import FBSDKShareKit
//
//class LoginView: UIViewController {
//
//    @IBOutlet weak var password_img: UIImageView!
//    @IBOutlet weak var user_phone_img: UIImageView!
//    @IBOutlet weak var Apple_ID_btn: UIButton!
//    @IBOutlet weak var email_txt: CustomUITextField!
//    @IBOutlet weak var donthave_lbl: UILabel!
//    @IBOutlet weak var password_txt: CustomUITextField!
//    @IBOutlet weak var show_password_btn: UIButton!
//    @IBOutlet weak var forgot_password_btn: UIButton!
//    @IBOutlet weak var sign_in_btn: KButton!
//    @IBOutlet weak var fb_btn: UIButton!
//
//    var Fb_logindata = NSDictionary()
//    let loginManager = LoginManager()
//
//    var show_password = false
//    var error_message = ""
//
//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//
//        let complete_s = "Don’t have an account? Sign Up"
//        let change_font = "Sign Up"
//
//        let range = (complete_s as NSString).range(of: change_font)
//
//        let attribute = NSMutableAttributedString.init(string: complete_s)
//        attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Inter-Bold", size: 14) , range: range)
//        attribute.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
//        self.donthave_lbl.attributedText = attribute
//
//        self.user_phone_img.layer.cornerRadius = 5.0
//        self.user_phone_img.layer.masksToBounds = true
//
//        self.password_img.layer.cornerRadius = 5.0
//        self.password_img.layer.masksToBounds = true
//
//        // Do any additional setup after loading the view.
//    }
//
//    @IBAction func show_password_btn_Action(_ sender: Any)
//    {
//        if self.show_password == false
//        {
//            self.password_txt.isSecureTextEntry = true
//            self.show_password = true
//        }
//        else
//        {
//            self.show_password = false
//            self.password_txt.isSecureTextEntry = false
//        }
//    }
//    @IBAction func forgot_password_btn_Action(_ sender: Any)
//    {
//        let forgot = self.storyboard?.instantiateViewController(withIdentifier: "ForgotView") as! ForgotView
//        self.present(forgot, animated: true, completion: nil)
//    }
//
//    @IBAction func sign_in_btn_action(_ sender: Any)
//    {
//        if self.check_validation() == false
//        {
//            self.alertmessage(error: "", message: self.error_message)
//        }
//        else
//        {
//
//            self.ws_Login_API(email_id: self.email_txt.text!, Device_token: Constant().Device_Token, password: self.password_txt.text!, flag: Constant().flag)
//        }
//
//    }
//
//    @IBAction func fb_btn_Action(_ sender: Any)
//    {
//       self.login_fb()
//    }
//
//    @IBAction func dont_have_btn_Action(_ sender: Any)
//    {
//        let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
//        self.present(signup, animated: true, completion: nil)
//     }
//
//
//    @IBAction func Apple_ID_action(_ sender: Any)
//    {
//
//    }
//
//    //# Check Validation
//
//    func check_validation() -> Bool
//    {
//        if self.email_txt.text?.isNumeric == true
//        {
//            if self.email_txt.text?.isEmpty == true
//            {
//                self.error_message = ResponseMessage().phone_number_empty
//                return false
//            }
//        }
//        else
//        {
//            if self.email_txt.text?.isEmpty == true
//            {
//                self.error_message = ResponseMessage().email_empty
//                return false
//            }
//            if EmailValidation().isValidEmail(testStr: self.email_txt.text ?? "") == false
//            {
//                self.error_message = ResponseMessage().email_valid
//                return false
//            }
//        }
//        if self.password_txt.text?.isEmpty == true
//        {
//            self.error_message = ResponseMessage().password_empty
//            return false
//        }
//
//        self.error_message = ""
//        return true
//    }
//
//    //# Social_login API
//
//    func ws_social_LoginAPI(login_type:String,fb_id:String,google_id:String,apple_id:String,email_id:String,user_type:String,user_status:String,gender:String,date_of_birth:String,first_name:String,last_name:String,profile_picture:String)
//    {
//        if Connectivity.isConnectedToInternet()
//        {
//            SVProgressHUD.show(withStatus: "Please wait")
//            SVProgressHUD.setDefaultMaskType(.black)
//
//            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().social_login)")
//
//            let dict = [APIKeys().login_type:login_type,APIKeys().fb_id:fb_id,APIKeys().google_id:google_id,APIKeys().apple_id:apple_id,APIKeys().user_type:user_type,APIKeys().user_status:user_status,APIKeys().gender:gender,APIKeys().date_of_birth:date_of_birth,APIKeys().first_name:first_name,APIKeys().last_name:last_name,APIKeys().profile_picture:profile_picture]
//
//            print(dict)
//
//            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
//
//                SVProgressHUD.dismiss()
//
//                if status == true
//                {
//                    let resonse_status = response as? NSDictionary ?? [:]
//                    print(resonse_status)
//
//                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
//
//                    if status_login == 200
//                    {
//                        UserDefaults.standard.set(resonse_status, forKey: "user_info")
//
//                        let main = UIStoryboard.init(name: "Main", bundle: nil)
//                        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
//                        let navigation = UINavigationController.init(rootViewController: drawerC)
//                        navigation.setNavigationBarHidden(true, animated: true)
//                        appDelegate.window?.rootViewController = navigation
//                        appDelegate.window?.makeKeyAndVisible()
//                    }
//                    else
//                    {
//                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
//                        self.alertmessage(error: "", message: message)
//                    }
//                }
//                else
//                {
//                    self.alertmessage(error: "", message: ResponseMessage().something_error)
//                }
//
//            }
//        }
//        else
//        {
//            self.alertmessage(error: "", message: ResponseMessage().no_internet)
//        }
//    }
//
//
//
//    //# ws-Login API
//
//
//    func ws_Login_API(email_id:String,Device_token:String,password:String,flag:String)
//    {
//        if Connectivity.isConnectedToInternet()
//        {
//            SVProgressHUD.show(withStatus: "Please wait")
//            SVProgressHUD.setDefaultMaskType(.black)
//
//            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().login)")
//
//            let dict = [APIKeys().email_id:email_id,APIKeys().Device_token:Device_token,APIKeys().password:password,APIKeys().flag:flag]
//
//            print(dict)
//
//            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
//
//                SVProgressHUD.dismiss()
//
//                if status == true
//                {
//                    let resonse_status = response as? NSDictionary ?? [:]
//                    print(resonse_status)
//
//                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
//
//                    if status_login == 200
//                    {
//                        UserDefaults.standard.set(resonse_status, forKey: "user_info")
//
//                        let main = UIStoryboard.init(name: "Main", bundle: nil)
//                        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
//                        let navigation = UINavigationController.init(rootViewController: drawerC)
//                        navigation.setNavigationBarHidden(true, animated: true)
//                        appDelegate.window?.rootViewController = navigation
//                        appDelegate.window?.makeKeyAndVisible()
//                    }
//                    else
//                    {
//                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
//                        self.alertmessage(error: "", message: message)
//                    }
//                }
//                else
//                {
//                    self.alertmessage(error: "", message: ResponseMessage().something_error)
//                }
//
//            }
//        }
//        else
//        {
//            self.alertmessage(error: "", message: ResponseMessage().no_internet)
//        }
//    }
//
//
//    //# Fb_login Delegate
//
//
//    func login_fb()
//    {
//        if AccessToken.isCurrentAccessTokenActive == true
//        {
//            //self.fbgooglelogout(message: custom_message().alreadysocial_login)
//        }
//        else
//        {
//            // let loginManager = LoginManager()
//            loginManager.logIn(permissions: [.email,.publicProfile], viewController: self) { (loginresult) in
//
//                switch loginresult {
//                case .failed(let error):
//                    print(error)
//                case .cancelled:
//                    print("User cancelled login.")
//                //  loginManager.logOut()
//                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
//                    self.getFBUserData()
//                }
//            }
//        }
//    }
//
//    func getFBUserData(){
//        let request = GraphRequest.init(graphPath: "me", parameters: ["fields":"email,name,id,picture.type(large),first_name,last_name,gender,birthday"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: .get)
//        //GraphRequest(graphPath: "me", parameters: ["fields":"email,name,id,picture.type(large),first_name,last_name"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: nil)
//
//        request.start { (response, result, error) in
//
//            if(error == nil)
//            {
//                let fb_info = result as? NSDictionary ?? [:]
//                print(fb_info)
//
//                let user_id = fb_info.value(forKey: "id") as? String ?? ""
//                let first_name = fb_info.value(forKey: "first_name") as? String ?? ""
//                let last_name = fb_info.value(forKey: "last_name") as? String ?? ""
//                let email = fb_info.value(forKey: "email") as? String ?? ""
//                let picture  = fb_info.value(forKey: "picture") as? NSDictionary ?? [:]
//                print(picture)
//
//                let pic_data = picture.value(forKey: "data") as? NSDictionary ?? [:]
//                print(pic_data)
//                let pic_url = pic_data.value(forKey: "url") as? String ?? ""
//
//                print(pic_url)
//
//                print(user_id,first_name,last_name,email)
//
//
//                let birthday = fb_info.value(forKey: "birthday") as? String ?? ""
//                let gender = fb_info.value(forKey: "gender") as? String ?? ""
//
//                var gender_status = String()
//
//                if gender.lowercased() == "male"
//                {
//                    gender_status = "1"
//                }
//                else if gender.lowercased() == "female"
//                {
//                    gender_status = "2"
//                }
//                else if gender.lowercased() == "other"
//                {
//                    gender_status = "3"
//                }
//
//                print(user_id,first_name,last_name,birthday,gender)
//
//
//                self.ws_social_LoginAPI(login_type: Constant().login_type_fb, fb_id: user_id, google_id: "", apple_id: "", email_id: email, user_type: "", user_status: Constant().user_status, gender: gender_status, date_of_birth: birthday, first_name: first_name, last_name: last_name, profile_picture: pic_url)
//
//
//            }
//            else
//            {
//                print(error)
//            }
//        }
//    }
//
//    //# Alert Messages
//
//    func alertmessage(error:String,message:String)
//    {
//        let alertController = UIAlertController(title: error,
//                                                message: message,
//                                                preferredStyle: .alert)
//
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(defaultAction)
//
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//}
