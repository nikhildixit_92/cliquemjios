//
//  LoyalityDealNextView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 14/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoyalityDealNextView: UIViewController {

    @IBOutlet weak var new_sahre_btn: UIButton!
    @IBOutlet weak var disp_img: UIImageView!
    @IBOutlet weak var required_buds_lbl: UILabel!
    @IBOutlet weak var buds_view: UILabel!
    @IBOutlet weak var disp_name: UILabel!
    @IBOutlet weak var enter_amount_txt: CustomUITextField!
    @IBOutlet weak var enter_user_amount_txt: CustomUITextField!
    @IBOutlet weak var share_btn: UIButton!
    @IBOutlet weak var bud_percent_Txt: UILabel!
    @IBOutlet weak var not_now_btn: UIButton!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var main_custom_view: UIView!
    var come_from = String()
    var index_value = Int()
    var errormessage = ""
    var Profile_redeem_data:CliqueDeal?
    var new_buds = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.main_custom_view.clipsToBounds = true
        self.main_custom_view.layer.cornerRadius = 30
        self.main_custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.main_custom_view.isUserInteractionEnabled = true
        
        
        if come_from == "loyalitydeal"
        {
            self.share_btn.isHidden = true
            
            self.disp_name.text = RedeemList[0].data.dispensaryName ?? ""
             let dispensries_img_S = RedeemList[0].data.profilePicture ?? ""
             let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
             self.disp_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                             placeholderImage: UIImage(named: ""),
                                             options: .refreshCached,
                                             completed: nil)
             let dict_c = RedeemList[0].data.deals[self.index_value]
             
             let fullString = NSMutableAttributedString(string: "You have")
             
             // create our NSTextAttachment
             let image1Attachment = NSTextAttachment()
             image1Attachment.image = UIImage(named: "loyalGreen")
             
             // wrap the attachment in its own attributed string so we can append it
             let image1String = NSAttributedString(attachment: image1Attachment)
             
             // add the NSTextAttachment wrapper to our full string, then add some more text.
             fullString.append(image1String)
             fullString.append(NSAttributedString(string: " \(RedeemList[0].data.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
             
             fullString.append(NSAttributedString(string:"Loyality Buds™"))
             
             self.buds_view.attributedText = fullString
            
            let fullString1 = NSMutableAttributedString(string: "Required ")
            
            // create our NSTextAttachment
            let image1Attachment1 = NSTextAttachment()
            image1Attachment1.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String1 = NSAttributedString(attachment: image1Attachment1)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString1.append(image1String1)
            fullString1.append(NSAttributedString(string: " \(dict_c.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
            
            fullString1.append(NSAttributedString(string:"Loyality Buds™"))
            
            self.required_buds_lbl.attributedText = fullString1
            
            // self.new_index_timer = dict_c.startTime ?? 0
             self.bud_percent_Txt.text = dict_c.title ?? ""
            self.enter_user_amount_txt.keyboardType = .phonePad
            
        }
        else if come_from == "homeprofiledata"
        {
            self.share_btn.isHidden = true
            
            self.disp_name.text = Profile_redeem_data?.dispensaryName ?? ""
             let dispensries_img_S = Profile_redeem_data?.dispenaryLogo ?? ""
             let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
             self.disp_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                             placeholderImage: UIImage(named: ""),
                                             options: .refreshCached,
                                             completed: nil)
             let dict_c = Profile_redeem_data
             
             let fullString = NSMutableAttributedString(string: "You have")
             
             // create our NSTextAttachment
             let image1Attachment = NSTextAttachment()
             image1Attachment.image = UIImage(named: "loyalGreen")
             
             // wrap the attachment in its own attributed string so we can append it
             let image1String = NSAttributedString(attachment: image1Attachment)
             
             // add the NSTextAttachment wrapper to our full string, then add some more text.
             fullString.append(image1String)
             fullString.append(NSAttributedString(string: " \(new_buds) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
             
             fullString.append(NSAttributedString(string:"Loyality Buds™"))
             
             self.buds_view.attributedText = fullString
            
            let fullString1 = NSMutableAttributedString(string: "Required ")
            
            // create our NSTextAttachment
            let image1Attachment1 = NSTextAttachment()
            image1Attachment1.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String1 = NSAttributedString(attachment: image1Attachment1)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString1.append(image1String1)
            fullString1.append(NSAttributedString(string: " \(dict_c?.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
            
            fullString1.append(NSAttributedString(string:"Loyality Buds™"))
            
            self.required_buds_lbl.attributedText = fullString1
            
            // self.new_index_timer = dict_c.startTime ?? 0
            self.bud_percent_Txt.text = dict_c?.title ?? ""
            self.enter_user_amount_txt.keyboardType = .phonePad
        }
    
        
        // Do any additional setup after loading the view.
    }
    
    func CheckValidation() -> Bool
    {
        if self.enter_user_amount_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().purchase_amount
            return false
        }
        if self.enter_amount_txt.text?.isEmpty == true
        {
            self.errormessage = ResponseMessage().code_amount
            return false
        }
        
        self.errormessage = ""
        return true
        
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func share_btn_Action(_ sender: Any)
    {
        
    }
    
    @IBAction func save_btn_action(_ sender: Any)
    {
        if CheckValidation() == false
        {
            self.alertmessage(error: "", message: self.errormessage)
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            if come_from == "loyalitydeal"
            {
                let dict_c = RedeemList[0].data.deals[self.index_value]
                
                self.Redeem_API(user_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", offer_id: "\(dict_c.id ?? 0)", campaion_type: self.enter_amount_txt.text!, amount: self.enter_user_amount_txt.text!)
            }
            else if come_from == "homeprofiledata"
            {
                let dict_c = Profile_redeem_data
                
                self.Redeem_API(user_id: "\(userInformation.value(forKey: "user_id") as? Int ?? 0)", offer_id: "\(dict_c?.id ?? 0)", campaion_type: self.enter_amount_txt.text!, amount: self.enter_user_amount_txt.text!)
            }
            
            
        }
        
        
    }
    
    @IBAction func not_now_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func Redeem_API(user_id:String,offer_id:String,campaion_type:String,amount:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().new_ws_redeem)")
            
            let dict  = [APIKeys().user_id:user_id,APIKeys().offer_id:offer_id,APIKeys().compaign_code:campaion_type,APIKeys().amount:amount]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "status") as? Int ?? 0
                    
                    if response_status == 200
                    {
                        let remaining_bd = response_Data.value(forKey: "remaining_buds") as? Int ?? 0
                        
                        let Mjdeal = self.storyboard?.instantiateViewController(withIdentifier: "LoyltyDealSuccess") as! LoyltyDealSuccess
                        Mjdeal.remaining_buds = remaining_bd
                        self.present(Mjdeal, animated: true, completion: nil)
                    }
                    else
                    {
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
