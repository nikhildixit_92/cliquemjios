//
//  LoyalityDealView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 14/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoyalityDealView: UIViewController {

    @IBOutlet weak var buds_percen_Txy: UILabel!
    @IBOutlet weak var required_bud_value: UILabel!
    @IBOutlet weak var loyality_bus_lbl: UILabel!
    @IBOutlet weak var share_btn: UIButton!
    @IBOutlet weak var disp_name: UILabel!
    @IBOutlet weak var dis_img: UIImageView!
    @IBOutlet weak var not_now_btn: UIButton!
    @IBOutlet weak var redeem_btn: UIButton!
    @IBOutlet weak var main_custom_view: UIView!
    var come_from = String()
    var dispensay_id = String()
    var inde_val = Int()
    var redeem_profile:CliqueDeal?
    var new_buds = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.main_custom_view.clipsToBounds = true
        self.main_custom_view.layer.cornerRadius = 30
        self.main_custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.main_custom_view.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view.
        
        if come_from == "homeprofiledata"
        {
            self.disp_name.text = redeem_profile?.dispensaryName ?? ""
             let dispensries_img_S = redeem_profile?.dispenaryLogo ?? ""
             let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
             self.dis_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                             placeholderImage: UIImage(named: ""),
                                             options: .refreshCached,
                                             completed: nil)
             let dict_c = redeem_profile
             
             let fullString = NSMutableAttributedString(string: "You have ")
             
             // create our NSTextAttachment
             let image1Attachment = NSTextAttachment()
             image1Attachment.image = UIImage(named: "loyalGreen")
             
             // wrap the attachment in its own attributed string so we can append it
             let image1String = NSAttributedString(attachment: image1Attachment)
             
             // add the NSTextAttachment wrapper to our full string, then add some more text.
             fullString.append(image1String)
             fullString.append(NSAttributedString(string: " \(new_buds) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
             
             fullString.append(NSAttributedString(string:"Loyality Buds™"))
             
             self.loyality_bus_lbl.attributedText = fullString
             
             let fullString1 = NSMutableAttributedString(string: "Required ")
             
             // create our NSTextAttachment
             let image1Attachment1 = NSTextAttachment()
             image1Attachment1.image = UIImage(named: "loyalGreen")
             
             // wrap the attachment in its own attributed string so we can append it
             let image1String1 = NSAttributedString(attachment: image1Attachment1)
             
             // add the NSTextAttachment wrapper to our full string, then add some more text.
             fullString1.append(image1String1)
             fullString1.append(NSAttributedString(string: " \(dict_c?.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
             
             fullString1.append(NSAttributedString(string:"Loyality Buds™"))
             
             self.required_bud_value.attributedText = fullString1
             
             
            
             
            // self.new_index_timer = dict_c.startTime ?? 0
             self.buds_percen_Txy.text = dict_c?.title ?? ""
        }
        else
        {
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                  let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            self.Chek_Deal(user_id:"\(userInformation.value(forKey: "user_id") as? Int ?? 0)", User_lat: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.latitude ?? 0.0)", user_lng: "\(LocationSingleton.sharedInstance.locationManager?.location?.coordinate.longitude ?? 0.0)", disp_id: dispensay_id)
        }
        
        
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func redeem_action(_ sender: Any)
    {
        if come_from == "homeprofiledata"
        {
            let deal = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityDealNextView") as! LoyalityDealNextView
            deal.come_from = "homeprofiledata"
            deal.index_value = self.inde_val
            deal.Profile_redeem_data = redeem_profile
            deal.new_buds = self.new_buds
            deal.modalPresentationStyle = .fullScreen
            self.present(deal, animated: true, completion: nil)
        }
        else
        {
            let deal = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityDealNextView") as! LoyalityDealNextView
            deal.come_from = "loyalitydeal"
            deal.index_value = self.inde_val
            deal.modalPresentationStyle = .fullScreen
            self.present(deal, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func not_now_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func share_btn_action(_ sender: Any)
    {
        
    }
    
    
    
    func Chek_Deal(user_id:String,User_lat:String,user_lng:String,disp_id:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string:"\(Base_url().base_url)\(APIName().new_ws_check_dispensary_deal)")
            
            let dict  = [APIKeys().user_id:user_id,APIKeys().user_lat:User_lat,APIKeys().user_lng:user_lng,APIKeys().dispensary_id:disp_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                SVProgressHUD.dismiss()
                RedeemList = []
                if status == true
                {
                    let response_Data = response as? NSDictionary ?? [:]
                    print(response_Data)
                    
                    let response_status = response_Data.value(forKey: "staus") as? Int ?? 0
                    
                    if response_status == 200
                    {
                        
                        
                        let red_list = response as? [String:Any] ?? [:]
                        
                        RedeemList.append(RedeemdispensaryList.init(fromDictionary: red_list))
                        
                        self.disp_name.text = RedeemList[0].data.dispensaryName ?? ""
                        let dispensries_img_S = RedeemList[0].data.profilePicture ?? ""
                        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        self.dis_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                                        placeholderImage: UIImage(named: ""),
                                                        options: .refreshCached,
                                                        completed: nil)
                        let dict_c = RedeemList[0].data.deals[self.inde_val]
                        
                        let fullString = NSMutableAttributedString(string: "You have ")
                        
                        // create our NSTextAttachment
                        let image1Attachment = NSTextAttachment()
                        image1Attachment.image = UIImage(named: "loyalGreen")
                        
                        // wrap the attachment in its own attributed string so we can append it
                        let image1String = NSAttributedString(attachment: image1Attachment)
                        
                        // add the NSTextAttachment wrapper to our full string, then add some more text.
                        fullString.append(image1String)
                        fullString.append(NSAttributedString(string: " \(RedeemList[0].data.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
                        
                        fullString.append(NSAttributedString(string:"Loyality Buds™"))
                        
                        self.loyality_bus_lbl.attributedText = fullString
                        
                        let fullString1 = NSMutableAttributedString(string: "Required ")
                        
                        // create our NSTextAttachment
                        let image1Attachment1 = NSTextAttachment()
                        image1Attachment1.image = UIImage(named: "loyalGreen")
                        
                        // wrap the attachment in its own attributed string so we can append it
                        let image1String1 = NSAttributedString(attachment: image1Attachment1)
                        
                        // add the NSTextAttachment wrapper to our full string, then add some more text.
                        fullString1.append(image1String1)
                        fullString1.append(NSAttributedString(string: " \(dict_c.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
                        
                        fullString1.append(NSAttributedString(string:"Loyality Buds™"))
                        
                        self.required_bud_value.attributedText = fullString1
                        
                        let dispensries_img_S1 = dict_c.logo ?? ""
                        let disp_img1 = dispensries_img_S1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                       
                        
                       // self.new_index_timer = dict_c.startTime ?? 0
                        self.buds_percen_Txy.text = dict_c.title ?? ""
                        //self.newTimer_index()
                    }
                    else
                    {
                        let msg = response_Data.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: msg)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    func alertmessage(error:String,message:String)
       {
           let alertController = UIAlertController(title: error,
                                                   message: message,
                                                   preferredStyle: .alert)
           let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
           alertController.addAction(defaultAction)
           
           self.present(alertController, animated: true, completion: nil)
       }
       
       func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
           return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
       }
}
