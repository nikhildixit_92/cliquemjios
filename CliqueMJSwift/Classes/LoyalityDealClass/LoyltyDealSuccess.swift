//
//  LoyltyDealSuccess.swift
//  CliqueMJSwift
//
//  Created by nikhil on 28/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class LoyltyDealSuccess: UIViewController {

    var come_from = String()
    @IBOutlet weak var very_sub_heading_Txt: UILabel!
    @IBOutlet weak var sub_heading_Txt: UILabel!
    @IBOutlet weak var rede_txt: UILabel!
    @IBOutlet weak var ok_btn: UIButton!
    @IBOutlet weak var custom_view: UIView!
    var remaining_buds = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        if come_from == ""
        {
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "You have")
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            
            fullString.append(NSAttributedString(string: " \(remaining_buds) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
            
            fullString.append(NSAttributedString(string:"Loyality Buds™ Left"))
            // draw the result in a label
            self.sub_heading_Txt.attributedText = fullString
        }
        else
        {
            self.rede_txt.text = "Transfer Success!"
            self.very_sub_heading_Txt.text = "See more deals you can redeem from this dispensary."
            
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "You have")
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: "100 Loyalty Buds™ left"))
            
            // draw the result in a label
            self.sub_heading_Txt.attributedText = fullString
        }
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ok_btn_action(_ sender: Any)
    {
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        let navigation = UINavigationController.init(rootViewController: drawerC)
        navigation.setNavigationBarHidden(true, animated: true)
        appDelegate.window?.rootViewController = navigation
        appDelegate.window?.makeKeyAndVisible()
    }

}
