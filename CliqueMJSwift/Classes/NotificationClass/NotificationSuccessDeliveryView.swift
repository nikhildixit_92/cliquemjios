//
//  NotificationSuccessDeliveryView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class NotificationSuccessDeliveryView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    
    @IBOutlet weak var sucess_delivery_Table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "NotificationDeliveryFirstCell", bundle: nil)
        self.sucess_delivery_Table.register(nib, forCellReuseIdentifier: "NotificationDeliveryFirstCell")

        let nib1 = UINib.init(nibName: "Notification_deliveryCell", bundle: nil)
        self.sucess_delivery_Table.register(nib1, forCellReuseIdentifier: "Notification_deliveryCell")
        
        let nib2 = UINib.init(nibName: "NotificationDeliveryDetailsCell", bundle: nil)
        self.sucess_delivery_Table.register(nib2, forCellReuseIdentifier: "NotificationDeliveryDetailsCell")
        
        
        let nib3 = UINib.init(nibName: "TrackingHistoryCell", bundle: nil)
        self.sucess_delivery_Table.register(nib3, forCellReuseIdentifier: "TrackingHistoryCell")
        
        
        self.sucess_delivery_Table.rowHeight = 177.0
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            return 2
        }
        else if section == 4
        {
            return 2
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationDeliveryFirstCell", for: indexPath) as! NotificationDeliveryFirstCell
            
           
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Notification_deliveryCell", for: indexPath) as! Notification_deliveryCell
             cell.heading_txt.text  = "Delivery Details"
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationDeliveryDetailsCell", for: indexPath) as! NotificationDeliveryDetailsCell
            
            if indexPath.row == 0
            {
                cell.order_txt.text = "Order Reference Number"
                cell.order_number_txt.text = "#12SD8AS7DGHAS"
            }
            else
            {
                cell.order_txt.text = "Tracking Number"
                cell.order_number_txt.text = "#8AS7DGHAS"
            }
            
            return cell
        }
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Notification_deliveryCell", for: indexPath) as! Notification_deliveryCell
            
            cell.heading_txt.text  = "Tracking History"
            cell.heading_txt.font = UIFont.init(name: "Inter-Regular", size: 12.0)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrackingHistoryCell", for: indexPath) as! TrackingHistoryCell
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
             return UITableView.automaticDimension
        }
        else if indexPath.section == 1
        {
             return 71
        }
        else if indexPath.section == 2
        {
            return 86
        }
        else if indexPath.section == 3
        {
             return 71
        }
        else
        {
            return UITableView.automaticDimension
        }
       
    }
    

    
    
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
