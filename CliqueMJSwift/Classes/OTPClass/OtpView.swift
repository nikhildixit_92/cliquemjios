//
//  OtpView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 06/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class OtpView: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var Resend_otp_btn: UIButton!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var otp4_txt: CustomUITextField!
    @IBOutlet weak var otp1_txt: CustomUITextField!
    @IBOutlet weak var otp2_txt: CustomUITextField!
    @IBOutlet weak var opt3_txt: CustomUITextField!
    @IBOutlet weak var opt_img3: UIImageView!
    @IBOutlet weak var opt_img2: UIImageView!
    @IBOutlet weak var opt_img1: UIImageView!
    @IBOutlet weak var opt_img: UIImageView!
    @IBOutlet weak var Skip_btn: UIButton!
    
    var come_from = String()
    var otp_value = Int()
    var user_id = String()
    var new_user_id = String()
    var error_message = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.opt_img.layer.cornerRadius = 5.0
        self.opt_img.layer.masksToBounds = true
        
        self.opt_img1.layer.cornerRadius = 5.0
        self.opt_img1.layer.masksToBounds = true
        
        self.opt_img2.layer.cornerRadius = 5.0
        self.opt_img2.layer.masksToBounds = true
        
        self.opt_img3.layer.cornerRadius = 5.0
        self.opt_img3.layer.masksToBounds = true
        
        
        
        otp1_txt.delegate = self
        otp2_txt.delegate = self
        opt3_txt.delegate = self
        otp4_txt.delegate = self
        
        otp1_txt.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        otp2_txt.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        opt3_txt.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        otp4_txt.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        
        
        self.perform(#selector(open_keyboard_Action), with: nil, afterDelay: 3.0)
        
        if self.come_from == "profile"
        {
             self.Resend_OTPAPI(user_id: new_user_id, mobile_number: self.user_id)
        }
        else
        {
             self.setotp()
        }
        
        
       
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: UITextField Action
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if   text?.count == 0 {
            switch textField{
            case otp1_txt:
                otp1_txt.becomeFirstResponder()
            case otp2_txt:
                otp2_txt.becomeFirstResponder()
            case opt3_txt:
                opt3_txt.becomeFirstResponder()
            case otp4_txt:
                otp4_txt.becomeFirstResponder()
            default:
                break
            }
        }
        else if text?.count == 1 {
            switch textField{
            case otp1_txt:
                otp2_txt.becomeFirstResponder()
            case otp2_txt:
                opt3_txt.becomeFirstResponder()
            case opt3_txt:
                otp4_txt.becomeFirstResponder()
            case otp4_txt:
                otp4_txt.resignFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    @objc func open_keyboard_Action()
    {
        self.otp1_txt.becomeFirstResponder()
    }
    
    //# Break OTP
    
    func setotp()
    {
        let new_otp = "\(self.otp_value)"
        
        let digits = new_otp.compactMap{Int(String($0))}
        
        print(digits)
        
        let new_key = digits as NSArray
        print(new_key)
        
        self.otp1_txt.text = String(new_key.object(at: 0) as? Int ?? 0)
        self.otp2_txt.text = String(new_key.object(at: 1) as? Int ?? 0)
        self.opt3_txt.text = String(new_key.object(at: 2) as? Int ?? 0)
        self.otp4_txt.text = String(new_key.object(at: 3) as? Int ?? 0)
        
        print("\(otp1_txt.text!)\(otp2_txt.text!)\(opt3_txt.text!)\(otp4_txt.text!)")
    }
    
    
    @IBAction func submit_btn_action(_ sender: Any)
    {
        if Check_validation() == false
        {
            self.alertmessage(error: "", message: self.error_message)
        }
        else
        {
            
            let new_otp = "\(otp1_txt.text!)\(otp2_txt.text!)\(opt3_txt.text!)\(otp4_txt.text!)"
            
            self.Verify_OTP_API(user_id: new_user_id, mobile_number: self.user_id, otp: new_otp)
        }
        
        
    }
    
    @IBAction func resend_otp_action(_ sender: Any)
    {
        self.Resend_OTPAPI(user_id: new_user_id, mobile_number: self.user_id)
    }
    
    //# Verify_OTP API
    
    func Verify_OTP_API(user_id:String,mobile_number:String,otp:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().verify_otp)")
            
            let dict = [APIKeys().user_id:user_id,APIKeys().mobile_number:mobile_number,APIKeys().otp:otp]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        if self.come_from == "profile"
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            
                            let main = UIStoryboard.init(name: "Main", bundle: nil)
                            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                            let navigation = UINavigationController.init(rootViewController: drawerC)
                            navigation.setNavigationBarHidden(true, animated: true)
                            appDelegate.window?.rootViewController = navigation
                            appDelegate.window?.makeKeyAndVisible()
                        }
                        else
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            
                            let permission = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
                            permission.user_id = self.new_user_id
                            permission.modalPresentationStyle = .fullScreen
                            self.present(permission, animated: true, completion: nil)
                            
                        }
                        
                            
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    
    //# Resend OTP View
    
    func Resend_OTPAPI(user_id:String,mobile_number:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().resend_otp)")
            
            let dict = [APIKeys().user_id:user_id,APIKeys().mobile_number:mobile_number]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        self.otp_value = resonse_status.value(forKey: "otp") as? Int ?? 0
                        self.setotp()
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    
    //# Check Validation
    
    func Check_validation() -> Bool
    {
        let complete_otp = "\(self.otp1_txt.text ?? "")\(self.otp2_txt.text ?? "")\(self.opt3_txt.text ?? "")\(self.otp4_txt.text ?? "")"
        
        if complete_otp.isEmpty == true
        {
            self.error_message = ResponseMessage().otp_empty
            return false
        }
        
        self.error_message = ""
        return true
    }
    
    
    @IBAction func Skip_btn_action(_ sender: Any)
    {
        if self.come_from == "profile"
        {
           // UserDefaults.standard.set(resonse_status, forKey: "user_info")
            
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigation = UINavigationController.init(rootViewController: drawerC)
            navigation.setNavigationBarHidden(true, animated: true)
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
        else
        {
            let permission = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
            permission.modalPresentationStyle = .fullScreen
            permission.user_id = new_user_id
            self.present(permission, animated: true, completion: nil)
        }
        
        
    }
    
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
