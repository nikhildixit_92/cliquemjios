//
//  DispanseryAskView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DispanseryAskView: UIViewController {

    @IBOutlet weak var next_btn: KButton!
    @IBOutlet weak var heading_Txt: UILabel!
    @IBOutlet weak var user_img: ANCustomView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func next_btn_action(_ sender: Any)
    {
        let pintrest = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as!DispenseryDetailsView
        pintrest.come_from = "already"
        self.present(pintrest, animated: true, completion: nil)
    }
    @IBAction func No_btn_action(_ sender: Any)
    {
        let follow = self.storyboard?.instantiateViewController(withIdentifier: "FollowDispenseryLocation") as! FollowDispenseryLocation
        self.present(follow, animated: true, completion: nil)
    }
}
