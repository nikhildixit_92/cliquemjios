//
//  OnboardingView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class OnboardingView: UIViewController {

    @IBOutlet weak var next_btn: KButton!
    @IBOutlet weak var sub_heading_txt: UILabel!
    @IBOutlet weak var heading_txt: UILabel!
    @IBOutlet weak var user_img: ANCustomView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2.0
        self.user_img.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func next_btn_action(_ sender: Any)
    {
        
    }
}
