//
//  productInterestView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class productInterestView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    @IBOutlet weak var next_btn: KButton!
    @IBOutlet weak var product_listing_table: UITableView!
    @IBOutlet weak var product_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "productIntCell", bundle: nil)
        self.product_table.register(nib, forCellReuseIdentifier: "productIntCell")
    
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func skip_btn_action(_ sender: Any)
    {
        
    }
    
    @IBAction func dismiss_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func select_all_product_action(_ sender: Any)
    {
        
    }
    
    @IBAction func next_action(_ sender: Any)
    {
        let askView = self.storyboard?.instantiateViewController(withIdentifier: "NotificationView") as! NotificationView
        self.present(askView, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productIntCell", for: indexPath) as! productIntCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 65
    }
    
}
