//
//  pendingreferlView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class pendingreferlView: UIViewController {

   
    @IBOutlet weak var pending_referl_btn: UIButton!
    @IBOutlet weak var sharing_txt: UILabel!
    @IBOutlet weak var custom_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.custom_view.layer.cornerRadius = 5.0
        self.custom_view.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func not_now_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pending_referel_action(_ sender: Any)
    {
        let follow_R = self.storyboard?.instantiateViewController(withIdentifier: "FollowRedeemView") as! FollowRedeemView
        self.present(follow_R, animated: true, completion: nil)
    }
}
