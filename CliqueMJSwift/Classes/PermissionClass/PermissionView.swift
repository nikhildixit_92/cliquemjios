//
//  PermissionView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 06/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class PermissionView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func enable_action(_ sender: Any)
    {
        let permission = self.storyboard?.instantiateViewController(withIdentifier: "DispanseryAskView") as! DispanseryAskView
        self.present(permission, animated: true, completion: nil)
    }
    
    
}
