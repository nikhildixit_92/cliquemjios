//
//  QRCodeProfileView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class QRCodeProfileView: UIViewController {

    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var qr_img: UIImageView!
    @IBOutlet weak var Ok_btn: UIButton!
    @IBOutlet weak var short_name_Txt: UILabel!
    @IBOutlet weak var user_name_txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.qr_img.image = #imageLiteral(resourceName: "sample_qr")
        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                   
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        self.user_name_txt.text = "\(userInformation.value(forKey:"first_name") as? String ?? "") \(userInformation.value(forKey:"last_name") as? String ?? "")"
        
        self.short_name_Txt.text = user_data.value(forKey: "nickname") as? String ?? ""
        
        print(user_data)
        
        let dispensries_img_S = userInformation.value(forKey: "qr_code") as? String ?? ""
        let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.qr_img.sd_setImage(with: URL(string:disp_img ?? ""),
                      placeholderImage: #imageLiteral(resourceName: "sample_qr"),
                      options: .refreshCached,
                      completed: nil)
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ok_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
