//
//  RedeemVocDoneView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class RedeemVocDoneView: UIViewController {

    var come_from = String()
    @IBOutlet weak var sub_heading_Txt: UILabel!
    @IBOutlet weak var rede_txt: UILabel!
    @IBOutlet weak var ok_btn: UIButton!
    @IBOutlet weak var custom_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        if come_from == "MjDealcome"
        {
            self.rede_txt.text = "Redeem Success!"
            self.sub_heading_Txt.text = "See more deals you can redeem from the dispensary"
        }
        else
        {
            self.rede_txt.text = "Redeem Success!"
            self.sub_heading_Txt.text = "You successfully redeem clique deal"
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ok_btn_action(_ sender: Any)
    {
        if come_from == "MjDealcome"
        {
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigation = UINavigationController.init(rootViewController: drawerC)
            navigation.setNavigationBarHidden(true, animated: true)
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
        else
        {
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigation = UINavigationController.init(rootViewController: drawerC)
            navigation.setNavigationBarHidden(true, animated: true)
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
}
