//
//  RedeemVochNextView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class RedeemVochNextView: UIViewController {

    @IBOutlet weak var dispencery_txt: CustomUITextField!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var cancel_btn: KButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_view.layer.cornerRadius = 5.0
        self.dispensery_view.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refer_btn_action(_ sender: Any)
    {
        let doneview = self.storyboard?.instantiateViewController(withIdentifier: "RedeemVocDoneView") as! RedeemVocDoneView
        self.present(doneview, animated: true, completion: nil)
    }
    
}
