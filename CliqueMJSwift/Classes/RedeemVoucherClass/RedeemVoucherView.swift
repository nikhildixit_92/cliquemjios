//
//  RedeemVoucherView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class RedeemVoucherView: UIViewController {

    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var cancel_btn: KButton!
    @IBOutlet weak var save_btn: KButton!
    @IBOutlet weak var Dispensery_txt: CustomUITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_view.layer.cornerRadius = 5.0
        self.dispensery_view.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save_action(_ sender: Any)
    {
        let redeem = self.storyboard?.instantiateViewController(withIdentifier: "RedeemVochNextView") as! RedeemVochNextView
        self.present(redeem, animated: true, completion: nil)
    }
    
    @IBAction func cancel_btn_action(_ sender: Any)
    {
        
    }
}
