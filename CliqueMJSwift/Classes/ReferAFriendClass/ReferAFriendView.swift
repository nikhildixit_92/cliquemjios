//
//  ReferAFriendView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import Social
import MessageUI

class ReferAFriendView: UIViewController,MFMessageComposeViewControllerDelegate  {

    @IBOutlet weak var copy_code_view: UIView!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var Code_Txt: UILabel!
    @IBOutlet weak var copy_code_btn: KButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.copy_code_view.isHidden = true
        // Do any additional setup after loading the view.
    }

    @IBAction func coy_code_action(_ sender: Any)
    {
        self.copy_code_view.isHidden = false
        UIPasteboard.general.string = ""
//        let reward_view = self.storyboard?.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
//        self.present(reward_view, animated: true, completion: nil)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now()+15.0) {
            self.copy_code_view.isHidden = true
        }
        
    }
    
    @IBAction func back_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func fb_btn_action(_ sender: Any)
    {
        if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
                   vc.setInitialText("Share CliqueMJ")
                   // vc.add(UIImage(named: "myImage.jpg")!)
                   vc.add(URL(string: ""))
                   vc.add(URL(string: ""))
                   present(vc, animated: true)
                    }
    }
    
    @IBAction func whtsp_btn_action(_ sender: Any)
    {
        let url  = NSURL(string: "whatsapp://send?text=Download%20this%20app%20hi")
               
               //Text which will be shared on WhatsApp is: "Hello Friends, Sharing some data here... !"
               
               if UIApplication.shared.canOpenURL(url! as URL) {
                   UIApplication.shared.open(url! as URL, options: [:]) { (success) in
                       if success {
                           print("WhatsApp accessed successfully")
                       } else {
                           print("Error accessing WhatsApp")
                       }
                   }
               }
    }
    
    @IBAction func message_btn_action(_ sender: Any)
    {
        if (MFMessageComposeViewController.canSendText()) {
                   let controller = MFMessageComposeViewController()
                   controller.body = "Message Body"
                    //controller.recipients = [""]
                   controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
               }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
           //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
       }

//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = false
//       }
    
    
    @IBAction func allshare_btn_action(_ sender: Any)
    {
        let firstActivityItem = "Text you want"
         let secondActivityItem : NSURL = NSURL(string: "http//:urlyouwant")!
         // If you want to put an image
        // let image : UIImage = UIImage(named: "dropicon")!

         let activityViewController : UIActivityViewController = UIActivityViewController(
             activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)

         // This lines is for the popover you need to show in iPad
         activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)

         // Anything you want to exclude
         activityViewController.excludedActivityTypes = [
             UIActivity.ActivityType.postToWeibo,
             UIActivity.ActivityType.print,
             UIActivity.ActivityType.assignToContact,
             UIActivity.ActivityType.saveToCameraRoll,
             UIActivity.ActivityType.addToReadingList,
             UIActivity.ActivityType.postToFlickr,
             UIActivity.ActivityType.postToVimeo,
             UIActivity.ActivityType.postToTencentWeibo
         ]

         self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func sell_all_btn_action(_ sender: Any)
    {
        

    }
}
