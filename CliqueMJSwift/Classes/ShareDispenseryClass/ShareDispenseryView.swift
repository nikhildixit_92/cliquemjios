//
//  ShareDispenseryView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import Social

class ShareDispenseryView: UIViewController {

    @IBOutlet weak var share_btn_img: UIImageView!
    @IBOutlet weak var share_link_img: UIImageView!
    @IBOutlet weak var share_whatts_img: UIImageView!
    @IBOutlet weak var share_message_img: UIImageView!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var share_btn: KButton!
    @IBOutlet weak var share_link_btn: KButton!
    @IBOutlet weak var share_whattsup_btn: KButton!
    @IBOutlet weak var share_Messager_btn: KButton!
    @IBOutlet weak var subheading_txt: UILabel!
    @IBOutlet weak var heading_txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.share_btn_img.layer.cornerRadius = 18.0
        self.share_btn_img.layer.masksToBounds = true
        
        self.share_link_img.layer.cornerRadius = 18.0
        self.share_link_img.layer.masksToBounds = true
        
        self.share_whatts_img.layer.cornerRadius = 18.0
        self.share_whatts_img.layer.masksToBounds = true
        
        self.share_message_img.layer.cornerRadius = 18.0
        self.share_message_img.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func not_now_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func share_btn_action(_ sender: Any)
    {
        let firstActivityItem = "Text you want"
        let secondActivityItem : NSURL = NSURL(string: "http//:urlyouwant")!
        // If you want to put an image
       // let image : UIImage = UIImage(named: "dropicon")!

        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)

        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)

        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]

        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func share_link_btn(_ sender: Any)
    {
        UIPasteboard.general.string = ""
    }
    @IBAction func share_whttsup_action(_ sender: Any)
    {
        let url  = NSURL(string: "whatsapp://send?text=Download%20this%20app%20Hello Friends, Sharing some data here... !")
        
        //Text which will be shared on WhatsApp is: "Hello Friends, Sharing some data here... !"
        
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.open(url! as URL, options: [:]) { (success) in
                if success {
                    print("WhatsApp accessed successfully")
                } else {
                    print("Error accessing WhatsApp")
                }
            }
        }
    }
    @IBAction func share_messaging_action(_ sender: Any)
    {
        if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
            vc.setInitialText("Share CliqueMJ")
            // vc.add(UIImage(named: "myImage.jpg")!)
            vc.add(URL(string: ""))
            vc.add(URL(string: ""))
            present(vc, animated: true)
               }
    }
}
