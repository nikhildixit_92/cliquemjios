//
//  SignUpView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 06/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import FBSDKShareKit
import AuthenticationServices

class SignUpView: UIViewController,ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding
{
    @IBOutlet weak var Apple_signIn: UIButton!
    
    @IBOutlet weak var email_Txt: CustomUITextField!
    @IBOutlet weak var fb_signIn: UIButton!
    var Fb_logindata = NSDictionary()
    let loginManager = LoginManager()
    
    var show_password = false
    var show_password1 = false
    var error_message = ""
    
    
    @IBOutlet weak var phone_num_img: UIImageView!
    @IBOutlet weak var phone_number_txt: CustomUITextField!
    @IBOutlet weak var user_img_img: UIImageView!
    @IBOutlet weak var user_name_Txt: CustomUITextField!
    @IBOutlet weak var password_img: UIImageView!
    @IBOutlet weak var password_txt: CustomUITextField!
    @IBOutlet weak var confirm_img: UIImageView!
    @IBOutlet weak var confirm_txt: CustomUITextField!
    @IBOutlet weak var sign_in_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user_img_img.layer.cornerRadius = 5.0
        self.user_img_img.layer.masksToBounds = true
        
        self.password_img.layer.cornerRadius = 5.0
        self.password_img.layer.masksToBounds = true
        
        self.phone_num_img.layer.cornerRadius = 5.0
        self.phone_num_img.layer.masksToBounds = true
        
        self.confirm_img.layer.cornerRadius = 5.0
        self.confirm_img.layer.masksToBounds = true
        
        self.phone_num_img.layer.cornerRadius = 5.0
        self.phone_num_img.layer.masksToBounds = true
        
        
        if #available(iOS 13.0, *)
        {
            self.Apple_signIn.isHidden = false
        }
        else
        {
            self.Apple_signIn.isHidden = true
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Sign_action(_ sender: Any)
    {
        
            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            login.modalPresentationStyle = .fullScreen
            self.present(login, animated: true, completion: nil)
       
    }
    
    @IBAction func apple_id_sign(_ sender: Any)
    {
        self.signInButtonTapped()
    }
    
    @IBAction func facebook_sign_action(_ sender: Any)
    {
        self.login_fb()
    }
    
    @IBAction func show_password(_ sender: Any)
    {
        if self.show_password == false
        {
            self.password_txt.isSecureTextEntry = true
            self.show_password = true
        }
        else
        {
            self.show_password = false
            self.password_txt.isSecureTextEntry = false
        }
    }
    @IBAction func confirm_password_show_Action(_ sender: Any)
    {
        if self.show_password1 == false
        {
            self.confirm_txt.isSecureTextEntry = true
            self.show_password1 = true
        }
        else
        {
            self.show_password1 = false
            self.confirm_txt.isSecureTextEntry = false
        }
    }
    
    @IBAction func sign_up_action(_ sender: Any)
    {
        
        if self.Check_validation() == false
        {
            self.alertmessage(error: "", message: self.error_message)
        }
        else
        {
            self.ws_registerAPI(user_name: self.user_name_Txt.text!, phone_number: self.phone_number_txt.text!, email: self.email_Txt.text!, password: self.password_txt.text!, confirmPassword: self.confirm_txt.text!, device_id: Constant().Device_Token, flag: Constant().flag)
        }
    }
    
    @objc private func signInButtonTapped()
    {
                if #available(iOS 13.0, *) {
                    let authorizationProvider = ASAuthorizationAppleIDProvider()
                    let request = authorizationProvider.createRequest()
                    if #available(iOS 13.0, *) {
                        request.requestedScopes = [.email,.fullName]
                    } else {
                        // Fallback on earlier versions
                    }
        
                    if #available(iOS 13.0, *) {
                        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                        authorizationController.delegate = self
                        authorizationController.presentationContextProvider = self
                        authorizationController.performRequests()
                    } else {
                        // Fallback on earlier versions
                    }
        
        
        
                } else {
                    // Fallback on earlier versions
                }
        
        
    }
    
        @available(iOS 13.0, *)
        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            if #available(iOS 13.0, *) {
                guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential else {
                    return
                }
                
                self.ws_check_social_userAPI(user_id: appleIDCredential.user, login_type:Constant().login_type_apple , first_name: appleIDCredential.fullName?.givenName ?? "", last_name: appleIDCredential.fullName?.familyName ?? "", pic_url: "", gender: "3", birthday: "", email: appleIDCredential.email ?? "")
                
                
            } else {
                // Fallback on earlier versions
            }
    
            
        }
        @available(iOS 13.0, *)
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            print(error.localizedDescription)
            print("AppleID Credential failed with error: \(error.localizedDescription)")
        }
    
        @available(iOS 13.0, *)
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return self.view.window!
        }
    
    
    //# Register API
    
    func ws_registerAPI(user_name:String,phone_number:String,email:String,password:String,confirmPassword:String,device_id:String,flag:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().registration)")
            
            let dict = [APIKeys().mobile_number:phone_number,APIKeys().nickname:user_name,APIKeys().email_id:email,APIKeys().password:password,APIKeys().token:device_id,APIKeys().flag:flag]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                       // UserDefaults.standard.set(resonse_status, forKey: "user_info")
                        let OTP = resonse_status.value(forKey: "otp") as? Int ?? 0
                        let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        let user_id = user_information.value(forKey: "mobile_number") as? String ?? ""
                        print(user_id)
                        let user_info = user_information.value(forKey: "userInformation") as? NSDictionary ?? [:]
                        let otp_view = self.storyboard?.instantiateViewController(withIdentifier: "OtpView") as! OtpView
                        otp_view.otp_value = OTP
                        otp_view.user_id = user_id
                        otp_view.new_user_id = "\(user_info.value(forKey: "user_id") as? Int ?? 0)"
                        otp_view.modalPresentationStyle = .fullScreen
                        self.present(otp_view, animated: true, completion: nil)
                        
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# Check Social Login
    
    func ws_check_social_userAPI(user_id:String,login_type:String,first_name:String,last_name:String,pic_url:String,gender:String,birthday:String,email:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().ws_check_social_user)")
            
            let dict = [APIKeys().login_type:login_type,APIKeys().social_id:user_id]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                 self.loginManager.logOut()
                SVProgressHUD.dismiss()
                
                if status == true
                {
                   
                    
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        let user_info = user_information.value(forKey: "userInformation") as? NSDictionary ?? [:]
                        
                        let dob = user_info.value(forKey: "user_birth_date") as? String ?? ""
                        
                        if dob.isEmpty == true
                        {
                            let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
                            age.user_id = "\(user_info.value(forKey: "user_id") as? Int ?? 0)"
                            age.come_from = "login"
                            age.modalPresentationStyle = .fullScreen
                            self.present(age, animated: true, completion: nil)
                        }
                        else
                        {
                            UserDefaults.standard.set(resonse_status, forKey: "user_info")
                            
                            let main = UIStoryboard.init(name: "Main", bundle: nil)
                            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                            let navigation = UINavigationController.init(rootViewController: drawerC)
                            navigation.setNavigationBarHidden(true, animated: true)
                            appDelegate.window?.rootViewController = navigation
                            appDelegate.window?.makeKeyAndVisible()
                        }
                    }
                    else if status_login == 400
                    {
                        let ask = self.storyboard?.instantiateViewController(withIdentifier: "AskingEmailView") as! AskingEmailView
                        ask.modalPresentationStyle = .fullScreen
                        ask.user_id = user_id
                        ask.first_name = first_name
                        ask.last_name = last_name
                        ask.Login_type = login_type
                        ask.email = email
                        ask.gender = gender
                        ask.birthday = birthday
                        ask.pic_url = pic_url
                        ask.come_social = login_type
                        ask.mobile_n = ""
                        self.present(ask, animated: true, completion: nil)
                    }
                    else if status_login == 401
                    {
                        let ask = self.storyboard?.instantiateViewController(withIdentifier: "AskingEmailView") as! AskingEmailView
                        ask.modalPresentationStyle = .fullScreen
                        ask.user_id = user_id
                        ask.first_name = first_name
                        ask.last_name = last_name
                        ask.Login_type = login_type
                        
                        if login_type == "fb"
                        {
                            ask.email = email
                        }
                        else
                        {
                            ask.email = ( Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:] ).value(forKey: "email") as? String ?? ""
                        }
                        ask.gender = gender
                        ask.birthday = birthday
                        ask.pic_url = pic_url
                        ask.come_social = login_type
                        ask.mobile_n = ""
                        
                        self.present(ask, animated: true, completion: nil)
                    }
                    else if status_login == 402
                    {
                        let ask = self.storyboard?.instantiateViewController(withIdentifier: "AskingEmailView") as! AskingEmailView
                        ask.modalPresentationStyle = .fullScreen
                        ask.user_id = user_id
                        ask.first_name = first_name
                        ask.last_name = last_name
                        ask.Login_type = login_type
                        
                        if login_type == "fb"
                        {
                            ask.email = email
                        }
                        else
                        {
                            ask.email = ( Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:] ).value(forKey: "email") as? String ?? ""
                        }
                        
                        let user_information = resonse_status.value(forKey: "user") as? NSDictionary ?? [:]
                        let mobile = user_information.value(forKey: "mobile_number") as? String ?? ""
                        print(user_id)
                        
                      // ask.email = email
                        ask.gender = gender
                        ask.birthday = birthday
                        ask.pic_url = pic_url
                        ask.come_social = login_type
                        ask.mobile_n = mobile
                        self.present(ask, animated: true, completion: nil)
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# Social_login API
    
    func ws_social_LoginAPI(login_type:String,fb_id:String,google_id:String,apple_id:String,email_id:String,user_type:String,user_status:String,gender:String,date_of_birth:String,first_name:String,last_name:String,profile_picture:String,mobile_number:String)
    {
        if Connectivity.isConnectedToInternet()
        {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.black)
            
            let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().social_login)")
            
            let dict = [APIKeys().login_type:login_type,APIKeys().fb_id:fb_id,APIKeys().google_id:google_id,APIKeys().apple_id:apple_id,APIKeys().user_type:user_type,APIKeys().user_status:user_status,APIKeys().gender:gender,APIKeys().date_of_birth:date_of_birth,APIKeys().first_name:first_name,APIKeys().last_name:last_name,APIKeys().profile_picture:profile_picture,APIKeys().mobile_number:mobile_number]
            
            print(dict)
            
            APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                
                SVProgressHUD.dismiss()
                
                if status == true
                {
                    let resonse_status = response as? NSDictionary ?? [:]
                    print(resonse_status)
                    
                    let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                    
                    if status_login == 200
                    {
                        UserDefaults.standard.set(resonse_status, forKey: "user_info")
                        
                        let main = UIStoryboard.init(name: "Main", bundle: nil)
                        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
                        let navigation = UINavigationController.init(rootViewController: drawerC)
                        navigation.setNavigationBarHidden(true, animated: true)
                        appDelegate.window?.rootViewController = navigation
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    else
                    {
                        let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                        self.alertmessage(error: "", message: message)
                    }
                }
                else
                {
                    self.alertmessage(error: "", message: ResponseMessage().something_error)
                }
                
            }
        }
        else
        {
            self.alertmessage(error: "", message: ResponseMessage().no_internet)
        }
    }
    
    //# Fb_login Delegate
    
    
    func login_fb()
    {
        if AccessToken.isCurrentAccessTokenActive == true
        {
            //self.fbgooglelogout(message: custom_message().alreadysocial_login)
        }
        else
        {
            // let loginManager = LoginManager()
            loginManager.logIn(permissions: [.email,.publicProfile], viewController: self) { (loginresult) in
                
                switch loginresult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                //  loginManager.logOut()
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        let request = GraphRequest.init(graphPath: "me", parameters: ["fields":"email,name,id,picture.type(large),first_name,last_name,gender,birthday"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: .get)
        //GraphRequest(graphPath: "me", parameters: ["fields":"email,name,id,picture.type(large),first_name,last_name"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: nil)
        
        request.start { (response, result, error) in
            
            if(error == nil)
            {
                let fb_info = result as? NSDictionary ?? [:]
                print(fb_info)
                
                let user_id = fb_info.value(forKey: "id") as? String ?? ""
                let first_name = fb_info.value(forKey: "first_name") as? String ?? ""
                let last_name = fb_info.value(forKey: "last_name") as? String ?? ""
                let email = fb_info.value(forKey: "email") as? String ?? ""
                let picture  = fb_info.value(forKey: "picture") as? NSDictionary ?? [:]
                print(picture)
                
                let pic_data = picture.value(forKey: "data") as? NSDictionary ?? [:]
                print(pic_data)
                let pic_url = pic_data.value(forKey: "url") as? String ?? ""
                
                print(pic_url)
                
                print(user_id,first_name,last_name,email)
                
                
                let birthday = fb_info.value(forKey: "birthday") as? String ?? ""
                let gender = fb_info.value(forKey: "gender") as? String ?? ""
                
                var gender_status = String()
                
                if gender.lowercased() == "male"
                {
                    gender_status = "1"
                }
                else if gender.lowercased() == "female"
                {
                    gender_status = "2"
                }
                else if gender.lowercased() == "other"
                {
                    gender_status = "3"
                }
                
                print(user_id,first_name,last_name,birthday,gender)
                
                self.ws_check_social_userAPI(user_id: user_id, login_type: Constant().login_type_fb, first_name: first_name, last_name: last_name, pic_url: pic_url, gender: gender_status, birthday: birthday, email: email)
            }
            else
            {
                self.loginManager.logOut()
                print(error as Any)
            }
        }
    }
    
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //# Check Validation
    
    func Check_validation() -> Bool
    {
        if self.user_name_Txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().user_name_empty
            return false
        }
        if passwordSpace().validate(string: self.user_name_Txt.text!) ==  false
        {
            self.error_message = ResponseMessage().empty_username_space
            return false
        }
        if self.phone_number_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().phone_number_empty
            return false
        }
        if self.phone_number_txt.text!.count < 6 || self.phone_number_txt!.text!.count > 15
        {
            self.error_message = ResponseMessage().phone_number_limit
            return false
        }
        if passwordSpace().validate(string: self.phone_number_txt.text!) == false
        {
            self.error_message = ResponseMessage().phone_space_error
            return false
        }
        if self.email_Txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().email_empty
            return false
        }
        if EmailValidation().isValidEmail(testStr: self.email_Txt.text!) == false
        {
            self.error_message = ResponseMessage().email_valid
            return false
        }
        if self.password_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().password_empty
            return false
        }
        if passwordSpace().validate(string: self.password_txt.text!) ==  false
        {
            self.error_message = ResponseMessage().passowrd_space_error
            return false
        }
        if self.confirm_txt.text?.isEmpty == true
        {
            self.error_message = ResponseMessage().confirm_password_empty
            return false
        }
        if self.password_txt.text! != self.confirm_txt.text!
        {
            self.error_message = ResponseMessage().password_confirm
            return false
        }
        
        self.error_message = ""
        return true
        
    }
    
}
