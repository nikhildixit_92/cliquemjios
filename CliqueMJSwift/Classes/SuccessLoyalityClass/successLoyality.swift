//
//  successLoyality.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class successLoyality: UIViewController {

    var come_from = String()
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_txt: UILabel!
    @IBOutlet weak var from_name_txt: UILabel!
    @IBOutlet weak var dashboard_btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.layer.cornerRadius = 5.0
        self.custom_view.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dashboard_btn_action(_ sender: Any)
    {
        if come_from == "collectloyality"
        {
            let dis = self.storyboard?.instantiateViewController(withIdentifier: "FollowRedeemView") as! FollowRedeemView
            self.present(dis, animated: true, completion: nil)
        }
        else
        {
            let dis = self.storyboard?.instantiateViewController(withIdentifier: "CompleteProfileView") as! CompleteProfileView
            self.present(dis, animated: true, completion: nil)
        }
        
       
        //self.dismiss(animated: true, completion: nil)
    }
    @IBAction func later_btn_action(_ sender: Any)
    {
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        let navigation = UINavigationController.init(rootViewController: drawerC)
        navigation.setNavigationBarHidden(true, animated: true)
        appDelegate.window?.rootViewController = navigation
        appDelegate.window?.makeKeyAndVisible()
    }
    
}
