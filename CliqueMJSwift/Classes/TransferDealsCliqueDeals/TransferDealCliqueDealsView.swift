//
//  TransferDealCliqueDealsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 03/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class TransferDealCliqueDealsView: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var Amount_view: UIView!
    @IBOutlet weak var transfer_buds_subheading_Txt: UILabel!
    @IBOutlet weak var store_name_subheading: UILabel!
    @IBOutlet weak var store_name_Txt: UILabel!
    @IBOutlet weak var store_img: UIImageView!
    @IBOutlet weak var refer_btn: UIButton!
    @IBOutlet weak var amount_txt: KButton!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var cancel_btn: KButton!
    @IBOutlet weak var save_btn: KButton!
    @IBOutlet weak var Dispensery_txt: CustomUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_view.layer.cornerRadius = 5.0
        self.dispensery_view.layer.masksToBounds = true
        
        self.Amount_view.layer.cornerRadius = 5.0
        self.Amount_view.layer.masksToBounds = true
        
        self.save_btn.addTarget(self, action: #selector(save_btn_action), for: .touchUpInside)
        self.cancel_btn.addTarget(self, action: #selector(cancel_btn_action), for: .touchUpInside)
        
        self.Dispensery_txt.delegate = self
        self.Dispensery_txt.addTarget(self, action:#selector(Editing_begin), for: .editingDidBegin)
        
        
        // create an NSMutableAttributedString that we'll append everything to
        let fullString = NSMutableAttributedString(string: "You have")
        
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "loyalGreen")
        
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: "124 Loyalty Buds"))
        
        // draw the result in a label
        self.store_name_subheading.attributedText = fullString
        
        
        // create an NSMutableAttributedString that we'll append everything to
        let fullString1 = NSMutableAttributedString(string: "Send ")
        
        // create our NSTextAttachment
        let image1Attachment1 = NSTextAttachment()
        image1Attachment1.image = UIImage(named: "MjDealIcon")
        
        // wrap the attachment in its own attributed string so we can append it
        let image1String1 = NSAttributedString(attachment: image1Attachment1)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString1.append(image1String1)
        fullString1.append(NSAttributedString(string: " Loyalty Buds from this dispensary to your friends."))
        
        // draw the result in a label
        self.transfer_buds_subheading_Txt.attributedText = fullString1
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func cancel_btn_action()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismiss_btn_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refer_btn_action(_ sender: Any)
    {
        let share = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
        self.present(share, animated: true, completion: nil)
    }
    
    @objc func save_btn_action()
    {
        let transfer_oyal = self.storyboard?.instantiateViewController(withIdentifier: "TransferbudsCliqueConfirmView") as! TransferbudsCliqueConfirmView
        self.present(transfer_oyal, animated: true, completion: nil)
    }
    
    @objc func Editing_begin(sender:UITextField)
    {
        let Search_tranfer = self.storyboard?.instantiateViewController(withIdentifier: "TransferSearchView") as! TransferbudsCliqueConfirmView
        self.present(Search_tranfer, animated: true, completion: nil)
    }
    
    @IBAction func qr_code_scan_Action(_ sender: Any)
    {
        let qr = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeView") as! QRCodeView
        self.present(qr, animated: true, completion: nil)
    }
    
    
}
