//
//  TransferbudsCliqueConfirmView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class TransferbudsCliqueConfirmView: UIViewController {

    @IBOutlet weak var amount_Txt: UILabel!
    @IBOutlet weak var user_name_txt: UILabel!
    @IBOutlet weak var refer_btn: UIButton!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var cancel_btn: KButton!
    @IBOutlet weak var save_btn: KButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_view.layer.cornerRadius = 5.0
        self.dispensery_view.layer.masksToBounds = true
        
        
        self.save_btn.addTarget(self, action: #selector(save_btn_Action), for: .touchUpInside)
        self.cancel_btn.addTarget(self, action: #selector(Go_back_action), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_btn_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refer_btn_action(_ sender: Any)
    {
        let share = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
        self.present(share, animated: true, completion: nil)
    }
    
    @objc func save_btn_Action()
    {
        let deal_transfer = self.storyboard?.instantiateViewController(withIdentifier: "LoyltyDealSuccess") as! LoyltyDealSuccess
        deal_transfer.come_from = "tranfer_buds"
        self.present(deal_transfer, animated: true, completion: nil)
    }
    
    @objc func Go_back_action()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
