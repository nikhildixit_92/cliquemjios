//
//  TransferBudsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class TransferBudsView: UIViewController,UITextFieldDelegate,SaveUser {
   
    @IBOutlet weak var new_amount_Txt: CustomUITextField!
    var to_ids = String()
    @IBOutlet weak var Amount_view: UIView!
    @IBOutlet weak var transfer_buds_subheading_Txt: UILabel!
    @IBOutlet weak var store_name_subheading: UILabel!
    @IBOutlet weak var store_name_Txt: UILabel!
    @IBOutlet weak var store_img: UIImageView!
    @IBOutlet weak var refer_btn: UIButton!
    @IBOutlet weak var amount_txt: KButton!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var cancel_btn: KButton!
    @IBOutlet weak var save_btn: KButton!
    @IBOutlet weak var Dispensery_txt: CustomUITextField!
    var come_from = String()
    var transfer_budsDisp:DispensaryList?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_view.layer.cornerRadius = 5.0
        self.dispensery_view.layer.masksToBounds = true
        
        self.Amount_view.layer.cornerRadius = 5.0
        self.Amount_view.layer.masksToBounds = true
        
        self.save_btn.addTarget(self, action: #selector(save_btn_action), for: .touchUpInside)
        
        self.Dispensery_txt.delegate = self
        self.Dispensery_txt.addTarget(self, action:#selector(Editing_begin), for: .editingDidBegin)
        
        
        if self.come_from == "homeprofiledata"
        {
            
            self.store_name_Txt.text = transfer_budsDisp?.dispensaryName ?? ""
            
            let dispensries_img_S = transfer_budsDisp?.profilePicture ?? ""
            let disp_img = dispensries_img_S.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            self.store_img.sd_setImage(with: URL(string:disp_img ?? ""),
                                            placeholderImage: UIImage(named: ""),
                                            options: .refreshCached,
                                            completed: nil)
            
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "You have")
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " \(transfer_budsDisp?.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
            fullString.append(NSAttributedString(string:"Loyality Buds™"))
            // draw the result in a label
            self.store_name_subheading.attributedText = fullString
            
            
            // create an NSMutableAttributedString that we'll append everything to
            let fullString1 = NSMutableAttributedString(string: "Send ")
            
            // create our NSTextAttachment
            let image1Attachment1 = NSTextAttachment()
            image1Attachment1.image = UIImage(named: "MjDealIcon")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String1 = NSAttributedString(attachment: image1Attachment1)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString1.append(image1String1)
            fullString1.append(NSAttributedString(string: " Loyalty Buds from this dispensary to your friends."))
            
            // draw the result in a label
            self.transfer_buds_subheading_Txt.attributedText = fullString1
        }
        else
        {
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "You have")
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: "124 Loyalty Buds"))
            
            // draw the result in a label
            self.store_name_subheading.attributedText = fullString
            
            
            // create an NSMutableAttributedString that we'll append everything to
            let fullString1 = NSMutableAttributedString(string: "Send ")
            
            // create our NSTextAttachment
            let image1Attachment1 = NSTextAttachment()
            image1Attachment1.image = UIImage(named: "MjDealIcon")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String1 = NSAttributedString(attachment: image1Attachment1)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString1.append(image1String1)
            fullString1.append(NSAttributedString(string: " Loyalty Buds from this dispensary to your friends."))
            
            // draw the result in a label
            self.transfer_buds_subheading_Txt.attributedText = fullString1
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_btn_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refer_btn_action(_ sender: Any)
    {
//        let share = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
//        self.present(share, animated: true, completion: nil)
    }
    
    @objc func save_btn_action()
    {
        let transfer_oyal = self.storyboard?.instantiateViewController(withIdentifier: "TransferloyalityView") as! TransferloyalityView
        transfer_oyal.modalPresentationStyle = .overFullScreen
        transfer_oyal.New_tranfer_List = transfer_budsDisp
        transfer_oyal.To_name = self.Dispensery_txt.text!
        transfer_oyal.Amount_string = self.new_amount_Txt.text!
        transfer_oyal.To_Id = self.to_ids
        self.present(transfer_oyal, animated: true, completion: nil)
    }
    
    @objc func Editing_begin(sender:UITextField)
    {
        let Search_tranfer = self.storyboard?.instantiateViewController(withIdentifier: "TransferSearchView") as! TransferSearchView
        Search_tranfer.SaveUserDelegate = self
        Search_tranfer.modalPresentationStyle = .overFullScreen
        
        self.present(Search_tranfer, animated: true, completion: nil)
    }
    
    @IBAction func qr_code_scan_Action(_ sender: Any)
    {
        let qr = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeView") as! QRCodeView
        self.present(qr, animated: true, completion: nil)
    }
    
    func SaveUserMethods(to_id: String, SearchUserId: String, nick_name: String, Full_name: String)
    {
        self.to_ids = to_id
        
        self.Dispensery_txt.text = "\(Full_name)@\(nick_name)"
        
    }
    
   
    
}
