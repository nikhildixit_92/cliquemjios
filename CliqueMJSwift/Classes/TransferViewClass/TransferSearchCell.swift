//
//  TransferSearchCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class TransferSearchCell: UITableViewCell {

    @IBOutlet weak var remove_btn: UIButton!
    @IBOutlet weak var nick_name_lbl: UILabel!
    @IBOutlet weak var full_name_lbl: UILabel!
    @IBOutlet weak var user_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
