//
//  TransferSearchView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SaveUser
{
    func SaveUserMethods(to_id:String,SearchUserId:String,nick_name:String,Full_name:String)
}

class TransferSearchView: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    var Save_UserList = NSArray()
    var SearchUser_List = NSArray()
    var SaveUserDelegate:SaveUser?
    var typing = false
    @IBOutlet weak var recent_table: UITableView!
    @IBOutlet weak var search_txt: CustomUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "TransferSearchCell", bundle: nil)
        self.recent_table.register(nib, forCellReuseIdentifier: "TransferSearchCell")
        
        self.search_txt.delegate = self
        self.search_txt.addTarget(self, action:#selector(check_text), for: .editingChanged)
        
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
        print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        self.GET_USerAPI(user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)", user_txt: "")
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.typing == false
        {
            return self.Save_UserList.count
        }
        else
        {
            return self.SearchUser_List.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if typing == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransferSearchCell", for: indexPath) as! TransferSearchCell
            
            let dict = self.Save_UserList.object(at:indexPath.row) as? NSDictionary ?? [:]
            cell.full_name_lbl.text = "\(dict.value(forKey:"first_name") as? String ?? "") \(dict.value(forKey:"last_name") as? String ?? "")"
            
            cell.nick_name_lbl.text = "@\(dict.value(forKey:"nickname") as? String ?? "")"
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransferSearchCell", for: indexPath) as! TransferSearchCell
            
            let dict = self.SearchUser_List.object(at:indexPath.row) as? NSDictionary ?? [:]
            cell.full_name_lbl.text = "\(dict.value(forKey:"first_name") as? String ?? "") \(dict.value(forKey:"last_name") as? String ?? "")"
            cell.remove_btn.isHidden = false
            cell.nick_name_lbl.text = "@\(dict.value(forKey:"nickname") as? String ?? "")"
            return cell
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.typing == false
        {
             let dict = self.Save_UserList.object(at:indexPath.row) as? NSDictionary ?? [:]
            SaveUserDelegate?.SaveUserMethods(to_id: "\(dict.value(forKey:"id") as? Int ?? 0)", SearchUserId: "", nick_name: dict.value(forKey:"nickname") as? String ?? "", Full_name: "\(dict.value(forKey:"first_name") as? String ?? "") \(dict.value(forKey:"last_name") as? String ?? "")")
            
             self.dismiss(animated: true, completion: nil)
            
        }
        else
        {
             let dict = self.SearchUser_List.object(at:indexPath.row) as? NSDictionary ?? [:]
            SaveUserDelegate?.SaveUserMethods(to_id: "\(dict.value(forKey:"id") as? Int ?? 0)", SearchUserId: "", nick_name: dict.value(forKey:"nickname") as? String ?? "", Full_name: "\(dict.value(forKey:"first_name") as? String ?? "") \(dict.value(forKey:"last_name") as? String ?? "")")
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 68
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scan_btn_action(_ sender: Any)
    {
        let reansfer = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeView") as! QRCodeView
        self.present(reansfer, animated: true, completion: nil)
    }
    
    @objc func check_text(sender:UITextField)
    {
        if search_txt.text?.count == 0
        {
            self.typing = false
            self.recent_table.reloadData()
        }
        else
        {
            self.typing = true
            
            let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
            print(user_data)
            let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
            
            self.Search_USerAPI(user_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)", user_txt: self.search_txt.text!)
        }
       
    }
    
    
    
    //# Set RecentUSer
       func Search_USerAPI(user_id:String,user_txt:String)
           {
               if Connectivity.isConnectedToInternet()
               {
                   SVProgressHUD.show(withStatus: "Please wait")
                   SVProgressHUD.setDefaultMaskType(.black)
                   
                   let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_search_user)")
                   
                let dict = [APIKeys().user_id:user_id,APIKeys().word:user_txt]
                   
                   print(dict)
                   
                   APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                       
                       SVProgressHUD.dismiss()
                       
                       if status == true
                       {
                           let resonse_status = response as? NSDictionary ?? [:]
                           print(resonse_status)
                           
                           let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                           
                           if status_login == 200
                           {
                               self.Save_UserList = resonse_status.value(forKey:"users") as? NSArray ?? []
                               
                               self.recent_table.reloadData()
                               
                           }
                           else
                           {
                               let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                               self.alertmessage(error: "", message: message)
                           }
                       }
                       else
                       {
                           self.alertmessage(error: "", message: ResponseMessage().something_error)
                       }
                       
                   }
               }
               else
               {
                   self.alertmessage(error: "", message: ResponseMessage().no_internet)
               }
           }
    
    
    
    //# Set RecentUSer
    func GET_USerAPI(user_id:String,user_txt:String)
        {
            if Connectivity.isConnectedToInternet()
            {
                SVProgressHUD.show(withStatus: "Please wait")
                SVProgressHUD.setDefaultMaskType(.black)
                
                let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_get_recent_tranferred_user)")
                
             let dict = [APIKeys().user_id:user_id]
                
                print(dict)
                
                APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                    
                    SVProgressHUD.dismiss()
                    
                    if status == true
                    {
                        let resonse_status = response as? NSDictionary ?? [:]
                        print(resonse_status)
                        
                        let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                        
                        if status_login == 200
                        {
                            self.Save_UserList = resonse_status.value(forKey:"users") as? NSArray ?? []
                            
                            self.recent_table.reloadData()
                            
                        }
                        else
                        {
                            let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                            self.alertmessage(error: "", message: message)
                        }
                    }
                    else
                    {
                        self.alertmessage(error: "", message: ResponseMessage().something_error)
                    }
                    
                }
            }
            else
            {
                self.alertmessage(error: "", message: ResponseMessage().no_internet)
            }
        }
        
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
