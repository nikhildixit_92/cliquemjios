//
//  TransferloyalityView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import SVProgressHUD

class TransferloyalityView: UIViewController {

    @IBOutlet weak var loyality_buds_txt: UILabel!
    @IBOutlet weak var store_name_tx: UILabel!
    @IBOutlet weak var store_img: UIImageView!
    @IBOutlet weak var amount_Txt: UILabel!
    @IBOutlet weak var user_name_txt: UILabel!
    @IBOutlet weak var refer_btn: UIButton!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_view: UIView!
    @IBOutlet weak var cancel_btn: KButton!
    @IBOutlet weak var save_btn: KButton!
    var come_from = String()
    var New_tranfer_List:DispensaryList?
    var To_name = String()
    var To_Id = String()
    var Amount_string = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.dispensery_view.layer.cornerRadius = 5.0
        self.dispensery_view.layer.masksToBounds = true
        
        
        self.save_btn.addTarget(self, action: #selector(save_btn_Action), for: .touchUpInside)
        self.cancel_btn.addTarget(self, action: #selector(Go_back_action), for: .touchUpInside)
        
        
        if come_from == "homeprofiledata"
        {
            self.user_name_txt.text = self.To_name
            self.amount_Txt.text = self.Amount_string
            
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "You have")
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "loyalGreen")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " \(New_tranfer_List?.bud ?? 0) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.fromHexaString(hex: colorcodes().button_color)]))
            fullString.append(NSAttributedString(string:"Loyality Buds™"))
            // draw the result in a label
            self.loyality_buds_txt.attributedText = fullString
            
        }
        else
        {
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_btn_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refer_btn_action(_ sender: Any)
    {
        let share = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
        self.present(share, animated: true, completion: nil)
    }
    
    @objc func save_btn_Action()
    {
        let user_data = Constant().User_info.value(forKey: "user") as? NSDictionary ?? [:]
                   print(user_data)
        let userInformation = user_data.value(forKey: "userInformation") as? NSDictionary ?? [:]
        
        self.TranferBud_USerAPI(from_id: "\(userInformation.value(forKey:"user_id") as? Int ?? 0)", to_id: self.To_Id, buds_amount: self.Amount_string, dispensary_id: "\(New_tranfer_List?.id ?? 0)")
    }
    
    @objc func Go_back_action()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    //# Set RecentUSer
    func TranferBud_USerAPI(from_id:String,to_id:String,buds_amount:String,dispensary_id:String)
       {
           if Connectivity.isConnectedToInternet()
           {
               SVProgressHUD.show(withStatus: "Please wait")
               SVProgressHUD.setDefaultMaskType(.black)
               
               let base_url = URL.init(string: "\(Base_url().base_url)\(APIName().new_ws_transfer_bud)")
               
            let dict = [APIKeys().from_id:from_id,APIKeys().to_id:to_id,APIKeys().bud:buds_amount,APIKeys().dispensary_id:dispensary_id]
               
               print(dict)
               
               APIMethods().PostAPI(url: base_url!, Parameters: dict, headers: [:]) { (response, status) in
                   
                   SVProgressHUD.dismiss()
                   
                   if status == true
                   {
                       let resonse_status = response as? NSDictionary ?? [:]
                       print(resonse_status)
                       
                       let status_login = resonse_status.value(forKey: "status") as? Int ?? 0
                       
                       if status_login == 200
                       {
                           let deal_transfer = self.storyboard?.instantiateViewController(withIdentifier: "LoyltyDealSuccess") as! LoyltyDealSuccess
                           deal_transfer.come_from = "tranfer_buds"
                           self.present(deal_transfer, animated: true, completion: nil)
                       }
                       else
                       {
                           let message  = resonse_status.value(forKey: "msg") as? String ?? ""
                           self.alertmessage(error: "", message: message)
                       }
                   }
                   else
                   {
                       self.alertmessage(error: "", message: ResponseMessage().something_error)
                   }
                   
               }
           }
           else
           {
               self.alertmessage(error: "", message: ResponseMessage().no_internet)
           }
       }
    
    //# Alert Messages
    
    func alertmessage(error:String,message:String)
    {
        let alertController = UIAlertController(title: error,
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
       
}
