//
//  Constant.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

let appDelegate = UIApplication.shared.delegate as! AppDelegate
//# MARK: color codes list

struct colorcodes
{
    let Steel_Grey = "1B1B29"
    let button_color = "2BC185"
    let Medium_Purple = "5B5BDE"
    let Sun_Glow = "2BC185"
    let Bittersweet = "FA7165"
    let Port_Gore = "202442"
    let Oxford_Blue = "363A55"
    let text_placeholder =  "9DB4D5"
    let text_color = "FFFFFF"

}

class CheckLocation
{
    func check_location() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return false
                
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
        } else {
            return false
        }
    }
}

extension UILabel {
    
    func addImageWith(name: String, behindText: Bool) {
        
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        let attachmentString = NSAttributedString(attachment: attachment)
        
        guard let txt = self.text else {
            return
        }
        
        if behindText {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSAttributedString(string: txt)
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }
}

extension UIColor
{
    class func fromHexaString(hex:String) -> UIColor
    {
        let scanner           = Scanner(string: hex)
        scanner.scanLocation  = 0
        var rgbValue: UInt64  = 0
        scanner.scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
}

struct Base_url
{
    let base_url = "http://vlcare.com/cliqueMJ_newUI_admin/api/"
}


struct APIName
{
    let date_birth = "ws-set-date-of-birth"
    let registration = "ws-registration"
    let verify_otp = "ws-verify-otp"
    let social_login = "ws-social-login"
    let login = "ws-login"
    let profile_data = "ws-user-profiledata"
    let resend_otp = "ws-resend-otp"
    let resend_expire = "ws-otp-expire"
    let forgot_pass = "ws-forgot-pass"
    let reset_password = "ws-reset-password"
    let get_user_dispensary = "ws-get-user-in-dispensary"
    let new_get_nearby_dispensary = "new-ws-get-nearby-dispensaries"
    let new_visit = "new-ws-visit"
    let interest_list = "new-ws-interest-list"
    let set_interest = "new-ws-set-interest"
    let get_interest = "new-ws-get-interest"
    let followd_dispensaries = "new-ws-get-followed-dispensaries"
    let follow_dispensary = "new-ws-follow-dispensary"
    let dispensary_details = "new-ws-get-dispensary-detail"
    let user_dispenseris = "new-ws-get-user-dispensaries"
    let productbydispensary = "new-ws-get-product-bydispenary"
    let product_detail = "new-ws-get-product-detail"
    let change_profile = "new-ws-change-profile"
    let check_nikname = "new-ws-check-nickname"
    let filter_dispensries = "new-ws-filter-dispensary"
    let sendbuds = "new-ws-send-budsms"
    let bud_by_dispensries = "new-ws-get-bud-by-dispensary"
    let search_user = "new-ws-search-user"
    let recent_user = "new-ws-recent-users"
    let recent_location = "new-ws-recent-locations"
    let transfer_buds = "new-ws-transfer-bud"
    let redeem = "new-ws-redeem"
    let generate_user_qr = "new-ws-generate-user-qr"
    let follow_collect = "new-ws-follow-collect"
    let get_my_coupan = "new-ws-get-my-coupons"
    let get_used_coupon = "new-ws-get-used-coupons"
    let reder_userdeal = "new-ws-refer-userdeal"
    let share_coupon = "new-ws-share-coupon"
    let get_bud_deal = "new-ws-get-bud-list"
    let scan_function = "new-ws-scan-function"
    let update_apn_device = "new-ws-update-apn-device"
    let update_fcm_device = "new-ws-update-fcm-device"
    let track_order = "new-ws-track-order"
    let new_track_order = "new-ws-track-order"
    let search_dispensry_name = "new-ws-search-dispensary-by-name"
    let newrewards = "new-ws-rewards"
    let signup_reward = "new-ws-signup-reward"
    let redeem_vocher = "new-ws-redeem-voucher"
    let new_share_dispensry = "new-ws-share-dispensary"
    let new_feedback = "new-ws-feedback"
    let privcy_policy = "new-ws-get-privacy-policy"
    let new_get_terms = "new-ws-get-terms"
    let get_buds_range = "new-ws-get-bud-range"
    let fliter_user_deal = "new-ws-filter-user-by-email"
    let coupon_expiry = "check-coupon-expiry"
    let customer_logout = "customer-logout"
    let offer_msg = "view-offer-msg-count"
    let get_offer = "ws-get-offer"
    let set_patient_data = "ws-set-patient-data"
    let delete_coupon = "ws-delete-coupon"
    let advert_offer = "ws-get-all-advertisement-offer"
    let redeem_offer = "ws-redeem-advertisement-offer"
    let ws_get_coupon = "ws-get-coupon"
    let get_patient_coupon = "ws-get-patient-coupon"
    let use_coupon = "ws-use-coupon"
    let coupon_details = "ws-get-coupon-details"
    let set_notification = "ws-set-notification"
    let ws_check_social_user  = "ws-check-social-user"
    let ws_set_date_of_birth  = "ws-set-date-of-birth"
    let new_ws_get_followed_dispensaries = "new-ws-get-followed-dispensaries"
    let new_ws_get_followed_nearby_dispensaries = "new-ws-get-followed-nearby-dispensaries"
    let new_ws_redeem = "new-ws-redeem"
    let new_ws_check_dispensary_deal = "new-ws-check-dispensary-deal"
    let browse_disp = "new-ws-get-browse-dispensary"
    let change_password = "ws-change-password"
    let ws_verify_email = "ws-verify-email"
    let ws_get_user_profile = "ws-get-user-profile"
    let ws_change_profile = "ws-change-profile"
    let new_ws_get_interest = "new-ws-get-interest"
    let new_ws_set_interest = "new-ws-set-interest"
    let new_ws_add_recent_location = "new-ws-add-recent-location"
    let new_ws_get_recent_locations = "new-ws-get-recent-locations"
    let new_ws_delete_recent_location = "new-ws-delete-recent-location"
    let new_ws_get_dispensary_detail  = "new-ws-get-dispensary-detail"
    let new_ws_get_recent_tranferred_user = "new-ws-get-recent-tranferred-user"
    let new_ws_search_user = "new-ws-search-user"
    let new_ws_transfer_bud = "new-ws-transfer-bud"
    let new_ws_rate_dispensary = "new-ws-rate-dispensary"
}

struct APIKeys
{
    let email_id = "email_id"
    let password = "password"
    let token = "token"
    let flag = "flag"
    let Device_token = "Device_token"
    let login_type = "login_type"
    let fb_id = "fb_id"
    let google_id = "google_id"
    let apple_id = "apple_id"
    let user_type = "user_type"
    let user_status = "user_status"
    let gender = "gender"
    let date_of_birth = "date_of_birth"
    let first_name = "first_name"
    let last_name = "last_name"
    let profile_picture = "profile_picture"
    let mobile_number = "mobile_number"
    let social_id = "social_id"
    let nickname = "nickname"
    let new_email = "new_email"
    let new_mobile = "new_mobile"
    let user_id = "user_id"
    let otp = "otp"
    let dob = "dob"
    let user_lat = "user_lat"
    let user_lng = "user_lng"
    let search_lat = "search_lat"
    let search_lng = "search_lng"
    let dispensary_id = "dispensary_id"
    let type = "type"
    let amount = "amount"
    let bud_code = "bud_code"
    let search_key = "search_key"
    let offer_id = "offer_id"
    let compaign_code = "compaign_code"
    let old_pass = "old_pass"
    let new_pass = "new_pass"
    let screen_type = "screen_type"
    let deal_id = "deal_id"
    let user_profile_img = "user_profile_img"
    let email = "email"
    let interests = "interests"
    let location = "location"
    let lat = "lat"
    let lng = "lng"
    let location_id = "location_id"
    let word = "word"
    let from_id = "from_id"
    let to_id = "to_id"
    let bud = "bud"
    let rate = "rate"
}



struct Constant
{
    let Device_Token = UserDefaults.standard.value(forKey: "DeviceToken") as? String ?? ""
    let Device_Id =    UIDevice.current.identifierForVendor?.uuidString ?? ""
    let User_info = UserDefaults.standard.value(forKey: "user_info") as? NSDictionary ?? [:]
    let flag = "1"
    let login_type_fb = "fb"
    let login_type_apple = "apple"
    let user_status = "1"
    let google_map_key = "AIzaSyBvNaY603xqCjcZUVtj1B3SCRTcx50w4uQ"
    //"AIzaSyBBuUJCoo6qDU6qtrGwINr-TxzUByxcyv0"
    //"AIzaSyCwpL-6L_gPLVF194pyYEvN-cz8v71Bddc"
}

struct google_url_links
{
    let address = "https://maps.google.com/maps/api/geocode/json?address="
    let attachaddress = "&sensor=false&key="
    let google_mapKey = "AIzaSyBvNaY603xqCjcZUVtj1B3SCRTcx50w4uQ"
    //"AIzaSyBBuUJCoo6qDU6qtrGwINr-TxzUByxcyv0"
    //"AIzaSyCwpL-6L_gPLVF194pyYEvN-cz8v71Bddc"
    let google_direction = "https://maps.googleapis.com/maps/api/directions/json?origin="
    let google_destination_key = "&destination="
    let google_attachmap = "&mode=driving&key="
}


struct ResponseMessage
{
    let phone_number_empty = "Please enter phone number"
    let email_empty = "Please enter your email address"
    let email_valid = "Pleasa enter valid email address"
    let password_empty = "Please enter password"
    let confirm_password_empty = "Please enter confirm password"
    let password_confirm = "Password & confirm password are not matched"
    let user_name_empty = "Please enter user name"
    let something_error = "Something went wrong"
    let no_internet = "No internet connection"
    let otp_empty = "Please enter otp"
    let date_empty = "Please enter date of birth"
    let age_valid = "Your age could not below 21"
    let email_phone_empty = "Please enter email address or phone number"
    let empty_username_space = "Username does not contain space"
    let phone_space_error = "Phone number does not contain space"
    let passowrd_space_error = "Password does not contain space"
    let forgot_email_phone =  "Please enter phone number & email address"
    let phone_number_limit = "Phone number minimum digits 6 but not more then 15 digits"
    let followed = "You are already followed"
    let subscribe = "You are already subscribe"
    let unfollowed = "You are not following this dispensary or already unfollowed this dispensary"
    let dispensary_select_error = "Please enter dispensary"
    let purchase_amount = " Please enter purchase amount"
    let code_amount = "Please enter code"
    let amount_error = "Please enter amount"
    let display_error = "Please enter display name"
    let display_error_valid = "Display name should not contain special characters"
    let current_error = "Please enter current password"
    let currrent_sace = "Current password should not contain space"
    let new_error = "Please enter new password"
    let new_sace = "New password should not contain space"
    let confirm_error = "Please Re-enter password"
    let password_m = "New password & confirm password does not mtach"
    let password_length = "Password length should be 6 to 20 characters"
    let profile_error = "Please select profile"
}


extension Date{
    var daysInMonth:Int{
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
}
 

