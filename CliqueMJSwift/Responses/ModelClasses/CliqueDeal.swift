//
//	CliqueDeal.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CliqueDeal : NSObject, NSCoding{

	var about : String!
	var backgroundImage : String!
	var bud : Int!
	var compaignType : Int!
	var companyUrl : String!
	var createdAt : String!
	var dispenaryLogo : String!
	var dispensaryId : Int!
	var dispensaryName : String!
	var endTime : Int!
	var id : Int!
	var interestId : Int!
	var interestImage : String!
	var interestName : String!
	var isOnline : Int!
	var logo : String!
	var offerCode : String!
	var offerType : Int!
	var onlineCode : String!
	var percent : Int!
	var quotient : Int!
	var rating : Int!
	var remain : Int!
	var scheduleId : Int!
	var startTime : Int!
	var title : String!
	var updatedAt : String!
	var used : Int!
	var userId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		about = dictionary["about"] as? String
		backgroundImage = dictionary["background_image"] as? String
		bud = dictionary["bud"] as? Int
		compaignType = dictionary["compaign_type"] as? Int
		companyUrl = dictionary["company_url"] as? String
		createdAt = dictionary["created_at"] as? String
		dispenaryLogo = dictionary["dispenary_logo"] as? String
		dispensaryId = dictionary["dispensary_id"] as? Int
		dispensaryName = dictionary["dispensary_name"] as? String
		endTime = dictionary["end_time"] as? Int
		id = dictionary["id"] as? Int
		interestId = dictionary["interest_id"] as? Int
		interestImage = dictionary["interest_image"] as? String
		interestName = dictionary["interest_name"] as? String
		isOnline = dictionary["is_online"] as? Int
		logo = dictionary["logo"] as? String
		offerCode = dictionary["offer_code"] as? String
		offerType = dictionary["offer_type"] as? Int
		onlineCode = dictionary["online_code"] as? String
		percent = dictionary["percent"] as? Int
		quotient = dictionary["quotient"] as? Int
		rating = dictionary["rating"] as? Int
		remain = dictionary["remain"] as? Int
		scheduleId = dictionary["schedule_id"] as? Int
		startTime = dictionary["start_time"] as? Int
		title = dictionary["title"] as? String
		updatedAt = dictionary["updated_at"] as? String
		used = dictionary["used"] as? Int
		userId = dictionary["user_id"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if about != nil{
			dictionary["about"] = about
		}
		if backgroundImage != nil{
			dictionary["background_image"] = backgroundImage
		}
		if bud != nil{
			dictionary["bud"] = bud
		}
		if compaignType != nil{
			dictionary["compaign_type"] = compaignType
		}
		if companyUrl != nil{
			dictionary["company_url"] = companyUrl
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if dispenaryLogo != nil{
			dictionary["dispenary_logo"] = dispenaryLogo
		}
		if dispensaryId != nil{
			dictionary["dispensary_id"] = dispensaryId
		}
		if dispensaryName != nil{
			dictionary["dispensary_name"] = dispensaryName
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if id != nil{
			dictionary["id"] = id
		}
		if interestId != nil{
			dictionary["interest_id"] = interestId
		}
		if interestImage != nil{
			dictionary["interest_image"] = interestImage
		}
		if interestName != nil{
			dictionary["interest_name"] = interestName
		}
		if isOnline != nil{
			dictionary["is_online"] = isOnline
		}
		if logo != nil{
			dictionary["logo"] = logo
		}
		if offerCode != nil{
			dictionary["offer_code"] = offerCode
		}
		if offerType != nil{
			dictionary["offer_type"] = offerType
		}
		if onlineCode != nil{
			dictionary["online_code"] = onlineCode
		}
		if percent != nil{
			dictionary["percent"] = percent
		}
		if quotient != nil{
			dictionary["quotient"] = quotient
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if remain != nil{
			dictionary["remain"] = remain
		}
		if scheduleId != nil{
			dictionary["schedule_id"] = scheduleId
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if used != nil{
			dictionary["used"] = used
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         about = aDecoder.decodeObject(forKey: "about") as? String
         backgroundImage = aDecoder.decodeObject(forKey: "background_image") as? String
         bud = aDecoder.decodeObject(forKey: "bud") as? Int
         compaignType = aDecoder.decodeObject(forKey: "compaign_type") as? Int
         companyUrl = aDecoder.decodeObject(forKey: "company_url") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         dispenaryLogo = aDecoder.decodeObject(forKey: "dispenary_logo") as? String
         dispensaryId = aDecoder.decodeObject(forKey: "dispensary_id") as? Int
         dispensaryName = aDecoder.decodeObject(forKey: "dispensary_name") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         interestId = aDecoder.decodeObject(forKey: "interest_id") as? Int
         interestImage = aDecoder.decodeObject(forKey: "interest_image") as? String
         interestName = aDecoder.decodeObject(forKey: "interest_name") as? String
         isOnline = aDecoder.decodeObject(forKey: "is_online") as? Int
         logo = aDecoder.decodeObject(forKey: "logo") as? String
         offerCode = aDecoder.decodeObject(forKey: "offer_code") as? String
         offerType = aDecoder.decodeObject(forKey: "offer_type") as? Int
         onlineCode = aDecoder.decodeObject(forKey: "online_code") as? String
         percent = aDecoder.decodeObject(forKey: "percent") as? Int
         quotient = aDecoder.decodeObject(forKey: "quotient") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? Int
         remain = aDecoder.decodeObject(forKey: "remain") as? Int
         scheduleId = aDecoder.decodeObject(forKey: "schedule_id") as? Int
         startTime = aDecoder.decodeObject(forKey: "start_time") as? Int
         title = aDecoder.decodeObject(forKey: "title") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         used = aDecoder.decodeObject(forKey: "used") as? Int
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if about != nil{
			aCoder.encode(about, forKey: "about")
		}
		if backgroundImage != nil{
			aCoder.encode(backgroundImage, forKey: "background_image")
		}
		if bud != nil{
			aCoder.encode(bud, forKey: "bud")
		}
		if compaignType != nil{
			aCoder.encode(compaignType, forKey: "compaign_type")
		}
		if companyUrl != nil{
			aCoder.encode(companyUrl, forKey: "company_url")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if dispenaryLogo != nil{
			aCoder.encode(dispenaryLogo, forKey: "dispenary_logo")
		}
		if dispensaryId != nil{
			aCoder.encode(dispensaryId, forKey: "dispensary_id")
		}
		if dispensaryName != nil{
			aCoder.encode(dispensaryName, forKey: "dispensary_name")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if interestId != nil{
			aCoder.encode(interestId, forKey: "interest_id")
		}
		if interestImage != nil{
			aCoder.encode(interestImage, forKey: "interest_image")
		}
		if interestName != nil{
			aCoder.encode(interestName, forKey: "interest_name")
		}
		if isOnline != nil{
			aCoder.encode(isOnline, forKey: "is_online")
		}
		if logo != nil{
			aCoder.encode(logo, forKey: "logo")
		}
		if offerCode != nil{
			aCoder.encode(offerCode, forKey: "offer_code")
		}
		if offerType != nil{
			aCoder.encode(offerType, forKey: "offer_type")
		}
		if onlineCode != nil{
			aCoder.encode(onlineCode, forKey: "online_code")
		}
		if percent != nil{
			aCoder.encode(percent, forKey: "percent")
		}
		if quotient != nil{
			aCoder.encode(quotient, forKey: "quotient")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if remain != nil{
			aCoder.encode(remain, forKey: "remain")
		}
		if scheduleId != nil{
			aCoder.encode(scheduleId, forKey: "schedule_id")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if used != nil{
			aCoder.encode(used, forKey: "used")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}