//
//	DisProfileClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

var DispProfile_List = [DisProfileClass]()
class DisProfileClass : NSObject, NSCoding{

	var cliqueDeals : [CliqueDeal]!
	var dispensary : DispensaryList!
	var loyaltyDeals : [CliqueDeal]!
	var mjDeals : [CliqueDeal]!
	var msg : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		cliqueDeals = [CliqueDeal]()
		if let cliqueDealsArray = dictionary["clique_deals"] as? [[String:Any]]{
			for dic in cliqueDealsArray{
				let value = CliqueDeal(fromDictionary: dic)
				cliqueDeals.append(value)
			}
		}
		if let dispensaryData = dictionary["dispensary"] as? [String:Any]{
			dispensary = DispensaryList(fromDictionary: dispensaryData)
		}
		loyaltyDeals = [CliqueDeal]()
		if let loyaltyDealsArray = dictionary["loyalty_deals"] as? [[String:Any]]{
			for dic in loyaltyDealsArray{
				let value = CliqueDeal(fromDictionary: dic)
				loyaltyDeals.append(value)
			}
		}
		mjDeals = [CliqueDeal]()
		if let mjDealsArray = dictionary["mj_deals"] as? [[String:Any]]{
			for dic in mjDealsArray{
				let value = CliqueDeal(fromDictionary: dic)
				mjDeals.append(value)
			}
		}
		msg = dictionary["msg"] as? String
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cliqueDeals != nil{
			var dictionaryElements = [[String:Any]]()
			for cliqueDealsElement in cliqueDeals {
				dictionaryElements.append(cliqueDealsElement.toDictionary())
			}
			dictionary["clique_deals"] = dictionaryElements
		}
		if dispensary != nil{
			dictionary["dispensary"] = dispensary.toDictionary()
		}
		if loyaltyDeals != nil{
			var dictionaryElements = [[String:Any]]()
			for loyaltyDealsElement in loyaltyDeals {
				dictionaryElements.append(loyaltyDealsElement.toDictionary())
			}
			dictionary["loyalty_deals"] = dictionaryElements
		}
		if mjDeals != nil{
			var dictionaryElements = [[String:Any]]()
			for mjDealsElement in mjDeals {
				dictionaryElements.append(mjDealsElement.toDictionary())
			}
			dictionary["mj_deals"] = dictionaryElements
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cliqueDeals = aDecoder.decodeObject(forKey :"clique_deals") as? [CliqueDeal]
         dispensary = aDecoder.decodeObject(forKey: "dispensary") as? DispensaryList
         loyaltyDeals = aDecoder.decodeObject(forKey :"loyalty_deals") as? [CliqueDeal]
         mjDeals = aDecoder.decodeObject(forKey :"mj_deals") as? [CliqueDeal]
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cliqueDeals != nil{
			aCoder.encode(cliqueDeals, forKey: "clique_deals")
		}
		if dispensary != nil{
			aCoder.encode(dispensary, forKey: "dispensary")
		}
		if loyaltyDeals != nil{
			aCoder.encode(loyaltyDeals, forKey: "loyalty_deals")
		}
		if mjDeals != nil{
			aCoder.encode(mjDeals, forKey: "mj_deals")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
