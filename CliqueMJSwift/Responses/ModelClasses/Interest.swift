//
//	Interest.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Interest : NSObject, NSCoding{

	var createdAt : String!
	var id : Int!
	var image : String!
	var imageWhite : String!
	var name : String!
	var selected : Int!
	var updatedAt : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		createdAt = dictionary["created_at"] as? String
		id = dictionary["id"] as? Int
		image = dictionary["image"] as? String
		imageWhite = dictionary["image_white"] as? String
		name = dictionary["name"] as? String
		selected = dictionary["selected"] as? Int
		updatedAt = dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if imageWhite != nil{
			dictionary["image_white"] = imageWhite
		}
		if name != nil{
			dictionary["name"] = name
		}
		if selected != nil{
			dictionary["selected"] = selected
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         imageWhite = aDecoder.decodeObject(forKey: "image_white") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         selected = aDecoder.decodeObject(forKey: "selected") as? Int
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if imageWhite != nil{
			aCoder.encode(imageWhite, forKey: "image_white")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if selected != nil{
			aCoder.encode(selected, forKey: "selected")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}