//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

var InterestList = [InterestClass]()
class InterestClass : NSObject, NSCoding{

	var interests : [Interest]!
	var msg : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		interests = [Interest]()
		if let interestsArray = dictionary["interests"] as? [[String:Any]]{
			for dic in interestsArray{
				let value = Interest(fromDictionary: dic)
				interests.append(value)
			}
		}
		msg = dictionary["msg"] as? String
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if interests != nil{
			var dictionaryElements = [[String:Any]]()
			for interestsElement in interests {
				dictionaryElements.append(interestsElement.toDictionary())
			}
			dictionary["interests"] = dictionaryElements
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         interests = aDecoder.decodeObject(forKey :"interests") as? [Interest]
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if interests != nil{
			aCoder.encode(interests, forKey: "interests")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
