//
//	Nearby.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Nearby : NSObject, NSCoding{

	var aboutMe : String!
	var address : String!
	var backgroundImage : AnyObject!
	var bud : Int!
	var dispensaryName : String!
	var distance : String!
	var email : String!
	var follow : Int!
	var id : Int!
	var isOpen : Int!
	var lat : Double!
	var lng : Double!
	var pos : Int!
	var profilePicture : String!
	var rating : Int!
	var realClosingTime : String!
	var realOpeningTime : String!
	var timezone : String!
	var userPhone : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		aboutMe = dictionary["about_me"] as? String
		address = dictionary["address"] as? String
		backgroundImage = dictionary["background_image"] as? AnyObject
		bud = dictionary["bud"] as? Int
		dispensaryName = dictionary["dispensary_name"] as? String
		distance = dictionary["distance"] as? String
		email = dictionary["email"] as? String
		follow = dictionary["follow"] as? Int
		id = dictionary["id"] as? Int
		isOpen = dictionary["is_open"] as? Int
		lat = dictionary["lat"] as? Double
		lng = dictionary["lng"] as? Double
		pos = dictionary["pos"] as? Int
		profilePicture = dictionary["profile_picture"] as? String
		rating = dictionary["rating"] as? Int
		realClosingTime = dictionary["real_closing_time"] as? String
		realOpeningTime = dictionary["real_opening_time"] as? String
		timezone = dictionary["timezone"] as? String
		userPhone = dictionary["user_phone"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if aboutMe != nil{
			dictionary["about_me"] = aboutMe
		}
		if address != nil{
			dictionary["address"] = address
		}
		if backgroundImage != nil{
			dictionary["background_image"] = backgroundImage
		}
		if bud != nil{
			dictionary["bud"] = bud
		}
		if dispensaryName != nil{
			dictionary["dispensary_name"] = dispensaryName
		}
		if distance != nil{
			dictionary["distance"] = distance
		}
		if email != nil{
			dictionary["email"] = email
		}
		if follow != nil{
			dictionary["follow"] = follow
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isOpen != nil{
			dictionary["is_open"] = isOpen
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if pos != nil{
			dictionary["pos"] = pos
		}
		if profilePicture != nil{
			dictionary["profile_picture"] = profilePicture
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if realClosingTime != nil{
			dictionary["real_closing_time"] = realClosingTime
		}
		if realOpeningTime != nil{
			dictionary["real_opening_time"] = realOpeningTime
		}
		if timezone != nil{
			dictionary["timezone"] = timezone
		}
		if userPhone != nil{
			dictionary["user_phone"] = userPhone
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aboutMe = aDecoder.decodeObject(forKey: "about_me") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         backgroundImage = aDecoder.decodeObject(forKey: "background_image") as? AnyObject
         bud = aDecoder.decodeObject(forKey: "bud") as? Int
         dispensaryName = aDecoder.decodeObject(forKey: "dispensary_name") as? String
         distance = aDecoder.decodeObject(forKey: "distance") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         follow = aDecoder.decodeObject(forKey: "follow") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         isOpen = aDecoder.decodeObject(forKey: "is_open") as? Int
         lat = aDecoder.decodeObject(forKey: "lat") as? Double
         lng = aDecoder.decodeObject(forKey: "lng") as? Double
         pos = aDecoder.decodeObject(forKey: "pos") as? Int
         profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? Int
         realClosingTime = aDecoder.decodeObject(forKey: "real_closing_time") as? String
         realOpeningTime = aDecoder.decodeObject(forKey: "real_opening_time") as? String
         timezone = aDecoder.decodeObject(forKey: "timezone") as? String
         userPhone = aDecoder.decodeObject(forKey: "user_phone") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if aboutMe != nil{
			aCoder.encode(aboutMe, forKey: "about_me")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if backgroundImage != nil{
			aCoder.encode(backgroundImage, forKey: "background_image")
		}
		if bud != nil{
			aCoder.encode(bud, forKey: "bud")
		}
		if dispensaryName != nil{
			aCoder.encode(dispensaryName, forKey: "dispensary_name")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "distance")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if follow != nil{
			aCoder.encode(follow, forKey: "follow")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isOpen != nil{
			aCoder.encode(isOpen, forKey: "is_open")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if pos != nil{
			aCoder.encode(pos, forKey: "pos")
		}
		if profilePicture != nil{
			aCoder.encode(profilePicture, forKey: "profile_picture")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if realClosingTime != nil{
			aCoder.encode(realClosingTime, forKey: "real_closing_time")
		}
		if realOpeningTime != nil{
			aCoder.encode(realOpeningTime, forKey: "real_opening_time")
		}
		if timezone != nil{
			aCoder.encode(timezone, forKey: "timezone")
		}
		if userPhone != nil{
			aCoder.encode(userPhone, forKey: "user_phone")
		}

	}

}