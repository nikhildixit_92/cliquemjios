//
//	RedeemdispensaryList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

var RedeemList = [RedeemdispensaryList]()
class RedeemdispensaryList : NSObject, NSCoding{

	var data : Datava!
	var msg : String!
	var staus : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let dataData = dictionary["data"] as? [String:Any]{
			data = Datava(fromDictionary: dataData)
		}
		msg = dictionary["msg"] as? String
		staus = dictionary["staus"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if data != nil{
			dictionary["data"] = data.toDictionary()
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if staus != nil{
			dictionary["staus"] = staus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObject(forKey: "data") as? Datava
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         staus = aDecoder.decodeObject(forKey: "staus") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if staus != nil{
			aCoder.encode(staus, forKey: "staus")
		}

	}

}
