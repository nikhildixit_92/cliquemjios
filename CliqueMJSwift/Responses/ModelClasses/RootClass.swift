//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

var CompleteRoot_data = [RootClass]()
class RootClass : NSObject, NSCoding{

	var dispensaries : [Dispensary]!
	var moreDeals : [MoreDeal]!
	var msg : String!
	var staus : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		dispensaries = [Dispensary]()
		if let dispensariesArray = dictionary["dispensaries"] as? [[String:Any]]{
			for dic in dispensariesArray{
				let value = Dispensary(fromDictionary: dic)
				dispensaries.append(value)
			}
		}
		moreDeals = [MoreDeal]()
		if let moreDealsArray = dictionary["more_deals"] as? [[String:Any]]{
			for dic in moreDealsArray{
				let value = MoreDeal(fromDictionary: dic)
				moreDeals.append(value)
			}
		}
		msg = dictionary["msg"] as? String
		staus = dictionary["staus"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if dispensaries != nil{
			var dictionaryElements = [[String:Any]]()
			for dispensariesElement in dispensaries {
				dictionaryElements.append(dispensariesElement.toDictionary())
			}
			dictionary["dispensaries"] = dictionaryElements
		}
		if moreDeals != nil{
			var dictionaryElements = [[String:Any]]()
			for moreDealsElement in moreDeals {
				dictionaryElements.append(moreDealsElement.toDictionary())
			}
			dictionary["more_deals"] = dictionaryElements
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if staus != nil{
			dictionary["staus"] = staus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dispensaries = aDecoder.decodeObject(forKey :"dispensaries") as? [Dispensary]
         moreDeals = aDecoder.decodeObject(forKey :"more_deals") as? [MoreDeal]
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         staus = aDecoder.decodeObject(forKey: "staus") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if dispensaries != nil{
			aCoder.encode(dispensaries, forKey: "dispensaries")
		}
		if moreDeals != nil{
			aCoder.encode(moreDeals, forKey: "more_deals")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if staus != nil{
			aCoder.encode(staus, forKey: "staus")
		}

	}

}
