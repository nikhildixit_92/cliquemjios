//
//	SearchDispensary.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

var Search_Dips_List = [SearchDispensary]()
class SearchDispensary : NSObject, NSCoding{

	var followed : [FollowedBrowse]!
	var msg : String!
	var nearby : [NearbyBrowse]!
	var staus : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		followed = [FollowedBrowse]()
		if let followedArray = dictionary["followed"] as? [[String:Any]]{
			for dic in followedArray{
				let value = FollowedBrowse(fromDictionary: dic)
				followed.append(value)
			}
		}
		msg = dictionary["msg"] as? String
		nearby = [NearbyBrowse]()
		if let nearbyArray = dictionary["nearby"] as? [[String:Any]]{
			for dic in nearbyArray{
				let value = NearbyBrowse(fromDictionary: dic)
				nearby.append(value)
			}
		}
		staus = dictionary["staus"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if followed != nil{
			var dictionaryElements = [[String:Any]]()
			for followedElement in followed {
				dictionaryElements.append(followedElement.toDictionary())
			}
			dictionary["followed"] = dictionaryElements
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if nearby != nil{
			var dictionaryElements = [[String:Any]]()
			for nearbyElement in nearby {
				dictionaryElements.append(nearbyElement.toDictionary())
			}
			dictionary["nearby"] = dictionaryElements
		}
		if staus != nil{
			dictionary["staus"] = staus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         followed = aDecoder.decodeObject(forKey :"followed") as? [FollowedBrowse]
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         nearby = aDecoder.decodeObject(forKey :"nearby") as? [NearbyBrowse]
         staus = aDecoder.decodeObject(forKey: "staus") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if followed != nil{
			aCoder.encode(followed, forKey: "followed")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if nearby != nil{
			aCoder.encode(nearby, forKey: "nearby")
		}
		if staus != nil{
			aCoder.encode(staus, forKey: "staus")
		}

	}

}
