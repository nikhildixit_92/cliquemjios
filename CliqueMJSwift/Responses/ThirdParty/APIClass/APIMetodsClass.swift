//
//  APIMetodsClass.swift
//  ELATES
//
//  Created by nikhil on 28/02/19.
//  Copyright © 2019 nikhil. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


struct APIMethods
{
    func PostAPI(url:URL,Parameters:Parameters,headers:[String:String],completionHandler: @escaping(_ response:Any,Bool) -> ()) ->()
    {
        Alamofire.request(url, method: .post, parameters: Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            
            
            switch(response.result)
            {
            case .success(_):
                if response.result.value != nil
                {
                    
                    let data = response.result.value as Any
                    completionHandler(data,true)
                }
                break
                
            case .failure(_):
                completionHandler("",false)
                break
                
            }
        }
        
    }
    
    func GetAPI(url:URL,Parameters:Parameters,headers:[String:String],completionHandler: @escaping(_ response:Any,Bool) -> ()) ->()
    {
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default , headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result)
            {
            case .success(_):
                if response.result.value != nil
                {
                    
                    let data = response.result.value as Any
                    completionHandler(data,true)
                }
                break
                
            case .failure(_):
                completionHandler("",false)
                break
                
            }
        }
        
    }
    
    
    func MultiRequest(url:URL,imageData:Data?,parameters:[String : Any],headers:[String:String],completionHandler: @escaping(_ response:Any,Bool) -> ()) ->()
    {
        
        let currentTime = Int(NSDate().timeIntervalSince1970 * 1000)
        let arrived_time = String(currentTime)
        print(arrived_time)
        
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "user_profile_img", fileName: "\(arrived_time).png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        completionHandler(err, false)
                        // SVProgressHUD.dismiss()
                        return
                    }
                    //   SVProgressHUD.dismiss()
                    completionHandler(response.result.value!, true)
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionHandler("", false)
                
                //SVProgressHUD.dismiss()
            }
        }
    }
    
}

class Connectivity
{
    class func isConnectedToInternet() ->Bool
    {
        return NetworkReachabilityManager()!.isReachable
        
        
    }
    
    
}


extension UIImage {
    
    func fixedOrientation() -> UIImage {
        // No-op if the orientation is already correct
        if (imageOrientation == UIImage.Orientation.up) {
            return self
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform:CGAffineTransform = CGAffineTransform.identity
        
        if (imageOrientation == UIImage.Orientation.down
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        }
        
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2))
        }
        
        if (imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            
            transform = transform.translatedBy(x: 0, y: size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2));
        }
        
        if (imageOrientation == UIImage.Orientation.upMirrored
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if (imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            
            transform = transform.translatedBy(x: size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx:CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                      bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                                      space: cgImage!.colorSpace!,
                                      bitmapInfo: cgImage!.bitmapInfo.rawValue)!
        
        ctx.concatenate(transform)
        
        
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored
            ) {
            
            
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.height,height:size.width))
            
        } else {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.width,height:size.height))
        }
        
        
        // And now we just create a new UIImage from the drawing context
        let cgimg:CGImage = ctx.makeImage()!
        let imgEnd:UIImage = UIImage(cgImage: cgimg)
        
        return imgEnd
    }
}

